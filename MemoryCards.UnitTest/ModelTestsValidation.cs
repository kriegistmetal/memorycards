﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MemoryCards.UnitTest
{
    public abstract class ModelTestsValidation
    {
        protected IList<ValidationResult> ValidateModel(object model)
        {
            var validationResult = new List<ValidationResult>();
            var validationContext = new ValidationContext(model);

            Validator.TryValidateObject(model, validationContext, validationResult, true);
            return validationResult;
        }
    }
}
