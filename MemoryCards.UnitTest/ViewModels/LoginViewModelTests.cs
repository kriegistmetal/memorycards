﻿using MemoryCards.ViewModels;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace MemoryCards.UnitTest.ViewModels
{
    public class LoginViewModelTests : ModelTestsValidation
    {
        [Fact]
        public void EmailRequiredTest()
        {
            var loginViewModel = new LoginViewModel
            {
                Email = null,                
                Password = "TestPassword"                
            };

            var result = ValidateModel(loginViewModel);

            Assert.True(ValidateModel(loginViewModel).Count > 0);
            Assert.Contains(result, v => v.MemberNames.Contains("Email"));
            Assert.Contains(result, v => v.ErrorMessage.Contains("Введите почту"));
        }
        [Fact]
        public void EmailStringLengthTest()
        {
            var loginViewModelMin = new LoginViewModel
            {
                Email = "l@m.r"
            };
            var loginViewModelMax = new LoginViewModel
            {
                Email = "whassupyallthisimysjrhfiproject228lsskducmcuwm@gmail.com"
            };

            var result = ValidateModel(loginViewModelMin);
            var result2 = ValidateModel(loginViewModelMax);

            Assert.True(ValidateModel(loginViewModelMin).Count > 0);
            Assert.True(ValidateModel(loginViewModelMax).Count > 0);

            Assert.Contains(ValidateModel(loginViewModelMin), v => v.MemberNames.Contains("Email") &&
            v.ErrorMessage.Contains("Длина Email должна быть от 6 до 31 символов"));
            Assert.Contains(ValidateModel(loginViewModelMax), v => v.MemberNames.Contains("Email") &&
             v.ErrorMessage.Contains("Длина Email должна быть от 6 до 31 символов"));
        }
        [Fact]
        public void PasswordRequiredTest()
        {
            var loginViewModel = new LoginViewModel
            {
                Email = "TestEmail",
                Password = null
            };

            var result = ValidateModel(loginViewModel);

            Assert.True(ValidateModel(loginViewModel).Count > 0);
            Assert.Contains(result, v => v.MemberNames.Contains("Password"));
            Assert.Contains(result, v => v.ErrorMessage.Contains("Введите пароль"));
        }
        [Fact]
        public void EmailRegularExpressionTest()
        {
            var loginViewModel = new LoginViewModel
            {
                Email = "TestEmail"
            };

            var result = ValidateModel(loginViewModel);

            Assert.True(ValidateModel(loginViewModel).Count > 0);
            Assert.Contains(ValidateModel(loginViewModel), v => v.MemberNames.Contains("Email") &&
            v.ErrorMessage.Contains("Некорректный адрес электронной почты"));
        }
    }
}
