﻿using MemoryCards.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace MemoryCards.UnitTest.ViewModels
{
    public class ForgotPasswordViewModelTests : ModelTestsValidation
    {
        [Fact]
        public void EmailRequiredTest()
        {
            var forgotPasswordViewModel = new ForgotPasswordViewModel
            {
                Email = null
            };
            var result = ValidateModel(forgotPasswordViewModel);

            Assert.True(ValidateModel(forgotPasswordViewModel).Count > 0);
            Assert.Contains(result, v => v.MemberNames.Contains("Email"));
            Assert.Contains(result, v => v.ErrorMessage.Contains("Введите почту"));
        }
        [Fact]
        public void EmailRegularExpressionTest()
        {
            var forgotPasswordViewModel = new ForgotPasswordViewModel
            {
                Email = "TestEmail"
            };

            var result = ValidateModel(forgotPasswordViewModel);

            Assert.True(ValidateModel(forgotPasswordViewModel).Count > 0);
            Assert.Contains(ValidateModel(forgotPasswordViewModel), v => v.MemberNames.Contains("Email") &&
            v.ErrorMessage.Contains("Некорректный адрес электронной почты"));
        }
        [Fact]
        public void EmailStringLengthTest()
        {
            var forgotPasswordViewmodelMin = new ForgotPasswordViewModel
            {
                Email = "l@m.r"
            };
            var forgotPasswordViewmodelMax = new ForgotPasswordViewModel
            {
                Email = "whassupyallthisimysjrhfiproject228lsskducmcuwm@gmail.com"
            };

            var result = ValidateModel(forgotPasswordViewmodelMin);
            var result2 = ValidateModel(forgotPasswordViewmodelMax);

            Assert.True(ValidateModel(forgotPasswordViewmodelMin).Count > 0);
            Assert.True(ValidateModel(forgotPasswordViewmodelMax).Count > 0);

            Assert.Contains(ValidateModel(forgotPasswordViewmodelMin), v => v.MemberNames.Contains("Email") &&
            v.ErrorMessage.Contains("Длина Email должна быть от 6 до 31 символов"));
            Assert.Contains(ValidateModel(forgotPasswordViewmodelMax), v => v.MemberNames.Contains("Email") &&
             v.ErrorMessage.Contains("Длина Email должна быть от 6 до 31 символов"));
        }
    }
}
