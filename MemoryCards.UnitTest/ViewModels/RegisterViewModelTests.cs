﻿using MemoryCards.Controllers;
using MemoryCards.Models;
using MemoryCards.ViewModels;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Localization;
using Moq;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Security.Claims;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace MemoryCards.UnitTest.ViewModels
{
    public class RegisterViewModelTests : ModelTestsValidation
    {
        [Fact]
        public void EmailRequiredTest()
        {
            var registerViewModel = new RegisterViewModel
            {
                Email = null                
            };

            var result = ValidateModel(registerViewModel);

            Assert.True(ValidateModel(registerViewModel).Count > 0);
            Assert.Contains(result, v => v.MemberNames.Contains("Email"));
            Assert.Contains(result, v => v.ErrorMessage.Contains("Введите почту"));
        }
        [Fact]
        public void EmailStringLengthTest()
        {
            var registerViewModellMin = new RegisterViewModel
            {
                Email = "l@m.r"
            };
            var registerViewModelMax = new RegisterViewModel
            {
                Email = "whassupyallthisimysjrhfiproject228lsskducmcuwm@gmail.com"
            };

            var result = ValidateModel(registerViewModellMin);
            var result2 = ValidateModel(registerViewModelMax);

            Assert.True(ValidateModel(registerViewModellMin).Count > 0);
            Assert.True(ValidateModel(registerViewModelMax).Count > 0);

            Assert.Contains(ValidateModel(registerViewModellMin), v => v.MemberNames.Contains("Email") &&
            v.ErrorMessage.Contains("Длина Email должна быть от 6 до 31 символов"));
            Assert.Contains(ValidateModel(registerViewModelMax), v => v.MemberNames.Contains("Email") &&
             v.ErrorMessage.Contains("Длина Email должна быть от 6 до 31 символов"));
        }
        [Fact]
        public void LoginRequiredTest()
        {
            var registerViewModel = new RegisterViewModel
            {                
                Login = null                
            };

            var result = ValidateModel(registerViewModel);

            Assert.True(ValidateModel(registerViewModel).Count > 0);
            Assert.Contains(result, v => v.MemberNames.Contains("Login"));
            Assert.Contains(result, v => v.ErrorMessage.Contains("Укажите логин"));
        }
        [Fact]
        public void CountryRequiredTest()
        {
            var registerViewModel = new RegisterViewModel
            {                
                Country = null                
            };

            var result = ValidateModel(registerViewModel);

            Assert.True(ValidateModel(registerViewModel).Count > 0);
            Assert.Contains(result, v => v.MemberNames.Contains("Country"));
            Assert.Contains(result, v => v.ErrorMessage.Contains("Укажите страну проживания"));
        }
        [Fact]
        public void PasswordRequiredTest()
        {
            var registerViewModel = new RegisterViewModel
            {                
                Password = null               
            };

            var result = ValidateModel(registerViewModel);

            Assert.True(ValidateModel(registerViewModel).Count > 0);
            Assert.Contains(result, v => v.MemberNames.Contains("Password"));
            Assert.Contains(result, v => v.ErrorMessage.Contains("Введите пароль"));
        }
        [Fact]
        public void PasswordConfirmRequiredTest()
        {
            var registerViewModel = new RegisterViewModel
            {                
                PasswordConfirm = null
            };

            var result = ValidateModel(registerViewModel);

            Assert.True(ValidateModel(registerViewModel).Count > 0);
            Assert.Contains(result, v => v.MemberNames.Contains("PasswordConfirm"));
            Assert.Contains(result, v => v.ErrorMessage.Contains("Подтвердите пароль"));
        }
        [Fact]
        public void EmailRegularExpressionTest()
        {
            var registerViewModel = new RegisterViewModel
            {
                Email = "TestEmail"                
            };

            var result = ValidateModel(registerViewModel);

            Assert.True(ValidateModel(registerViewModel).Count > 0);
            Assert.Contains(ValidateModel(registerViewModel), v => v.MemberNames.Contains("Email") &&
            v.ErrorMessage.Contains("Некорректный адрес электронной почты"));
        }
        [Fact]
        public void LoginStringLengthTest()
        {
            var registerViewModelMin = new RegisterViewModel
            {
                Login = "Te"                
            };
            var registerViewModelMax = new RegisterViewModel
            {
                Login = "TestLoginStringLength"
            };

            var result = ValidateModel(registerViewModelMin);
            var result2 = ValidateModel(registerViewModelMax);

            Assert.True(ValidateModel(registerViewModelMin).Count > 0);
            Assert.True(ValidateModel(registerViewModelMax).Count > 0);

            Assert.Contains(ValidateModel(registerViewModelMin), v => v.MemberNames.Contains("Login") &&
            v.ErrorMessage.Contains("Длина логина должна быть от 3 до 15 символов"));
            Assert.Contains(ValidateModel(registerViewModelMax), v => v.MemberNames.Contains("Login") &&
             v.ErrorMessage.Contains("Длина логина должна быть от 3 до 15 символов"));
        }
        [Fact]
        public void PasswordConfirmCompareTest()
        {
            var registerViewModel = new RegisterViewModel
            {
                Password = "TestPassword",
                PasswordConfirm = "TestPasswordConfirm",
            };

            var result = ValidateModel(registerViewModel);

            Assert.True(ValidateModel(registerViewModel).Count > 0);
            Assert.Contains(ValidateModel(registerViewModel), v => v.MemberNames.Contains("PasswordConfirm") &&
            v.ErrorMessage.Contains("Пароли не совпадают"));
        }
        public static Mock<UserManager<User>> MockUserManager<User>() where User : class
        {
            var store = new Mock<IUserStore<User>>();
            var mgr = new Mock<UserManager<User>>(store.Object, null, null, null, null, null, null, null, null);
            mgr.Object.UserValidators.Add(new UserValidator<User>());
            mgr.Object.PasswordValidators.Add(new PasswordValidator<User>());
            return mgr;
        }
        public Mock<SignInManager<User>> MockSignInManager(Mock<UserManager<User>> userManager)
        {
            var contextAccesor = new Mock<IHttpContextAccessor>();
            var claimsFactory = new Mock<IUserClaimsPrincipalFactory<User>>();
            return new Mock<SignInManager<User>>(userManager.Object, contextAccesor.Object, claimsFactory.Object, null,
                null, null, null);
        }
        [Fact]
        public void CheckEmailTest()
        {
            var options = new DbContextOptionsBuilder<MemoryContext>()
                              .UseInMemoryDatabase(databaseName: "memorydbtestsValidation")
                              .Options;
            using (var context = new MemoryContext(options))
            {
                string email = "TestsEmails@gmail.com";
                User user = new User
                {
                    Id = "TestId",
                    Email = "TestEmail@gmail.com",
                    Login = "TestLogin",
                    PasswordHash = "TestPasswordHash"
                };
                context.Users.Add(user);
                context.SaveChanges();

                var identity = new Mock<IIdentity>();
                identity.SetupGet(i => i.IsAuthenticated).Returns(true);
                identity.SetupGet(i => i.Name).Returns("FakeUserName");
                var mockPrincipal = new Mock<ClaimsPrincipal>();
                mockPrincipal.Setup(x => x.Identity).Returns(identity.Object);
                var mockHttpContext = new Mock<HttpContext>();
                mockHttpContext.Setup(m => m.User).Returns(mockPrincipal.Object);
                var _UserManager = MockUserManager<User>();
                _UserManager.Setup(x => x.GetUserAsync(mockPrincipal.Object)).ReturnsAsync(user);
                var signInMocker = MockSignInManager(_UserManager);
                var webHostEnvironment = new Mock<IWebHostEnvironment>();
                var _serviceScopeFactory = new Mock<IServiceScopeFactory>();
                var mock = new Mock<IStringLocalizer<AccountController>>();
                var accountcontroller = new AccountController(_UserManager.Object, signInMocker.Object, context, webHostEnvironment.Object, _serviceScopeFactory.Object, mock.Object);
                var results = accountcontroller.CheckEmail(email);
                Assert.NotNull(results);
                Assert.IsType<JsonResult>(results);
            }
        }
        [Fact]
        public void PasswordStringLengthTest()
        {
            var registerViewModelMin = new RegisterViewModel
            {
                Password = "Test"
            };
            var registerViewModelMax = new RegisterViewModel
            {
                Password = "TestPasswordStringLength"
            };

            var result = ValidateModel(registerViewModelMin);
            var result2 = ValidateModel(registerViewModelMax);

            Assert.True(ValidateModel(registerViewModelMin).Count > 0);
            Assert.True(ValidateModel(registerViewModelMax).Count > 0);

            Assert.Contains(ValidateModel(registerViewModelMin), v => v.MemberNames.Contains("Password") &&
            v.ErrorMessage.Contains("Длина пароля должна быть от 6 до 10 символов"));
            Assert.Contains(ValidateModel(registerViewModelMax), v => v.MemberNames.Contains("Password") &&
             v.ErrorMessage.Contains("Длина пароля должна быть от 6 до 10 символов"));
        }
    }
}
