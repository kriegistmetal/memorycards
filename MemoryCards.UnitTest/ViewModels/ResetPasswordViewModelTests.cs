﻿using MemoryCards.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace MemoryCards.UnitTest.ViewModels
{
    public class ResetPasswordViewModelTests : ModelTestsValidation
    {
        [Fact]
        public void EmailRequiredTest()
        {
            var resetPasswordViewModel = new ResetPasswordViewModel
            {
                Email = null
            };
            var result = ValidateModel(resetPasswordViewModel);

            Assert.True(ValidateModel(resetPasswordViewModel).Count > 0);
            Assert.Contains(result, v => v.MemberNames.Contains("Email"));
            Assert.Contains(result, v => v.ErrorMessage.Contains("Введите почту"));
        }
        [Fact]
        public void EmailStringLengthTest()
        {
            var resetPasswordViewModelMin = new ResetPasswordViewModel
            {
                Email = "l@m.r"
            };
            var resetPasswordViewModelMax = new ResetPasswordViewModel
            {
                Email = "whassupyallthisimysjrhfiproject228lsskducmcuwm@gmail.com"
            };

            var result = ValidateModel(resetPasswordViewModelMin);
            var result2 = ValidateModel(resetPasswordViewModelMax);

            Assert.True(ValidateModel(resetPasswordViewModelMin).Count > 0);
            Assert.True(ValidateModel(resetPasswordViewModelMax).Count > 0);

            Assert.Contains(ValidateModel(resetPasswordViewModelMin), v => v.MemberNames.Contains("Email") &&
            v.ErrorMessage.Contains("Длина Email должна быть от 6 до 31 символов"));
            Assert.Contains(ValidateModel(resetPasswordViewModelMax), v => v.MemberNames.Contains("Email") &&
             v.ErrorMessage.Contains("Длина Email должна быть от 6 до 31 символов"));
        }
        [Fact]
        public void EmailRegularExpressionTest()
        {
            var resetPasswordViewModel = new ResetPasswordViewModel
            {
                Email = "TestEmail"
            };

            var result = ValidateModel(resetPasswordViewModel);

            Assert.True(ValidateModel(resetPasswordViewModel).Count > 0);
            Assert.Contains(ValidateModel(resetPasswordViewModel), v => v.MemberNames.Contains("Email") &&
            v.ErrorMessage.Contains("Некорректный адрес электронной почты"));
        }
        [Fact]
        public void PasswordRequiredTest()
        {
            var resetPasswordViewModel = new ResetPasswordViewModel
            {
                Password = null
            };
            var result = ValidateModel(resetPasswordViewModel);

            Assert.True(ValidateModel(resetPasswordViewModel).Count > 0);
            Assert.Contains(result, v => v.MemberNames.Contains("Password"));
            Assert.Contains(result, v => v.ErrorMessage.Contains("Введите пароль"));
        }
        [Fact]
        public void ConfirmPasswordRequiredTest()
        {
            var resetPasswordViewModel = new ResetPasswordViewModel
            {
                ConfirmPassword = null
            };
            var result = ValidateModel(resetPasswordViewModel);

            Assert.True(ValidateModel(resetPasswordViewModel).Count > 0);
            Assert.Contains(result, v => v.MemberNames.Contains("Password"));
            Assert.Contains(result, v => v.ErrorMessage.Contains("Введите пароль"));
        }
        [Fact]
        public void PasswordConfirmCompareTest()
        {
            var resetPasswordViewModel = new ResetPasswordViewModel
            {
                Password = "TestPassword",
                ConfirmPassword = "TestPasswordConfirm",
            };

            var result = ValidateModel(resetPasswordViewModel);

            Assert.True(ValidateModel(resetPasswordViewModel).Count > 0);
            Assert.Contains(ValidateModel(resetPasswordViewModel), v => v.MemberNames.Contains("ConfirmPassword") &&
            v.ErrorMessage.Contains("Пароли не совпадают"));
        }
        [Fact]
        public void PasswordStringLengthTest()
        {
            var resetPasswordViewModelMin = new ResetPasswordViewModel
            {
                Password = "Test"
            };
            var resetPasswordViewModelMax = new ResetPasswordViewModel
            {
                Password = "TestPasswordStringLength"
            };

            var result = ValidateModel(resetPasswordViewModelMin);
            var result2 = ValidateModel(resetPasswordViewModelMax);

            Assert.True(ValidateModel(resetPasswordViewModelMin).Count > 0);
            Assert.True(ValidateModel(resetPasswordViewModelMax).Count > 0);

            Assert.Contains(ValidateModel(resetPasswordViewModelMin), v => v.MemberNames.Contains("Password") &&
            v.ErrorMessage.Contains("Длина пароля должна быть от 6 до 10 символов"));
            Assert.Contains(ValidateModel(resetPasswordViewModelMax), v => v.MemberNames.Contains("Password") &&
             v.ErrorMessage.Contains("Длина пароля должна быть от 6 до 10 символов"));
        }
    }
}
