﻿using MemoryCards.Controllers;
using MemoryCards.Models;
using MemoryCards.ViewModels;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Localization;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace MemoryCards.UnitTest.ViewModels
{
    public class ChangeProfileUserViewModelTests : ModelTestsValidation
    {
        [Fact]
        public void EmailRegularExpressionTest()
        {
            var changeProfileUserViewModel = new ChangeProfileUserViewModel
            {
                Email = "TestEmail"
            };

            var result = ValidateModel(changeProfileUserViewModel);

            Assert.True(ValidateModel(changeProfileUserViewModel).Count > 0);
            Assert.Contains(ValidateModel(changeProfileUserViewModel), v => v.MemberNames.Contains("Email") &&
            v.ErrorMessage.Contains("Некорректный адрес электронной почты"));
        }
        [Fact]
        public void LoginStringLengthTest()
        {
            var changeProfileUserViewModelMin = new ChangeProfileUserViewModel
            {
                Login = "Te"
            };
            var changeProfileUserViewModelMax = new ChangeProfileUserViewModel
            {
                Login = "TestLoginStringLength"
            };

            var result = ValidateModel(changeProfileUserViewModelMin);
            var result2 = ValidateModel(changeProfileUserViewModelMax);

            Assert.True(ValidateModel(changeProfileUserViewModelMin).Count > 0);
            Assert.True(ValidateModel(changeProfileUserViewModelMax).Count > 0);

            Assert.Contains(ValidateModel(changeProfileUserViewModelMin), v => v.MemberNames.Contains("Login") &&
            v.ErrorMessage.Contains("Длина логина должна быть от 3 до 15 символов"));
            Assert.Contains(ValidateModel(changeProfileUserViewModelMax), v => v.MemberNames.Contains("Login") &&
             v.ErrorMessage.Contains("Длина логина должна быть от 3 до 15 символов"));
        }
        [Fact]
        public void EmailStringLengthTest()
        {
            var changeProfileUserViewModelMin = new ChangeProfileUserViewModel
            {
                Email = "l@m.r"
            };
            var changeProfileUserViewModelMax = new ChangeProfileUserViewModel
            {
                Email = "whassupyallthisimysjrhfiproject228lsskducmcuwm@gmail.com"
            };

            var result = ValidateModel(changeProfileUserViewModelMin);
            var result2 = ValidateModel(changeProfileUserViewModelMax);

            Assert.True(ValidateModel(changeProfileUserViewModelMin).Count > 0);
            Assert.True(ValidateModel(changeProfileUserViewModelMax).Count > 0);

            Assert.Contains(ValidateModel(changeProfileUserViewModelMin), v => v.MemberNames.Contains("Email") &&
            v.ErrorMessage.Contains("Длина Email должна быть от 6 до 31 символов"));
            Assert.Contains(ValidateModel(changeProfileUserViewModelMax), v => v.MemberNames.Contains("Email") &&
             v.ErrorMessage.Contains("Длина Email должна быть от 6 до 31 символов"));
        }
        [Fact]
        public void PasswordConfirmCompareTest()
        {
            var changeProfileUserViewModel = new ChangeProfileUserViewModel
            {
                NewPassword = "TestPassword",
                PasswordConfirm = "TestPasswordConfirm",
            };

            var result = ValidateModel(changeProfileUserViewModel);

            Assert.True(ValidateModel(changeProfileUserViewModel).Count > 0);
            Assert.Contains(ValidateModel(changeProfileUserViewModel), v => v.MemberNames.Contains("PasswordConfirm") &&
            v.ErrorMessage.Contains("Пароли не совпадают"));
        }
        public static Mock<UserManager<User>> MockUserManager<User>() where User : class
        {
            var store = new Mock<IUserStore<User>>();
            var mgr = new Mock<UserManager<User>>(store.Object, null, null, null, null, null, null, null, null);
            mgr.Object.UserValidators.Add(new UserValidator<User>());
            mgr.Object.PasswordValidators.Add(new PasswordValidator<User>());
            return mgr;
        }
        public Mock<SignInManager<User>> MockSignInManager(Mock<UserManager<User>> userManager)
        {
            var contextAccesor = new Mock<IHttpContextAccessor>();
            var claimsFactory = new Mock<IUserClaimsPrincipalFactory<User>>();
            return new Mock<SignInManager<User>>(userManager.Object, contextAccesor.Object, claimsFactory.Object, null,
                null, null, null);
        }
        [Fact]
        public void CheckEmailTest()
        {
            var options = new DbContextOptionsBuilder<MemoryContext>()
                              .UseInMemoryDatabase(databaseName: "memorydbtestsValidations")
                              .Options;
            using (var context = new MemoryContext(options))
            {                
                string email = "TestsEmails@gmail.com";
                User user = new User
                {
                    Id = "TestId",
                    Email = "TestEmail@gmail.com",
                    Login = "TestLogin",
                    PasswordHash = "TestPasswordHash"
                };
                context.Users.Add(user);
                context.SaveChanges();

                var identity = new Mock<IIdentity>();
                identity.SetupGet(i => i.IsAuthenticated).Returns(true);
                identity.SetupGet(i => i.Name).Returns("FakeUserName");
                var mockPrincipal = new Mock<ClaimsPrincipal>();
                mockPrincipal.Setup(x => x.Identity).Returns(identity.Object);
                var mockHttpContext = new Mock<HttpContext>();
                mockHttpContext.Setup(m => m.User).Returns(mockPrincipal.Object);
                var _UserManager = MockUserManager<User>();
                _UserManager.Setup(x => x.GetUserAsync(mockPrincipal.Object)).ReturnsAsync(user);
                var signInMocker = MockSignInManager(_UserManager);
                var webHostEnvironment = new Mock<IWebHostEnvironment>();
                var _serviceScopeFactory = new Mock<IServiceScopeFactory>();
                var mock = new Mock<IStringLocalizer<AccountController>>();
                var accountcontroller = new AccountController(_UserManager.Object, signInMocker.Object, context, webHostEnvironment.Object, _serviceScopeFactory.Object, mock.Object);
                var results =  accountcontroller.CheckEmail(email);
                Assert.NotNull(results);
                Assert.IsType<JsonResult>(results);
            }
        }
        [Fact]
        public void PasswordStringLengthTest()
        {
            var changeProfileUserViewModelMin = new ChangeProfileUserViewModel
            {
                NewPassword = "Test"
            };
            var changeProfileUserViewModelMax = new ChangeProfileUserViewModel
            {
                NewPassword = "TestPasswordStringLength"
            };

            var result = ValidateModel(changeProfileUserViewModelMin);
            var result2 = ValidateModel(changeProfileUserViewModelMax);

            Assert.True(ValidateModel(changeProfileUserViewModelMin).Count > 0);
            Assert.True(ValidateModel(changeProfileUserViewModelMax).Count > 0);

            Assert.Contains(ValidateModel(changeProfileUserViewModelMin), v => v.MemberNames.Contains("NewPassword") &&
            v.ErrorMessage.Contains("Длина пароля должна быть от 6 до 10 символов"));
            Assert.Contains(ValidateModel(changeProfileUserViewModelMax), v => v.MemberNames.Contains("NewPassword") &&
             v.ErrorMessage.Contains("Длина пароля должна быть от 6 до 10 символов"));
        }
    }
}
