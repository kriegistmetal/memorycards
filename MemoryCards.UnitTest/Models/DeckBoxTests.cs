﻿using MemoryCards.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace MemoryCards.UnitTest.Models
{
   public class DeckBoxTests : ModelTestsValidation
    {
        [Fact]
        public void TestModelCard()
        {
            DeckBox deckBox = new DeckBox
            {               
                Description = "TestDescription"
            };
            DeckBox deckBox1 = new DeckBox
            {
                Title = "A",
                Description = "TestDescription"
            };

            DeckBox deckBox2 = new DeckBox
            {
                Title = "AASDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDSDSD",
                Description = "TestDescription"
            };

            Assert.Contains(ValidateModel(deckBox), v => v.MemberNames.Contains("Title") &&
            v.ErrorMessage.Contains("Отсутствует название"));       

            Assert.Contains(ValidateModel(deckBox1), d => d.MemberNames.Contains("Title") && 
            d.ErrorMessage.Contains("Длина названия должна быть от 3 до 50 символов"));

            Assert.Contains(ValidateModel(deckBox2), v => v.MemberNames.Contains("Title") &&
             v.ErrorMessage.Contains("Длина названия должна быть от 3 до 50 символов"));
        }
        [Fact]
        public void TestDescriptionModelCard()
        {
            DeckBox deckBox = new DeckBox
            {
                Description = "T"
            };
            DeckBox deckBox1 = new DeckBox
            {                
                Description = "TestDescriptionTestDescriptionTestDescriptionTestDescription"
            };
            
            Assert.Contains(ValidateModel(deckBox), v => v.MemberNames.Contains("Description") &&
            v.ErrorMessage.Contains("Длина описания должна быть от 3 до 50 символов"));
            Assert.Contains(ValidateModel(deckBox1), d => d.MemberNames.Contains("Description") &&
            d.ErrorMessage.Contains("Длина описания должна быть от 3 до 50 символов"));            
        }
    }  
}
