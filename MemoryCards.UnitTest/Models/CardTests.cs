﻿using MemoryCards.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace MemoryCards.UnitTest.Models
{
    public class CardTests : ModelTestsValidation
    {
        [Fact]
        public void TestRequiredWord()
        {            
            Card cardWord = new Card
            {
                CreatedAt = DateTime.Parse("2021-01-11"),
                Word = null,
                UseExample = "saskklsad",
                Defination = "TestDefination",
                UpdatedAt = DateTime.Parse("2021-01-11")
            };

            var resultWord = ValidateModel(cardWord);            
            Assert.True(ValidateModel(cardWord).Count > 0);  
            Assert.Contains(resultWord, v => v.MemberNames.Contains("Word"));
            Assert.Contains(resultWord, v => v.ErrorMessage.Contains("Обязательное поле"));
        }
        [Fact]
        public void TestRequiredDefination()
        {
            Card cardDefination = new Card
            {
                CreatedAt = DateTime.Parse("2021-01-11"),
                Word = "TestWord",
                UseExample = "saskklsad",
                Defination = null,
                UpdatedAt = DateTime.Parse("2021-01-11")
            };            

            var resultDefination = ValidateModel(cardDefination);            
            Assert.True(ValidateModel(cardDefination).Count > 0);            
            Assert.Contains(resultDefination, v => v.MemberNames.Contains("Defination"));
            Assert.Contains(resultDefination, v => v.ErrorMessage.Contains("Обязательное поле"));            
        }
        [Fact]
        public void TestRequiredUseExample()
        {
            Card carUseExample = new Card
            {
                CreatedAt = DateTime.Parse("2021-01-11"),
                Word = "TestWord",
                Defination = "sdsfjjslsa",
                UseExample = null,
                UpdatedAt = DateTime.Parse("2021-01-11")
            };

            var resultUse = ValidateModel(carUseExample);
            Assert.True(ValidateModel(carUseExample).Count > 0);
            Assert.Contains(resultUse, v => v.MemberNames.Contains("UseExample"));
            Assert.Contains(resultUse, v => v.ErrorMessage.Contains("Обязательное поле"));
        }

        [Fact]
        public void TestWordStringLength()
        {
            Card cardWord = new Card
            {
                CreatedAt = DateTime.Parse("2021-01-11"),
                Word = "asdaskdsklklsadasldklasdklaslkdsalkddsfdsfdfdfddfdf",
                UseExample = "saskklsad",
                Defination = "asdaskdsklkfjsdflksjfsdlkfjdlkfdsjfksldjfkljsdfkljsdfklsjdffff",
                UpdatedAt = DateTime.Parse("2021-01-11")
            };

            Assert.Contains(ValidateModel(cardWord), v => v.MemberNames.Contains("Word") &&
            v.ErrorMessage.Contains("Длина названия должна быть от 3 до 50 символов"));
        }

        [Fact]
        public void TestDefinationStringLength()
        {
            Card cardDefination = new Card
            {
                CreatedAt = DateTime.Parse("2021-01-11"),
                Word = "TestWord",
                UseExample = "saskklsad",
                Defination = "asdaskdsklklsadasldklasdklaslkdsalkdalkldkaksldadfmljslfnslkfndsfk,ndfsdfjfdskfjdskfsjdklfsjdkfjsdklfjsdflksjfsdlkfjdlkfdsjfksldjfkljsdfkljsdfklsjdffff",
                UpdatedAt = DateTime.Parse("2021-01-11")
            };

            Assert.Contains(ValidateModel(cardDefination), v => v.MemberNames.Contains("Defination") &&
            v.ErrorMessage.Contains("Длина определения должна быть от 3 до 150 символов"));
        }
        [Fact]
        public void TestUseExampleStringLength()
        {
            Card cardUseExample = new Card
            {
                CreatedAt = DateTime.Parse("2021-01-11"),
                Word = "TestWord",
                Defination = "skfsjdklfsjdkfjsdklfjsdflksjfsdlkfjdlkfdsjfksldjfkljsdfkljsdfklsjdffff",
                UseExample = "asdaskdsklklsadasldklasdklaslkdsalkdalkldkaksldadfmljslfnslkfndsfk,ndfsdfjfdskfjdskfsjdklfsjdkfjsdklfjsdflksjfsdlkfjdlkfdsjfksldjfkljsdfkljsdfklsjdffff",
                UpdatedAt = DateTime.Parse("2021-01-11")
            };

            Assert.Contains(ValidateModel(cardUseExample), v => v.MemberNames.Contains("UseExample") &&
            v.ErrorMessage.Contains("Длина названия должна быть от 1 до 150 символов"));
        }
    }
}
