﻿using MemoryCards.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace MemoryCards.UnitTest.Models
{
   public class DeckTests : ModelTestsValidation
    {
        [Fact]
        public void RepeatDayChangeableTest() {
            Deck deckRepeatDay = new Deck
            { 
            RepeatDay = 0
            };

            Deck deckRepeatDay2 = new Deck
            {
                RepeatDay = 361
            };

            Deck deckRepeatDay3 = new Deck
            {
                RepeatDay = -3
            };

            Assert.Contains(ValidateModel(deckRepeatDay), d => d.MemberNames.Contains("RepeatDay") &&
          d.ErrorMessage.Contains("Недопустимое значение"));

            Assert.Contains(ValidateModel(deckRepeatDay2), d => d.MemberNames.Contains("RepeatDay") &&
         d.ErrorMessage.Contains("Недопустимое значение"));

            Assert.Contains(ValidateModel(deckRepeatDay3), d => d.MemberNames.Contains("RepeatDay") &&
         d.ErrorMessage.Contains("Недопустимое значение"));



        }

    }
}
