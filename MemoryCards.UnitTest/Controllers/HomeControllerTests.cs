﻿using MemoryCards.Controllers;
using MemoryCards.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace MemoryCards.UnitTest.Controllers
{
    public class HomeControllerTests
    {
        private HomeController _homeController;
        private MemoryContext _context;
        private UserManager<User> _userManager;
        private SignInManager<User> _signInManager;

        public static Mock<UserManager<User>> MockUserManager<User>() where User : class
        {
            var store = new Mock<IUserStore<User>>();
            var mgr = new Mock<UserManager<User>>(store.Object, null, null, null, null, null, null, null, null);
            mgr.Object.UserValidators.Add(new UserValidator<User>());
            mgr.Object.PasswordValidators.Add(new PasswordValidator<User>());
            return mgr;
        }
        public Mock<SignInManager<User>> MockSignInManager(Mock<UserManager<User>> userManager)
        {
            var contextAccesor = new Mock<IHttpContextAccessor>();
            var claimsFactory = new Mock<IUserClaimsPrincipalFactory<User>>();
            return new Mock<SignInManager<User>>(userManager.Object, contextAccesor.Object, claimsFactory.Object, null,
                null, null, null);
        }
        public HomeControllerTests()
        {
            var options = new DbContextOptionsBuilder<MemoryContext>()
                     .UseInMemoryDatabase(databaseName: "memorydbcards6999")
                     .Options;
            var mockLogger = new Mock<ILogger<HomeController>>();
            var userManagerMocker = MockUserManager<User>();
            var signInMocker = MockSignInManager(userManagerMocker);
            var deckBoxesController = new Mock<DeckBoxesController>();
            _userManager = userManagerMocker.Object;
            _context = new MemoryContext(options);
            _signInManager = signInMocker.Object;
            _homeController = new HomeController(mockLogger.Object, _context, _userManager, _signInManager);

        }

        [Fact]
        public async Task DeckBox()
        {
            var deckBoxes = new DeckBox[]
               {
               new DeckBox{ Title = "DefaultTitle", Description = "DefaultDescription"},
               };
            _context.DeckBoxes.AddRange(deckBoxes);
            await _context.SaveChangesAsync();
            int dbCOunt = _context.DeckBoxes.Count();
            var result = _homeController.DeckBox();
            List<DeckBox> models = _context.DeckBoxes.ToList();
            Assert.NotNull(result);
            Assert.Equal(dbCOunt, models.Count());
        }
        [Fact]
        public async Task Deck()
        {
            DeckBox deck = new DeckBox()
            {
                Id = 111,
                Title = "TestTitle",
                Description = "testdescription"
            };
            _context.Add(deck);
            _context.SaveChanges();
            var decks = new Deck[]
             {
            new Deck { RepeatDay = 2,   CountLearnCards = 2,
                LastSendMsgDate = DateTime.Parse("2010-09-01"), DeckBoxId=111 },
            new Deck { RepeatDay = 4, CountLearnCards = 1,
                LastSendMsgDate = DateTime.Parse("2012-09-01"),  DeckBoxId=111 },
            new Deck { RepeatDay = 8,   CountLearnCards = 4,
                LastSendMsgDate = DateTime.Parse("2013-09-01"), DeckBoxId=111 },
             };
            _context.Decks.AddRange(decks);
            await _context.SaveChangesAsync();

            var result = _homeController.Deck(111) as ViewResult;
            List<Deck> models = _context.Decks.Where(p => p.DeckBoxId == 111).ToList();
            Assert.NotNull(result);
            Assert.Equal(3, models.Count());
        }
        [Fact]
        public async Task DeckUserProfile()
        {
            DeckBox deck = new DeckBox()
            {
                Id = 222,
                Title = "TestTitle",
                Description = "testdescription"
            };
            _context.Add(deck);
            _context.SaveChanges();
            var decks = new Deck[]
             {
            new Deck { RepeatDay = 1,   CountLearnCards = 2,
                LastSendMsgDate = DateTime.Parse("2010-09-01"), DeckBoxId=222 },
            new Deck { RepeatDay = 3, CountLearnCards = 1,
                LastSendMsgDate = DateTime.Parse("2012-09-01"), DeckBoxId=222 },
            new Deck { RepeatDay = 4,   CountLearnCards = 4,
                LastSendMsgDate = DateTime.Parse("2013-09-01"), DeckBoxId=222 },
            new Deck { RepeatDay = 128,   CountLearnCards = 12,
                LastSendMsgDate = DateTime.Parse("2013-09-01"), DeckBoxId=333 }
             };
            _context.Decks.AddRange(decks);
            await _context.SaveChangesAsync();

            var result = _homeController.DeckUserProfile(222) as ViewResult;
            List<Deck> models = _context.Decks.Where(p => p.DeckBoxId == 222).ToList();
            Assert.NotNull(result);
            Assert.Equal(3, models.Count);
        }
        [Fact]
        public async Task Card()
        {
            var deck = new Deck
            {
                DeckBoxId = 444,
                RepeatDay = 1
            };
            _context.Decks.Add(deck);
            await _context.SaveChangesAsync();
            int cardBfrCount = _context.Cards.Count();
            var cards = new Card[]
              {
               new Card{ Word = "TestWord", Defination = "TestDefination", DeckId = deck.Id},
               new Card{ Word = "TestWord2", Defination = "TestDefination2", DeckId = deck.Id},
               new Card{ Word = "TestWord3", Defination = "TestDefination3", DeckId = deck.Id}
              };
            _context.Cards.AddRange(cards);
            await _context.SaveChangesAsync();

            var result = _homeController.Card(deck.Id);
            List<Card> models = _context.Cards.ToList();
            Assert.NotNull(result);
            Assert.Equal(cardBfrCount + 3, models.Count());
        }
        [Fact]
        public async Task CardUserProfile()
        {
            var deck = new Deck
            {
                DeckBoxId = 555,
                RepeatDay = 1
            };
            _context.Decks.Add(deck);
            await _context.SaveChangesAsync();
            int cardBfrCount = _context.Cards.Count();
            var cards = new Card[]
              {
               new Card{ Word = "TestWord", Defination = "TestDefination", DeckId = deck.Id},
               new Card{ Word = "TestWord2", Defination = "TestDefination2", DeckId = deck.Id},
               new Card{ Word = "TestWord3", Defination = "TestDefination3", DeckId = deck.Id}
              };
            _context.Cards.AddRange(cards);
            await _context.SaveChangesAsync();

            var result = _homeController.CardUserProfile(deck.Id);
            List<Card> models = _context.Cards.ToList();
            Assert.NotNull(result);
            Assert.Equal(cardBfrCount + 3, models.Count());
        }

        [Fact]
        public async Task SetDarkThemeTrue()
        {
            User user = new User
            {
                Email = "12@mail.ru",
                Login = "Alexander",
                Country = "Albania",
                PasswordHash = "dsad sa",
                DarkTheme = true
            };
            await _userManager.UpdateAsync(user);
            var result = _homeController.DeckBox();
            Assert.NotNull(result);
            Assert.True(user.DarkTheme);
        }
        [Fact]
        public async Task SetDarkThemeFalse()
        {
            User user = new User
            {
                Email = "12@mail.ru",
                Login = "Alexander",
                Country = "Albania",
                PasswordHash = "dsad sa",
                DarkTheme = false
            };
            await _userManager.UpdateAsync(user);
            var result = _homeController.DeckBox();
            Assert.NotNull(result);
            Assert.False(user.DarkTheme);
        }
    }
}
