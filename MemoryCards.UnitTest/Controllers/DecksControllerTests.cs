﻿using MemoryCards.Controllers;
using MemoryCards.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Localization;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace MemoryCards.UnitTest.Controllers
{
    public class DecksControllerTests
    {
        private MemoryContext _context;
        private DecksController decksController;
        private UserManager<User> _userManager;
        private readonly IStringLocalizer<DecksController> _localizer;
        public static Mock<UserManager<User>> MockUserManager<User>() where User : class
        {
            var store = new Mock<IUserStore<User>>();
            var mgr = new Mock<UserManager<User>>(store.Object, null, null, null, null, null, null, null, null);
            mgr.Object.UserValidators.Add(new UserValidator<User>());
            mgr.Object.PasswordValidators.Add(new PasswordValidator<User>());
            return mgr;
        }
        
        public DecksControllerTests()
        {
            var options = new DbContextOptionsBuilder<MemoryContext>()
                      .UseInMemoryDatabase(databaseName: "memorydbdecks")
                      .Options;

            var mock = new Mock<IStringLocalizer<DecksController>>();         
            var userManagerMocker = MockUserManager<User>();
            _userManager = userManagerMocker.Object;
            _context = new MemoryContext(options);
            decksController = new DecksController(_context, mock.Object, _userManager);
            
        }
        [Fact]
        public async Task Index()
        {
            DeckBox deck = new DeckBox()
            {
                Id = 1,
                Title = "TestTitle",
                Description = "testdescription"
            };
            _context.Add(deck);
            _context.SaveChanges();
            var decks = new Deck[]
             {
            new Deck { RepeatDay = 1,   CountLearnCards = 2,
                LastSendMsgDate = DateTime.Parse("2010-09-01"), DeckBoxId=1 },
            new Deck { RepeatDay = 3, CountLearnCards = 1,
                LastSendMsgDate = DateTime.Parse("2012-09-01"),  DeckBoxId=1},
            new Deck { RepeatDay = 4,   CountLearnCards = 4,
                LastSendMsgDate = DateTime.Parse("2013-09-01"), DeckBoxId=1 },
            new Deck { RepeatDay = 128,   CountLearnCards = 12,
                LastSendMsgDate = DateTime.Parse("2013-09-01"),  DeckBoxId=3 }
             };
            _context.Decks.AddRange(decks);
            await _context.SaveChangesAsync();
             
            var result = decksController.Index(1) as ViewResult;
            List<Deck> models = _context.Decks.Where(p => p.DeckBoxId == 1).ToList();
            Assert.NotNull(result);
            Assert.Equal(3, models.Count());

        }
        //test edit GET method
        [Fact]
        public async Task EditGet()
        {
            var Deck = new Deck
            {
                DeckBoxId = 4,
                RepeatDay = 32
            };
            _context.Decks.Add(Deck);
            await _context.SaveChangesAsync();
            
            var result = decksController.Edit(Deck.Id);
            Assert.NotNull(result);
        }

        //test edit POST method
        [Fact]
        public async Task EditPost()
        {

            var deckBoxes = new DeckBox[]
              {
               new DeckBox {Id=15, Title = "TestDict1",  UserId="testuserId1"},
               new DeckBox {Id=3, Title = "TestDict12",  UserId="testuserId2"},

              };
            _context.DeckBoxes.AddRange(deckBoxes);
            _context.SaveChanges();

            var decks = new Deck[]
            {
               new Deck { RepeatDay = 100, DeckBoxId=15 },
               new Deck { RepeatDay = 3, DeckBoxId=15},
               new Deck { RepeatDay = 4, DeckBoxId=15 },
              new Deck { RepeatDay = 1280, DeckBoxId=15 }
            };
            _context.Decks.AddRange(decks);
            _context.SaveChanges();

            Deck deck = _context.Decks.FirstOrDefault(p => p.RepeatDay == 100);
            int rptDay = deck.RepeatDay;

            
            var mockTempData = new Mock<ITempDataDictionary>();
            decksController.TempData = mockTempData.Object;
            var result = await decksController.Edit(deck.Id, deck);
            deck.RepeatDay = 4;
            Deck deck1 = _context.Decks.FirstOrDefault(p => p.Id == deck.Id);

           
            Assert.Equal(deck.Id, deck1.Id);
            Assert.NotEqual(rptDay, deck1.RepeatDay);

            Deck deck2 = _context.Decks.FirstOrDefault(p => p.RepeatDay == 1280);
            deck2.RepeatDay = 3;
            var result2 = await decksController.Edit(deck2.Id, deck2);

            var deck2rpday = _context.Decks.FirstOrDefault(p => p.RepeatDay == 1280);
            Assert.NotNull(deck2rpday);
        }
        [Fact]
        public async Task TestEditNotValidRepeadDay()
        {
            DeckBox deckBox = new DeckBox
            { 
                Id = 33,
                Title = "TestDict12", 
                UserId = "testuserId2" 
            };

            _context.DeckBoxes.Add(deckBox);
            _context.SaveChanges();

            var decks = new Deck[]
            {
               new Deck { RepeatDay = 500, DeckBoxId = deckBox.Id },
               new Deck { RepeatDay = 501,DeckBoxId = deckBox.Id},
               new Deck { RepeatDay = 504, DeckBoxId = deckBox.Id },
              new Deck { RepeatDay = 505, DeckBoxId = deckBox.Id }
            };
            _context.Decks.AddRange(decks);
            _context.SaveChanges();

            var testDeck = _context.Decks.FirstOrDefault(p => p.RepeatDay == 500);
             
            var mockTempData = new Mock<ITempDataDictionary>();
            decksController.TempData = mockTempData.Object;
             
            testDeck.RepeatDay = 501;
            var result = await decksController.Edit(testDeck.Id, testDeck);
           
            var check = _context.Decks.Where(p => p.RepeatDay == 501);
            Assert.IsType<ViewResult>(result);
            Assert.NotNull(result);
            Assert.Equal(1, check.Count());
        }
    }
}
