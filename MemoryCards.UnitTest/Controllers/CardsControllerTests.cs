﻿using MemoryCards.Controllers;
using MemoryCards.Models;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Moq;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace MemoryCards.UnitTest.Controllers
{
    public class CardsControllerTests : CleanUpAfterTests
    {
        private MemoryContext _context;
        private UserManager<User> _userManager;
        private Mock<IWebHostEnvironment> _webHostEnvironment;
        private CardsController _cardsController;
        public static Mock<UserManager<User>> MockUserManager<User>() where User : class
        {
            var store = new Mock<IUserStore<User>>();
            var mgr = new Mock<UserManager<User>>(store.Object, null, null, null, null, null, null, null, null);
            mgr.Object.UserValidators.Add(new UserValidator<User>());
            mgr.Object.PasswordValidators.Add(new PasswordValidator<User>());
            return mgr;
        }
        public CardsControllerTests()
        {
            var options = new DbContextOptionsBuilder<MemoryContext>()
                      .UseInMemoryDatabase(databaseName: "memorydbcards")
                      .Options;
            var userManagerMocker = MockUserManager<User>();
            _context = new MemoryContext(options);
            _webHostEnvironment = new Mock<IWebHostEnvironment>();
            _userManager = userManagerMocker.Object;
            _cardsController = new CardsController(_context, _webHostEnvironment.Object, _userManager);
        }


        [Fact]
        public async Task Index()
        {
            var deck = new Deck
            {
                DeckBoxId = 1,
                RepeatDay = 1
            };
            _context.Decks.Add(deck);
            await _context.SaveChangesAsync();
            int cardBfrCount = _context.Cards.Count();
            var cards = new Card[]
              {
               new Card{ Word = "TestWord", Defination = "TestDefination", DeckId = deck.Id},
               new Card{ Word = "TestWord2", Defination = "TestDefination2", DeckId = deck.Id},
               new Card{ Word = "TestWord3", Defination = "TestDefination3", DeckId = deck.Id}
              };
            _context.Cards.AddRange(cards);
            await _context.SaveChangesAsync();
            
            var result = _cardsController.Index(deck.Id, null);
            List<Card> models = _context.Cards.ToList();
            Assert.NotNull(result);
            Assert.Equal(cardBfrCount+3, models.Count());
        }
        //Test GET method Create card
        [Fact]
        public async Task CreateTestGet()
        {
            var deck = new Deck
            {
                DeckBoxId = 1,
                RepeatDay = 1
            };
            _context.Decks.Add(deck);
            await _context.SaveChangesAsync();
            var result = _cardsController.Create(deck.Id);
            Assert.NotNull(result);
        }
        //Test POST  method Create card
        [Fact]
        public async Task CreateTestPost()
        {
            var deck = new Deck
            {
                DeckBoxId = 1,
                RepeatDay = 1
            };
            _context.Decks.Add(deck);
            await _context.SaveChangesAsync();
            Card card = new Card
            {
                DeckId = deck.Id,
                Word = "TestWord",
                Defination = "TestDefination",
                UseExample = "TestUseExample"
            };
            int cardsCount = _context.Cards.Count();
            var result = _cardsController.Create(card, deck.Id);
            List<Card> models = _context.Cards.ToList();
            Assert.NotNull(result);
            Assert.Equal(cardsCount + 1, models.Count());
        }

        [Fact]
        public async Task DeleteCardTest()
        {
            var deck = new Deck
            {
                DeckBoxId = 1,
                RepeatDay = 1
            };
            _context.Decks.Add(deck);
            await _context.SaveChangesAsync();

            Card card = new Card
            {
                DeckId = deck.Id,
                Word = "TestWord",
                Defination = "TestDefination",
                UseExample = "TestUseExample"
            };

            _context.Cards.Add(card);
            await _context.SaveChangesAsync();
            int cardsCount = _context.Cards.Count();
            var result = _cardsController.Delete(card.Id, deck.Id);
            List<Card> models = _context.Cards.ToList();
            Assert.NotNull(result);
            Assert.Equal(cardsCount - 1, models.Count());
        }

        
        [Fact]
        public async Task KnowTest()
        {
            var deck = new Deck
            {
                DeckBoxId = 1,
                RepeatDay = 1
            };
            _context.Decks.Add(deck);
            await _context.SaveChangesAsync();
            var deck2 = new Deck
            {
                DeckBoxId = 1,
                RepeatDay = 2
            };
            _context.Decks.Add(deck2);
            await _context.SaveChangesAsync();
            Card card = new Card
            {
                DeckId = deck.Id,
                Word = "TestWord",
                Defination = "TestDefination",
                UseExample = "TestUseExample"
            };
            _context.Cards.Add(card);
            await _context.SaveChangesAsync();
            List<Deck> decks = _context.Decks.OrderBy(p => p.RepeatDay).ToList();
            Deck nextDeck = decks.FirstOrDefault(p => p.RepeatDay > deck.RepeatDay);
            if (nextDeck != null)
            {
                card.DeckId = nextDeck.Id;
                card.Deck = nextDeck;
                card.ShowCount += 1;
                card.MoveDate = DateTime.Now;
                if (card.ShowCount == 3)
                {
                    deck.CountLearnCards += 1;
                }
                _context.Update(card);
                await _context.SaveChangesAsync();
            }
            var result = deck.Cards.Count();
            var result2 = deck2.Cards.Count();
            var know = _cardsController.Know(card.Id);
            Assert.NotNull(know);
            Assert.Equal(0, result);
            Assert.Equal(1, result2);
        }
        [Fact]
        public async Task DontKnowTest()
        {
            var deck = new Deck
            {
                DeckBoxId = 1,
                RepeatDay = 1
            };
            _context.Decks.Add(deck);
            await _context.SaveChangesAsync();
            var deck2 = new Deck
            {
                DeckBoxId = 1,
                RepeatDay = 2
            };
            _context.Decks.Add(deck2);
            await _context.SaveChangesAsync();
            Card card = new Card
            {
                DeckId = deck2.Id,
                Word = "TestWord",
                Defination = "TestDefination",
                UseExample = "TestUseExample"
            };
            _context.Cards.Add(card);
            await _context.SaveChangesAsync();
            List<Deck> decks = _context.Decks.ToList();
            Deck prevDeck = decks.FirstOrDefault(p => p.RepeatDay < deck2.RepeatDay);
            if (prevDeck != null)
            {
                card.DeckId = prevDeck.Id;
                card.Deck = prevDeck;
                card.ShowCount += 1;
                card.MoveDate = DateTime.Now;
                if (card.ShowCount == 3)
                {
                    deck.CountLearnCards += 1;
                }
                _context.Update(card);
                await _context.SaveChangesAsync();
            }
            var result = card.DeckId;
            var dontKnow = _cardsController.DontKnow(card.Id);
            Assert.NotNull(dontKnow);
            Assert.Equal(1, result);
        }
        [Fact]
        public async Task EditPostTest()
        {
            cleanUpDir = @"..\..\..\..\MemoryCards.UnitTest\testfiles\images\card_images\";
            cleanUpSearchPattern = "*.jpg";

            var file = new Mock<IFormFile>();

            var fileName = "testcard.jpg";
            file.Setup(f => f.FileName).Returns(fileName).Verifiable();
            _webHostEnvironment.Setup(h => h.WebRootPath).Returns(@"..\..\..\..\MemoryCards.UnitTest\testfiles").Verifiable();
            var deckBox = new DeckBox
            {
                Id = 74,
                Title = "TestDict121",
                UserId = "testuserId21"
            };
            _context.DeckBoxes.Add(deckBox);
            await _context.SaveChangesAsync();

            var deck = new Deck
            {
                Id = 5,
                DeckBoxId = deckBox.Id,
                RepeatDay = 1
            };
            _context.Decks.Add(deck);
            await _context.SaveChangesAsync();

            Image cardimage = new Image
            {
                CardImageName = "testcard.jpg",
                CardId = 33,
                ImageFile = file.Object
            };
            _context.Images.Add(cardimage);
            await _context.SaveChangesAsync();

            var cards = new Card[]
            {
                new Card{ Id = 2548, DeckId =deck.Id , Word = "Test1", Defination = "TestDefination", UseExample = "TestUseExample" ,Image = cardimage

                },
                 new Card{ Id = 2448, DeckId =deck.Id , Word = "Test2", Defination = "TestDefination2", UseExample = "TestUseExample2" ,Image = cardimage

                },
            };
            _context.Cards.AddRange(cards);
            await _context.SaveChangesAsync();

            Card card = _context.Cards.FirstOrDefault(p => p.Id == 2448);

            var cardsController = new CardsController(_context, _webHostEnvironment.Object, _userManager);
            var edit = cardsController.Edit(card.Id, card, deck.Id, cardimage.ImageFile);
            int beforeCount = _context.Images.Count();            
            Assert.NotNull(edit);            
            Assert.Contains(fileName, cardimage.CardImageName);
            Assert.Equal(beforeCount, _context.Images.Count());
        }

        [Fact]
        public async Task TestGetEdit()
        {
            var options = new DbContextOptionsBuilder<MemoryContext>()
                              .UseInMemoryDatabase(databaseName: "memorycarddbTests2")
                              .Options;
            using (var context = new MemoryContext(options))
            {
                var deckBox = new DeckBox
                {
                    Id = 51,
                    Title = "TestDict12",
                    UserId = "testuserId2"
                };
                _context.DeckBoxes.Add(deckBox);
                await _context.SaveChangesAsync();

                var deck = new Deck
                {
                    Id = 17,
                    DeckBoxId = deckBox.Id,
                    RepeatDay = 1
                };
                _context.Decks.Add(deck);
                await _context.SaveChangesAsync();

                int id = 107;

                var cards = new Card[]
                {
                new Card{ Id = 205, DeckId = deck.Id , Word = "Test1", Defination = "TestDefination", UseExample = "TestUseExample"

                },
                 new Card{ Id = 107, DeckId = deck.Id , Word = "Test2", Defination = "TestDefination2", UseExample = "TestUseExample2"
                }
                }; 
                context.Cards.AddRange(cards);
                await context.SaveChangesAsync();

                var cardsController = new CardsController(context, _webHostEnvironment.Object, _userManager);
                var result = await cardsController.Edit(id) as ViewResult;

                List<Card>models = context.Cards.ToList();
                Assert.NotNull(result);
                Assert.Equal(2, models.Count());
            }
        }
    }
}
