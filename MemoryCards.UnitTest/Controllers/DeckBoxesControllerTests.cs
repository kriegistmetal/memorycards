﻿using MemoryCards.Controllers;
using MemoryCards.Models;
using MemoryCards.ViewModels;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Localization;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Security.Principal;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace MemoryCards.UnitTest.Controllers
{
    public class DeckBoxesControllerTests : CleanUpAfterTests
    {
        private DeckBoxesController _deckBoxesController;
        private MemoryContext _context;
        private Mock<IWebHostEnvironment> _webHostEnvironment;
        private UserManager<User> _userManager;
        private readonly IStringLocalizer<DecksController> _localizer;
        public static Mock<UserManager<User>> MockUserManager<User>() where User : class
        {
            var store = new Mock<IUserStore<User>>();
            var mgr = new Mock<UserManager<User>>(store.Object, null, null, null, null, null, null, null, null);
            mgr.Object.UserValidators.Add(new UserValidator<User>());
            mgr.Object.PasswordValidators.Add(new PasswordValidator<User>());
            return mgr;
        }
        public DeckBoxesControllerTests()
        {
            var options = new DbContextOptionsBuilder<MemoryContext>()
                      .UseInMemoryDatabase(databaseName: "memorydb")
                      .Options;
            var mock = new Mock<IStringLocalizer<DeckBoxesController>>();
            var userManagerMocker = MockUserManager<User>();
            var webHostEnvironment = new Mock<IWebHostEnvironment>();
            _userManager = userManagerMocker.Object;
            _context = new MemoryContext(options);
            _webHostEnvironment = new Mock<IWebHostEnvironment>();
            _deckBoxesController = new DeckBoxesController(_context, _userManager, _webHostEnvironment.Object);
        }

        [Fact]
        public async Task Index()
        {
            var deckBoxes = new DeckBox[]
              {
               new DeckBox{ Title = "TestTitle", Description = "TestDescription"},
               new DeckBox{ Title = "TestTitle2", Description = "TestDescription2"},
               new DeckBox{ Title = "TestTitle3", Description = "TestDescription3"}
              };
            _context.DeckBoxes.AddRange(deckBoxes);
            await _context.SaveChangesAsync();
            int dbCOunt = _context.DeckBoxes.Count();
            var result = _deckBoxesController.Index();
            List<DeckBox> models = _context.DeckBoxes.ToList();
            Assert.NotNull(result);
            Assert.Equal(dbCOunt, models.Count());
        }

        [Fact]
        public void TestGetCreate()
        {
            var result = _deckBoxesController.Create() as ViewResult;
            Assert.NotNull(result);
        }

        [Fact]
        public async Task TestCreate()
        {
            
                cleanUpDir = @"..\..\..\..\MemoryCards.UnitTest\testfiles\images\deckBox_images\";
                cleanUpSearchPattern = "*.jpg";


                var file = new Mock<IFormFile>();
                var fileName = "test2.jpg";
                file.Setup(f => f.FileName).Returns(fileName).Verifiable();
                _webHostEnvironment.Setup(h => h.WebRootPath).Returns(@"..\..\..\..\MemoryCards.UnitTest\testfiles").Verifiable();                

                var deckBox = new DeckBox()
                {
                    Title = "Test",
                    Description = "Test",
                    Rating = 1,
                    Private = false,
                    CreatedAt = DateTime.Parse("2020-11-11"),
                    DeckBoxcColor = "Red"
                };

                User user = new User
                {
                    Id = "TestId321",
                    Email = "test@test.test",
                    Login = "TestLogin",
                    PasswordHash = "TestPasswordHash"
                };
                _context.Users.Add(user);
                _context.SaveChanges();

                var identity = new Mock<IIdentity>();
                identity.SetupGet(i => i.IsAuthenticated).Returns(true);
                identity.SetupGet(i => i.Name).Returns("FakeUserName");
                var mockPrincipal = new Mock<ClaimsPrincipal>();
                mockPrincipal.Setup(x => x.Identity).Returns(identity.Object);
                var mockHttpContext = new Mock<HttpContext>();
                mockHttpContext.Setup(m => m.User).Returns(mockPrincipal.Object);
                var _UserManager = MockUserManager<User>();
                _UserManager.Setup(x => x.GetUserAsync(mockPrincipal.Object)).ReturnsAsync(user);
                var deckBoxesController = new DeckBoxesController(_context, _UserManager.Object, _webHostEnvironment.Object);

                deckBoxesController.ControllerContext = new ControllerContext();
                deckBoxesController.ControllerContext.HttpContext = new DefaultHttpContext()
                {
                    User = mockPrincipal.Object
                };
                deckBox.UserId = user.Id;
              
                int dbCount = _context.DeckBoxes.Count();              
                var result = await deckBoxesController.Create(deckBox, file.Object);               
                Assert.Equal(dbCount + 1, _context.DeckBoxes.Count());                
                var redirectToActionResult = Assert.IsType<RedirectToActionResult>(result);
                Assert.Equal("Index", redirectToActionResult.ActionName);             
        }
        [Fact]
        public async Task TestCreateNoValidTitle()
        {
           
                cleanUpDir = @"..\..\..\..\MemoryCards.UnitTest\testfiles\images\deckBox_images\";
                cleanUpSearchPattern = "*.jpg";

                var file = new Mock<IFormFile>();

                var fileName = "test2.jpg";
                file.Setup(f => f.FileName).Returns(fileName).Verifiable();
                _webHostEnvironment.Setup(h => h.WebRootPath).Returns(@"..\..\..\..\MemoryCards.UnitTest\testfiles").Verifiable();                

                var deckBox = new DeckBox()
                {
                    Id = 10,
                    Description = "TestDesctiption",
                    Rating = 1,
                    Private = false,
                    CreatedAt = DateTime.Parse("2020-11-11"),
                };

                User user = new User
                {
                    Id = "TestId123",
                    Email = "test@test.test",
                    Login = "TestLogin",
                    PasswordHash = "TestPasswordHash"
                };
                _context.Users.Add(user);
                _context.SaveChanges();

                var identity = new Mock<IIdentity>();
                identity.SetupGet(i => i.IsAuthenticated).Returns(true);
                identity.SetupGet(i => i.Name).Returns("FakeUserName");
                var mockPrincipal = new Mock<ClaimsPrincipal>();
                mockPrincipal.Setup(x => x.Identity).Returns(identity.Object);
                var mockHttpContext = new Mock<HttpContext>();
                mockHttpContext.Setup(m => m.User).Returns(mockPrincipal.Object);
                var _UserManager = MockUserManager<User>();
                _UserManager.Setup(x => x.GetUserAsync(mockPrincipal.Object)).ReturnsAsync(user);
                var deckBoxesController = new DeckBoxesController(_context, _UserManager.Object, _webHostEnvironment.Object);

                deckBoxesController.ControllerContext = new ControllerContext();
                deckBoxesController.ControllerContext.HttpContext = new DefaultHttpContext()
                {
                    User = mockPrincipal.Object
                };
                deckBoxesController.ModelState.AddModelError("Title", "Is required");
                deckBox.UserId = user.Id;
                int deckboxCount = _context.DeckBoxes.Count();
                int imgCount = _context.Images.Count();
                var result = await deckBoxesController.Create(deckBox, file.Object);                
                Assert.Equal(deckboxCount, deckboxCount);
                Assert.IsType<ViewResult>(result);
                Assert.Equal(_context.Images.Count(), imgCount);
                Assert.NotNull(result);
                 
            
        }
        [Fact]
        public async Task TestGetEdit()
        {
            var options = new DbContextOptionsBuilder<MemoryContext>()
                              .UseInMemoryDatabase(databaseName: "memorydbTests2")
                              .Options;
            using (var context = new MemoryContext(options))
            {
                int id = 2;

                var deckBoxes = new DeckBox[]
                {
                new DeckBox{ Id = 1, Title = "Test1", Description = "Test1" },
                new DeckBox{ Id = 2, Title = "Test2", Description = "Test2" },
                };
                context.DeckBoxes.AddRange(deckBoxes);
                await context.SaveChangesAsync();
                var deckBoxesController = new DeckBoxesController(context, _userManager, _webHostEnvironment.Object);
                var result = await deckBoxesController.Edit(id) as ViewResult;

                List<DeckBox> models = context.DeckBoxes.ToList();
                Assert.NotNull(result);
                Assert.Equal(2, models.Count());
            }
        }

        [Fact]
        public async Task TestEdit()
        {
            var options = new DbContextOptionsBuilder<MemoryContext>()
                              .UseInMemoryDatabase(databaseName: "memorydbTests1")
                              .Options;
            using (var context = new MemoryContext(options))
            {
                cleanUpDir = @"..\..\..\..\MemoryCards.UnitTest\testfiles\images\deckBox_images\";
                cleanUpSearchPattern = "*.jpg";

                var file = new Mock<IFormFile>();

                var fileName = "test.jpg";
                file.Setup(f => f.FileName).Returns(fileName).Verifiable();
                _webHostEnvironment.Setup(h => h.WebRootPath).Returns(@"..\..\..\..\MemoryCards.UnitTest\testfiles").Verifiable();


                Image image = new Image
                {
                    DeckBoxImageName = "test.jpg",
                    DeckBoxId = 10,
                    ImageFile = file.Object
                };
                context.Images.Add(image);
                context.SaveChanges();

                var deckBoxes = new DeckBox[]
                {
                new DeckBox{ Id = 1, Title = "Test1", Description = "Test1", Image = image

                },
                new DeckBox{ Id = 2, Title = "Test2", Description = "Test2", Image = image

                },
                };
                context.DeckBoxes.AddRange(deckBoxes);
                await context.SaveChangesAsync();

                DeckBox deckBox = context.DeckBoxes.FirstOrDefault(p => p.Id == 2);

                var deckBoxesController = new DeckBoxesController(context, _userManager, _webHostEnvironment.Object);
                var result = await deckBoxesController.Edit(deckBox.Id, deckBox, image.ImageFile);

                int beforeCount = context.Images.Count();
                Assert.NotNull(result);
                Assert.Contains(fileName, image.DeckBoxImageName);
                Assert.Equal(beforeCount, context.Images.Count());
                var redirectToActionResult = Assert.IsType<RedirectToActionResult>(result);
                Assert.Equal("Index", redirectToActionResult.ActionName);
            }
        }

        [Fact]
        public async Task DeleteTest()
        {             
            var deckBox = new DeckBox
            {   
                Id = 57800,                 
                Title = "TestTitle",
                Description = "TestDescription"                   
            };
            _context.DeckBoxes.Add(deckBox);
            _context.SaveChanges();
            int dbxCount = _context.DeckBoxes.Count();

            var deck = new Deck
            {
                DeckBoxId = deckBox.Id,
                RepeatDay = 1
            };
            _context.Decks.Add(deck);
            _context.SaveChanges();
            int deckCount = _context.Decks.Count();

            var card = new Card
            {
                DeckId=deck.Id,
                Word= "TestCardWord",
                Defination = "TestDefination",
                UseExample = "TestUseExample"
            };
            _context.Cards.Add(card);
            _context.SaveChanges();
            int cardCount = _context.Cards.Count();
              
            var deckBoxesController = new DeckBoxesController(_context,  _userManager,_webHostEnvironment.Object);
            var result = await deckBoxesController.Delete(deckBox.Id);
            var redirectToActionResult = Assert.IsType<RedirectToActionResult>(result);

            Assert.Equal("Index", redirectToActionResult.ActionName);
            Assert.Equal(dbxCount - 1, _context.DeckBoxes.Count());
            Assert.Equal(deckCount - 1, _context.Decks.Count());
            Assert.Equal(cardCount - 1, _context.Cards.Count());
             

        }
        [Fact]
        public void CreateDeckDefaultTest()
        {          
            DeckBox deckBox = new DeckBox
            {
                Title = "TestTitle1",
                Description = "TestDesctiption1"
            };
            _context.DeckBoxes.Add(deckBox);
            _context.SaveChanges();

            int decksCount = _context.Decks.Count();
            var deckBoxesController = new DeckBoxesController(_context, _userManager, _webHostEnvironment.Object);
            deckBoxesController.CreateDecksDefault(deckBox);
            Deck deck = _context.Decks.FirstOrDefault(p => p.DeckBoxId == deckBox.Id);
            Assert.Equal(decksCount + 8, _context.Decks.Count());
            Assert.NotNull(deck);
            
        }
        [Fact]
        public async Task GetDeckBoxTest()
        {
            var options = new DbContextOptionsBuilder<MemoryContext>()
                              .UseInMemoryDatabase(databaseName: "memorydb2")
                              .Options;
            using (var context = new MemoryContext(options))
            {

                DeckBox deckBox = new DeckBox
                {
                    Id = 1,
                    UserId="thisUserId",
                    Title = "ENGLISH",
                    Description = "LEARN ENGLISH",
                    Rating =5,
                    DeckBoxcColor = "#ffff"
                };
                context.DeckBoxes.Add(deckBox);
                context.SaveChanges();
                int deckBoxesCount = context.DeckBoxes.Count();

                Deck deck = new Deck
                {
                    DeckBoxId = deckBox.Id,
                    RepeatDay = 1,
                };
                context.Decks.Add(deck);
                context.SaveChanges();
                int DecksCount = context.Decks.Count();

                Card card = new Card
                {
                    Id = 2,
                    DeckId = deck.Id,
                    Defination = "TestDefination",
                    Word = "TestWord",
                    UseExample = "TestExample"
                };
                context.Cards.Add(card);
                context.SaveChanges();
                int cardsCount = context.Cards.Count();

                User user = new User
                {
                    Id = "TestId",
                    Email = "test@test.test",
                    Login = "TestLogin",
                    PasswordHash = "TestPasswordHash"
                };
                context.Users.Add(user);
                context.SaveChanges();

              
                
                var identity = new Mock<IIdentity>();
                identity.SetupGet(i => i.IsAuthenticated).Returns(true);
                identity.SetupGet(i => i.Name).Returns("FakeUserName");
                var mockPrincipal = new Mock<ClaimsPrincipal>();
                mockPrincipal.Setup(x => x.Identity).Returns(identity.Object);
                var mockHttpContext = new Mock<HttpContext>();
                mockHttpContext.Setup(m => m.User).Returns(mockPrincipal.Object);
                var _UserManager = MockUserManager<User>();
                _UserManager.Setup(x => x.GetUserAsync(mockPrincipal.Object)).ReturnsAsync(user);
                var deckBoxesController = new DeckBoxesController(context, _UserManager.Object , _webHostEnvironment.Object);
              
                
                deckBoxesController.ControllerContext = new ControllerContext();
                deckBoxesController.ControllerContext.HttpContext = new DefaultHttpContext()
                {
                    User = mockPrincipal.Object
                };
                var result = await deckBoxesController.GetDeckBox(deckBox.Id);
                var redirectToActionResult = Assert.IsType<RedirectToActionResult>(result);
                var checkcard = context.Cards.FirstOrDefault(p => p.Id == 2);
                Assert.Equal("Index", redirectToActionResult.ActionName);
                Assert.Equal(DecksCount+8, context.Decks.Count());
                Assert.Equal(deckBoxesCount+1, context.DeckBoxes.Count());
                Assert.Equal(cardsCount + 1, context.Cards.Count());
                Assert.NotNull(checkcard);
            }
        }
        [Fact]
        public void  RatingSortTest()
        {
            var options = new DbContextOptionsBuilder<MemoryContext>()
                           .UseInMemoryDatabase(databaseName: "memorydb65")
                           .Options;
            using (var context = new MemoryContext(options))
            {
                List<RatingSortViewModel> model = new List<RatingSortViewModel>();
                DeckBox deckBox = new DeckBox
                {
                    Id = 1,
                    UserId = "thisUserId",
                    Title = "ENGLISH",
                    Description = "LEARN ENGLISH",
                    Rating = 5,
                    DeckBoxcColor = "#ffff"
                };
                context.DeckBoxes.Add(deckBox);
                context.SaveChanges();

                RatingSortViewModel ratingSort = new RatingSortViewModel
                {
                    DeckBoxId = deckBox.Id,
                    NewRating = deckBox.Rating - 2

                };
                model.Add(ratingSort);
                
                var deckBoxesController = new DeckBoxesController(context, _userManager, _webHostEnvironment.Object);
                var result =  deckBoxesController.RatingSort(model);
                Assert.IsType<JsonResult>(result);
                Assert.NotNull(result);
            }
        }
        [Fact]
        public void CreateCardsGetDeckBoxTest()
        {
            var options = new DbContextOptionsBuilder<MemoryContext>()
                           .UseInMemoryDatabase(databaseName: "memorydb655")
                           .Options;
            using (var context = new MemoryContext(options))
            {
                List<Deck> decks = new List<Deck>
                {
                new Deck{ RepeatDay = 1, DeckBoxId =  1}, new Deck{ RepeatDay = 1, DeckBoxId =  1},
                new Deck{ RepeatDay = 4, DeckBoxId =  1}, new Deck{ RepeatDay = 8, DeckBoxId =  1},
                new Deck{ RepeatDay = 16, DeckBoxId =  1}, new Deck{ RepeatDay = 32, DeckBoxId =  1},
                new Deck{ RepeatDay = 64, DeckBoxId =  1}, new Deck{ RepeatDay = 128, DeckBoxId =  1}
                };
                DeckBox deckBox = new DeckBox
                {
                    Id = 1,
                    UserId = "thisUserId",
                    Title = "ENGLISH",
                    Description = "LEARN ENGLISH",
                    Rating = 5,
                    DeckBoxcColor = "#ffff",
                    Decks = decks
                };
                var deckBoxesController = new DeckBoxesController(context, _userManager, _webHostEnvironment.Object);
                deckBoxesController.CreateCardsGetDeckBox(deckBox, decks);
                Deck deck = deckBox.Decks.FirstOrDefault(p => p.RepeatDay == 1);

                Card card = new Card
                {
                    Id = 2,
                    DeckId = deck.Id,
                    Defination = "TestDefination",
                    Word = "TestWord",
                    UseExample = "TestExample"
                };

                Card cardNew = new Card
                {
                    Word = card.Word,
                    Defination = card.Defination,
                    DeckId = deck.Id,
                    UseExample = card.UseExample,
                    ShowCount = 0,
                };
                context.Add(cardNew);
                context.SaveChanges();
                int cardsCount = context.Cards.Count();

                Image CardImg = new Image
                {
                    CardId = cardNew.Id,
                    CardImageName = "Test"
                };
                Image newCardImg = new Image
                {
                    CardId = cardNew.Id,
                    CardImageName = CardImg.CardImageName
                };
                context.Add(newCardImg);
                context.SaveChanges();
                int imagesCount = context.Images.Count();

                Assert.NotNull(cardNew);
                Assert.NotNull(newCardImg);
                Assert.Equal(cardsCount, context.Cards.Count());
                Assert.Equal(imagesCount, context.Images.Count());
            }
        }
        [Fact]
        public async Task Base64EncodeTest()
        {
            var options = new DbContextOptionsBuilder<MemoryContext>()
                              .UseInMemoryDatabase(databaseName: "memorydb55")
                              .Options;
            using (var context = new MemoryContext(options))
            {
                DeckBox deckBox = new DeckBox
                {
                    Id = 1,
                    UserId = "thisUserId",
                    Title = "ENGLISH",
                    Description = "LEARN ENGLISH",
                    Rating = 5,
                    DeckBoxcColor = "#ffff"
                };
                context.DeckBoxes.AddRange(deckBox);
                context.SaveChanges();

                DeckBox deckbox = await context.DeckBoxes.FindAsync(deckBox.Id);
                string link = $"{deckbox.Id}|{DateTime.Now.AddHours(2)}";
                var linkbytes = System.Text.Encoding.UTF8.GetBytes(link);
                string encUrl = System.Convert.ToBase64String(linkbytes);

                var controller = new DeckBoxesController(context, _userManager, _webHostEnvironment.Object);

                string url = "/DeckBoxes/DeckboxView?deckboxid=" + encUrl;

                var result = controller.Base64Encode(deckBox.Id);
                Assert.NotNull(result);
                Assert.IsNotType<JsonResult>(url);
            }
        }
        [Fact]
        public async Task DeckboxViewTest()
        {
            var options = new DbContextOptionsBuilder<MemoryContext>()
                              .UseInMemoryDatabase(databaseName: "memorydb555")
                              .Options;
            using (var context = new MemoryContext(options))
            {
                DeckBox deckBox = new DeckBox
                {
                    Id = 1,
                    UserId = "thisUserId",
                    Title = "ENGLISH",
                    Description = "LEARN ENGLISH",
                    Rating = 5,
                    DeckBoxcColor = "#ffff"
                };
                context.DeckBoxes.AddRange(deckBox);
                context.SaveChanges();
                string link = $"{deckBox.Id}|{DateTime.Now.AddHours(2)}";
                var linkbytes = System.Text.Encoding.UTF8.GetBytes(link);
                string deckboxid = System.Convert.ToBase64String(linkbytes);

                var deckBoxesController = new DeckBoxesController(context, _userManager, _webHostEnvironment.Object);
                var result = await deckBoxesController.DeckboxView(deckboxid);

                Assert.NotNull(result);
                Assert.IsType<ViewResult>(result);
            }
        }
    }
}
