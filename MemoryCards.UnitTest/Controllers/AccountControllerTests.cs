﻿using MemoryCards.Controllers;
using MemoryCards.Models;
using MemoryCards.ViewModels;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Routing;
using Microsoft.AspNetCore.Routing;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Localization;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace MemoryCards.UnitTest.Controllers
{
    public class AccountControllerTests : CleanUpAfterTests
    {
        private AccountController _accountController;
        private MemoryContext _context;
        private SignInManager<User> _signInManager;
        private UserManager<User> _userManager;
        private Mock<IWebHostEnvironment> _webHostEnvironment;
        private Mock<IServiceScopeFactory> _serviceScopeFactory;
        private readonly IStringLocalizer<AccountController> _localizer;
        public static Mock<UserManager<User>> MockUserManager<User>() where User : class
        {
            var store = new Mock<IUserStore<User>>();
            var mgr = new Mock<UserManager<User>>(store.Object, null, null, null, null, null, null, null, null);
            mgr.Object.UserValidators.Add(new UserValidator<User>());
            mgr.Object.PasswordValidators.Add(new PasswordValidator<User>());
            return mgr;
        }
        public Mock<SignInManager<User>> MockSignInManager(Mock<UserManager<User>> userManager)
        {
            var contextAccesor = new Mock<IHttpContextAccessor>();
            var claimsFactory = new Mock<IUserClaimsPrincipalFactory<User>>();
            return new Mock<SignInManager<User>>(userManager.Object, contextAccesor.Object, claimsFactory.Object, null,
                null, null, null);
        }
        public AccountControllerTests()
        {
            var options = new DbContextOptionsBuilder<MemoryContext>()
                      .UseInMemoryDatabase(databaseName: "memorydb")
                      .Options;

            var userManagerMocker = MockUserManager<User>();
            var signInMocker = MockSignInManager(userManagerMocker);
            var webHostEnvironment = new Mock<IWebHostEnvironment>();
            var mock = new Mock<IStringLocalizer<AccountController>>();
            _serviceScopeFactory = new Mock<IServiceScopeFactory>();
            _signInManager = signInMocker.Object;
            _userManager = userManagerMocker.Object;
            _context = new MemoryContext(options);
            _webHostEnvironment = new Mock<IWebHostEnvironment>();     
            _accountController = new AccountController(_userManager, _signInManager, _context, _webHostEnvironment.Object, _serviceScopeFactory.Object, mock.Object);

        }

        [Fact]
        public async Task UserProfilesPaginationTest()
        {
            var options = new DbContextOptionsBuilder<MemoryContext>()
                              .UseInMemoryDatabase(databaseName: "memorydbtests")
                              .Options;
            using (var context = new MemoryContext(options))
            {
                var users = new User[]
                {
              new User { Email = "12@mail.ru",   Login = "Alexander",
                   Country = "Albania", PasswordHash="dsadsa"},
              new User { Email = "12@mail.ru",   Login = "SomeUser",
                   Country = "Argentina", PasswordHash="dsadsa"},
              new User { Email = "12@mail.ru",   Login = "Petya",
                   Country = "Kyrgyzstan", PasswordHash="dsadsa"},
              new User { Email = "12@mail.ru",   Login = "Andrey",
                   Country = "Albania", PasswordHash="dsadsa"},
              new User { Email = "12@mail.ru",   Login = "Andrey",
                   Country = "Kazakhstan", PasswordHash="dsadsa"},
                new User { Email = "12@mail.ru",   Login = "Andrey",
                   Country = "China", PasswordHash="dsadsa"},
                };
                context.Users.AddRange(users);
                context.SaveChanges();
                User user = new User
                {
                    Id = "TestId3212251",
                    Email = "test1@test.test",
                    Login = "TestLogin1",
                    PasswordHash = "TestPasswordHash"
                };
                context.Users.Add(user);
                context.SaveChanges();

                var identity = new Mock<IIdentity>();
                identity.SetupGet(i => i.IsAuthenticated).Returns(true);
                identity.SetupGet(i => i.Name).Returns("FakeUserName");
                var mockPrincipal = new Mock<ClaimsPrincipal>();
                mockPrincipal.Setup(x => x.Identity).Returns(identity.Object);
                var mockHttpContext = new Mock<HttpContext>();
                mockHttpContext.Setup(m => m.User).Returns(mockPrincipal.Object);
                var _UserManager = MockUserManager<User>();
                _UserManager.Setup(x => x.GetUserAsync(mockPrincipal.Object)).ReturnsAsync(user);
                var mock = new Mock<IStringLocalizer<AccountController>>();
                var accountcontroller = new AccountController(_UserManager.Object, _signInManager, context, _webHostEnvironment.Object, _serviceScopeFactory.Object, mock.Object);
                accountcontroller.ControllerContext = new ControllerContext();
                accountcontroller.ControllerContext.HttpContext = new DefaultHttpContext()
                {
                    User = mockPrincipal.Object
                };
                var result = await accountcontroller.UserProfiles(null, null);
                var viewResult = Assert.IsType<ViewResult>(result);
                var model = Assert.IsAssignableFrom<UsersListViewModel>(viewResult.ViewData.Model);
                Assert.Equal(6, model.PageViewModel.Count());
                Assert.NotNull(result);
            }
        }

        [Fact]
        public async Task UserProfilesSearchTest()
        {
            var options = new DbContextOptionsBuilder<MemoryContext>()
                              .UseInMemoryDatabase(databaseName: "memorydb")
                              .Options;
            using (var context = new MemoryContext(options))
            {
                string search = "Kot";
                var users = new User[]
                {
              new User { Email = "Kotalka@mail.ru",   Login = "Alexander",
                   Country = "Albania", PasswordHash="dsadsa"},
              new User { Email = "12@mail.ru",   Login = "Kotik",
                   Country = "Argentina", PasswordHash="dsadsa"},
              new User { Email = "12@mail.ru",   Login = "Petya",
                   Country = "Kotaklizm", PasswordHash="dsadsa"},
              new User { Email = "12@mail.ru",   Login = "Andrey",
                   Country = "Albania", PasswordHash="dsadsa"},
              new User { Email = "12@mail.ru",   Login = "Koteyka",
                   Country = "Kotovasia", PasswordHash="dsadsa"},
                new User { Email = "Kot@mail.ru",   Login = "Andrey",
                   Country = "China", PasswordHash="dsadsa"},
                };
                context.Users.AddRange(users);
                context.SaveChanges();
                User user = new User
                {
                    Id = "TestId3212243",
                    Email = "test1@test.test",
                    Login = "TestLogin1",
                    PasswordHash = "TestPasswordHash"
                };
                context.Users.Add(user);
                context.SaveChanges();
                var identity = new Mock<IIdentity>();
                identity.SetupGet(i => i.IsAuthenticated).Returns(true);
                identity.SetupGet(i => i.Name).Returns("FakeUserName");
                var mockPrincipal = new Mock<ClaimsPrincipal>();
                mockPrincipal.Setup(x => x.Identity).Returns(identity.Object);
                var mockHttpContext = new Mock<HttpContext>();
                mockHttpContext.Setup(m => m.User).Returns(mockPrincipal.Object);
                var _UserManager = MockUserManager<User>();
                _UserManager.Setup(x => x.GetUserAsync(mockPrincipal.Object)).ReturnsAsync(user);
                var mock = new Mock<IStringLocalizer<AccountController>>();
                var accountcontroller = new AccountController(_UserManager.Object, _signInManager, context, _webHostEnvironment.Object, _serviceScopeFactory.Object, mock.Object);
                accountcontroller.ControllerContext = new ControllerContext();
                accountcontroller.ControllerContext.HttpContext = new DefaultHttpContext()
                {
                    User = mockPrincipal.Object
                };


                var result = await accountcontroller.UserProfiles(null, search) as PartialViewResult;
                var partialViewResult = Assert.IsType<PartialViewResult>(result);
                var model = Assert.IsAssignableFrom<UsersListViewModel>(partialViewResult.ViewData.Model);
                Assert.NotNull(result);
                Assert.True(model.Users.Where(u => u.Login.Contains(search)).Count() >= 2);
                Assert.True(model.Users.Where(u => u.Country.Contains(search)).Count() >= 2);
                Assert.True(model.Users.Where(u => u.Email.Contains(search)).Count() >= 2);
                Assert.Equal(5, model.PageViewModel.Count());
            }
        }

        [Fact]
        public async Task UserProfilesViewResultTest()
        {
            User user = new User
            {
                Id = "TestId321221",
                Email = "test1@test.test",
                Login = "TestLogin1",
                PasswordHash = "TestPasswordHash"
            };
            _context.Users.Add(user);
            _context.SaveChanges();
            var identity = new Mock<IIdentity>();
            identity.SetupGet(i => i.IsAuthenticated).Returns(true);
            identity.SetupGet(i => i.Name).Returns("FakeUserName");
            var mockPrincipal = new Mock<ClaimsPrincipal>();
            mockPrincipal.Setup(x => x.Identity).Returns(identity.Object);
            var mockHttpContext = new Mock<HttpContext>();
            mockHttpContext.Setup(m => m.User).Returns(mockPrincipal.Object);
            var _UserManager = MockUserManager<User>();
            _UserManager.Setup(x => x.GetUserAsync(mockPrincipal.Object)).ReturnsAsync(user);
            var mock = new Mock<IStringLocalizer<AccountController>>();
            var accountcontroller = new AccountController(_UserManager.Object, _signInManager, _context, _webHostEnvironment.Object, _serviceScopeFactory.Object, mock.Object);

            accountcontroller.ControllerContext = new ControllerContext();
            accountcontroller.ControllerContext.HttpContext = new DefaultHttpContext()
            {
                User = mockPrincipal.Object
            };

            var result = await accountcontroller.UserProfiles(null, null);
            Assert.NotNull(result);
            Assert.IsType<ViewResult>(result);
        }

        [Fact]
        public async Task ChangeProfileTest()
        {
            var options = new DbContextOptionsBuilder<MemoryContext>()
                              .UseInMemoryDatabase(databaseName: "memorydb4")
                              .Options;
            using (var context = new MemoryContext(options))
            {
                User user = new User
                {
                    Email = "12@mail.ru",
                    Login = "Alexander",
                    Country = "Albania",
                    PasswordHash = "dsadsa"
                };

                context.Users.Add(user);
                context.SaveChanges();
                string countrybeforChange = user.Country;
                string loginbeforChange = user.Login;
                ChangeProfileUserViewModel changeProfile = new ChangeProfileUserViewModel
                {
                    Id = user.Id,
                    Email = user.Email,
                    Login = "Ivan",
                    Country = "Kazakhstan",
                    NewPassword = "dsadsa",
                    PasswordConfirm = "dsadsa"
                };
                var mock = new Mock<IStringLocalizer<AccountController>>();
                var accountcontroller = new AccountController(_userManager, _signInManager, context, _webHostEnvironment.Object, _serviceScopeFactory.Object,mock.Object);
                User user1 = context.Users.FirstOrDefault(p => p.Id == user.Id);
                var result = await accountcontroller.ChangeProfile(changeProfile);
                Assert.Equal(user.Id, user1.Id);
                Assert.NotEqual(countrybeforChange, changeProfile.Country);
                Assert.NotEqual(loginbeforChange, changeProfile.Login);
            }
        }

        [Fact]
        public async Task RegisterNewAccountTest()
        {
            var options = new DbContextOptionsBuilder<MemoryContext>()
                              .UseInMemoryDatabase(databaseName: "memorydbRegister")
                              .Options;
            using (var context = new MemoryContext(options))
            {
                var _UserManager = MockUserManager<User>();
                _UserManager.Setup(x => x.CreateAsync(It.IsAny<User>(), It.IsAny<string>())).ReturnsAsync(IdentityResult.Success);
                var _signinmanager = MockSignInManager(_UserManager);
                var mock = new Mock<IStringLocalizer<AccountController>>();
                var registerViewmodel = new RegisterViewModel
                {
                    Email = "pastebin@gmail.com",
                    Login = "Nurik",
                    Country = "Iran",
                    Password = "123",
                    PasswordConfirm = "123"
                };
               
                var accountcontroller = new AccountController(_UserManager.Object, _signinmanager.Object, context,
                   _webHostEnvironment.Object, _serviceScopeFactory.Object, mock.Object);

                var urlHelperMock = new Mock<IUrlHelper>();
                urlHelperMock
                  .Setup(x => x.Action(It.IsAny<UrlActionContext>()))
                  .Returns((UrlActionContext uac) =>
                    $"{uac.Controller}/{uac.Action}#{uac.Fragment}?"
                    + string.Join("&", new RouteValueDictionary(uac.Values).Select(p => p.Key + "=" + p.Value)));
                accountcontroller.Url = urlHelperMock.Object;
                accountcontroller.ControllerContext.HttpContext = new DefaultHttpContext();

                var result = accountcontroller.Register();
                Assert.NotNull(result);
                Assert.IsType<ViewResult>(result);
            }
        }

        [Fact]
        public async Task EmailisAlreadyTakenTest()
        {
            var options = new DbContextOptionsBuilder<MemoryContext>()
                              .UseInMemoryDatabase(databaseName: "memorydbTakenEmail")
                              .Options;

            using (var context = new MemoryContext(options))
            {
                User oldUser = new User
                {
                    Email = "Nurik@gmail.com",
                    Login = "Alexander",
                    Country = "Albania",
                    PasswordHash = "dsadsa"
                };
                context.Users.Add(oldUser);
                context.SaveChanges();

                var registerViewmodel = new RegisterViewModel
                {
                    Email = oldUser.Email,
                    Login = "Nurik",
                    Country = "Albania",
                    Password = "P123456a*",
                    PasswordConfirm = "P123456a*"
                };

                _accountController.ModelState.AddModelError("Email", "Email уже используется");
                var result = await _accountController.Register(registerViewmodel);
                Assert.IsType<ViewResult>(result);
            }
        }
        [Fact]
        public void GetLoginTest()
        {
            var options = new DbContextOptionsBuilder<MemoryContext>()
                           .UseInMemoryDatabase(databaseName: "memorydbValid")
                           .Options;
            using (var context = new MemoryContext(options))
            {
                string returnUrl = "";
                User user = new User
                {
                    Email = "12@mail.ru",
                    Login = "Alexander",
                    Country = "Albania",
                    PasswordHash = "dsadsa"
                };

                context.Users.AddRange(user);
                context.SaveChanges();

                var loginViewModel = new LoginViewModel
                {
                    Email = user.Email,
                    Password = user.PasswordHash,
                };
                var mock = new Mock<IStringLocalizer<AccountController>>();
                var accountcontroller = new AccountController(_userManager, _signInManager, context, _webHostEnvironment.Object, _serviceScopeFactory.Object, mock.Object);
                _signInManager.PasswordSignInAsync(loginViewModel.Email, loginViewModel.Password, true, false);
                var result = accountcontroller.Login(returnUrl);
                Assert.IsType<ViewResult>(result);
            };
        }
        [Fact]
        public async Task LoginTest()
        {
            var options = new DbContextOptionsBuilder<MemoryContext>()
                           .UseInMemoryDatabase(databaseName: "memorydbValid")
                           .Options;
            using (var context = new MemoryContext(options))
            {

                User user = new User
                {
                    Email = "12@mail.ru",
                    Login = "Alexander",
                    Country = "Albania",
                    PasswordHash = "dsadsa"
                };

                context.Users.AddRange(user);
                context.SaveChanges();

                var loginViewModel = new LoginViewModel
                {
                    Email = user.Email,
                    Password = user.PasswordHash,
                };
                var mock = new Mock<IStringLocalizer<AccountController>>();
                var accountcontroller = new AccountController(_userManager, _signInManager, context, _webHostEnvironment.Object, _serviceScopeFactory.Object,mock.Object);
                await _signInManager.PasswordSignInAsync(loginViewModel.Email, loginViewModel.Password, true, false);
                var result = await accountcontroller.Login(loginViewModel);
                Assert.IsType<ViewResult>(result);
            };
        }

        [Fact]
        public async Task LoginInvalidUser()
        {
            var options = new DbContextOptionsBuilder<MemoryContext>()
                          .UseInMemoryDatabase(databaseName: "memorydbInvalid")
                          .Options;
            using (var context = new MemoryContext(options))
            {
                User user = new User
                {
                    Email = "Nurik@gmail.com",
                    Login = "Alexander",
                    Country = "Albania",
                    PasswordHash = "dsadsa"
                };

                context.Users.Add(user);
                context.SaveChanges();
                var mock = new Mock<IStringLocalizer<AccountController>>();
                var accountcontroller = new AccountController(_userManager, _signInManager, context, _webHostEnvironment.Object, _serviceScopeFactory.Object, mock.Object);
                var loginViewModel = new LoginViewModel
                {
                    Email = "Nurik@gmail.com",
                    Password = "P123456a*"
                };
                accountcontroller.ModelState.AddModelError("", "Неправильный email и (или) пароль");
                var result = await _accountController.Login(loginViewModel);
                Assert.IsType<ViewResult>(result);
            };
        }

        [Fact]
        public async Task LogOffTest()
        {
            var result = await _accountController.LogOff();
            var redirectToAction = Assert.IsType<RedirectToActionResult>(result);
            Assert.NotNull(result);
            Assert.Equal("DeckBox", redirectToAction.ActionName);
        }

        [Fact]
        public void ForgotPasswordGetTest()
        {
            var result = _accountController.ForgotPassword() as ViewResult;
            Assert.NotNull(result);
        }
        [Fact]
        public async Task ForgotPasswordTest()
        {
            var options = new DbContextOptionsBuilder<MemoryContext>()
                              .UseInMemoryDatabase(databaseName: "memorydbfgpass")
                              .Options;
            using (var context = new MemoryContext(options))
            {
                User user = new User
                {
                    Email = "12@mail.ru",
                    Login = "Alexander",
                    Country = "Albania",
                    PasswordHash = "dsadsa"
                };

                context.Users.Add(user);
                context.SaveChanges();

                ForgotPasswordViewModel forgotPasswordViewModel = new ForgotPasswordViewModel
                {
                    Email = user.Email
                };
                var mock = new Mock<IStringLocalizer<AccountController>>();
                var accountcontroller = new AccountController(_userManager, _signInManager, context, _webHostEnvironment.Object, _serviceScopeFactory.Object, mock.Object);
                User user1 = context.Users.FirstOrDefault(p => p.Id == user.Id);
                var result = await accountcontroller.ForgotPassword(forgotPasswordViewModel);
                Assert.NotNull(result);
                Assert.IsType<RedirectToActionResult>(result);
            }
        }

        [Fact]
        public void ResetPasswordGetTest()
        {
            var result = _accountController.ResetPassword() as ViewResult;
            Assert.NotNull(result);
        }

        [Fact]
        public async Task ResetPasswordTest()
        {
            var options = new DbContextOptionsBuilder<MemoryContext>()
                              .UseInMemoryDatabase(databaseName: "memorydbreset")
                              .Options;
            using (var context = new MemoryContext(options))
            {
                ResetPasswordViewModel model = new ResetPasswordViewModel
                {
                    Email = "TestEmail@gmail.com",
                    Password = "123",
                    ConfirmPassword = "123"
                };
                var mock = new Mock<IStringLocalizer<AccountController>>();
                var accountcontroller = new AccountController(_userManager, _signInManager, context, _webHostEnvironment.Object, _serviceScopeFactory.Object,mock.Object);

                var result = await accountcontroller.ResetPassword(model);
                Assert.NotNull(result);
                Assert.IsType<RedirectToActionResult>(result);
            }
        }

        [Fact]
        public async Task ChangeAvatar()
        {
            var options = new DbContextOptionsBuilder<MemoryContext>()
                              .UseInMemoryDatabase(databaseName: "memorydb4")
                              .Options;
            using (var context = new MemoryContext(options))
            {
                User user = new User
                {
                    Email = "12@mail.ru",
                    Login = "Alexander",
                    Country = "Albania",
                    PasswordHash = "dsadsa"
                };
                context.Users.Add(user);
                context.SaveChanges();
                var file = new Mock<IFormFile>();
                var fileName = "somepng.png";
                file.Setup(f => f.FileName).Returns(fileName).Verifiable();
                _webHostEnvironment.Setup(h => h.WebRootPath).Returns(@"\..\..\..MemoryCards.UnitTest\testfiles\images\avatars\").Verifiable();
                Image image = new Image
                {
                    Avatar = "somepng.png",
                    UserId = user.Id,
                    ImageFile = file.Object
                };
                context.Images.Add(image);
                context.SaveChanges();

                ChangeProfileUserViewModel changeProfile = new ChangeProfileUserViewModel
                {
                    Id = user.Id,
                    Email = user.Email,
                    Login = user.Login,
                    Country = user.Country,
                    Avatar = image.Avatar,
                    Image = image
                };
                var mock = new Mock<IStringLocalizer<AccountController>>();
                var accountcontroller = new AccountController(_userManager, _signInManager, context, _webHostEnvironment.Object, _serviceScopeFactory.Object, mock.Object);
                Image image1 = context.Images.FirstOrDefault(p => p.UserId == user.Id);
                var result = await accountcontroller.ChangeProfile(changeProfile);
                Assert.NotNull(image.Avatar);
                Assert.Equal(image.Avatar, image1.Avatar);
                Assert.Equal(image.UserId, image1.UserId);
                Assert.Equal(image.ImageFile, image1.ImageFile);
            }
        }

        [Fact]
        public Task ProfileTest()
        {
            User user = new User { Id = "TestId", Email = "Test@mail.ru",
                Login = "TestLogin", Country = "Russia"
            };
            Image image = new Image { Avatar = "test",
                UserId = "TestId", User = user
            };
            _context.Images.AddRange(image);
            _context.Users.AddRange(user);
            _context.SaveChanges();

            Image imagenew = _context.Images.FirstOrDefault(i => i.UserId == user.Id);
            ChangeProfileUserViewModel changeProfile = new ChangeProfileUserViewModel
            {
                Id = user.Id, Email = user.Email, Login = user.Login,
                Country = user.Country, Avatar = imagenew.Avatar, ImageId = imagenew.Id
            };
            var result = _accountController.Profile();

            Assert.IsAssignableFrom<ChangeProfileUserViewModel>(changeProfile);
            Assert.NotNull(result);
            return Task.CompletedTask;
        }

        [Fact]
        public Task UserProfileTest()
        {
            string id = "TestsId";
            User user = new User
            {
                Id = "TestsId",
                Email = "Test@mail.ru",
                Login = "TestName",
                Country = "Russia",
                PasswordHash = "testpasword"
            };

            _context.Users.AddRange(user);
            _context.SaveChanges();


            User users = _userManager.Users.FirstOrDefault(p => p.Id == id);
            var result = _accountController.UserProfile(id);
            Assert.NotNull(result);
            return Task.CompletedTask;
        }

        [Fact]
        public async Task DeleteAvatarTest()
        {
            User user = new User
            {
                Email = "12@mail.ru",
                Login = "Alexander",
                Country = "Albania",
                PasswordHash = "dsadsa"
            };
            _context.Users.Add(user);
            _context.SaveChanges();

            cleanUpDir = @"..\..\..\..\MemoryCards.UnitTest\testfiles\images\avatars\";
            cleanUpSearchPattern = "*.jpg";

            var file = new Mock<IFormFile>();
            var fileName = "somepng.png";
            file.Setup(f => f.FileName).Returns(fileName).Verifiable();
            _webHostEnvironment.Setup(h => h.WebRootPath).Returns(@"..\..\..\..\MemoryCards.UnitTest\testfiles").Verifiable();
            Image image = new Image
            {
                Avatar = "somesspng.png",
                UserId = user.Id,
                ImageFile = file.Object
            };
            _context.Images.Add(image);
            _context.SaveChanges();

            int beforeCount = _context.Images.Count();
            var mock = new Mock<IStringLocalizer<AccountController>>();
            var accountcontroller = new AccountController(_userManager, _signInManager, _context, _webHostEnvironment.Object, _serviceScopeFactory.Object,mock.Object);

            var result = await accountcontroller.DeleteAvatar(image.Id);
            Assert.Equal(beforeCount - 1, _context.Images.Count());
            var redirectToActionResult = Assert.IsType<RedirectToActionResult>(result);
            Assert.Equal("Profile", redirectToActionResult.ActionName);
        }
        [Fact]
        public async Task ConfirmUserEmail()
        {
            var options = new DbContextOptionsBuilder<MemoryContext>()
                               .UseInMemoryDatabase(databaseName: "memorydbConfrim")
                               .Options;
            using (var context = new MemoryContext(options))
            {
                string code = "CfDJ8C1mNZBQpKJPtrtuwnkyxrOWAF5YfomvVX961AmaJn1t0joO6fsagsdsdfTLDQ0xI7C11qQ4IBNRbP4te92m89HnDmV04WK25UPoMoY2raEp655pSfqrXfVjukpIsipeLMYUJ + KKN2 / cv1IUOJZUiSV / VkzbsKUnkphjaHSr + s / fB4QjghmufsorSzgg8hQuv0LGrBkkk6YyOZQE5X1qvFyIEkQoAgV0MySHjTxOxIQFPDwlkvJ3I5QJ2O / b7pPb2C6mfAg / Ltw";

                User user = new User
                {
                    Email = "12@mail.ru",
                    Login = "Alexander",
                    Country = "Albania",
                    PasswordHash = "dsadsa"
                };
                context.Users.AddRange(user);
                context.SaveChanges();

                var identity = new Mock<IIdentity>();
                identity.SetupGet(i => i.IsAuthenticated).Returns(true);
                identity.SetupGet(i => i.Name).Returns("FakeUserName");
                var mockPrincipal = new Mock<ClaimsPrincipal>();
                mockPrincipal.Setup(x => x.Identity).Returns(identity.Object);
                var mockHttpContext = new Mock<HttpContext>();
                mockHttpContext.Setup(m => m.User).Returns(mockPrincipal.Object);
                var _UserManager = MockUserManager<User>();
                 _UserManager.Setup(x => x.GetUserAsync(mockPrincipal.Object)).ReturnsAsync(user);
                _UserManager.Setup(x => x.ConfirmEmailAsync(user, code));
                var mock = new Mock<IStringLocalizer<AccountController>>();
                var accountcontroller = new AccountController(_UserManager.Object, _signInManager, context, _webHostEnvironment.Object, _serviceScopeFactory.Object,mock.Object);

                accountcontroller.ControllerContext = new ControllerContext();
                accountcontroller.ControllerContext.HttpContext = new DefaultHttpContext()
                {
                    User = mockPrincipal.Object
                };
                var result = await accountcontroller.ConfirmEmail(user.Id, code);
               Assert.IsType<ViewResult>(result);
                Assert.NotNull(result);



            }    

        }
    }
}
