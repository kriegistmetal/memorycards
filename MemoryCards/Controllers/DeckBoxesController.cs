﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using MemoryCards;
using MemoryCards.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using System.IO;
using Microsoft.AspNetCore.Http;
using MemoryCards.Solid;
using MemoryCards.ViewModels;
using System.Globalization;
using Microsoft.Extensions.Localization;
using System.Threading;

namespace MemoryCards.Controllers
{
    [Authorize]
     
    public class DeckBoxesController : Controller
    {
        private readonly MemoryContext _context;
        private readonly UserManager<User> _userManager;
        private readonly IWebHostEnvironment _webHostEnvironment;
        private readonly IStringLocalizer<DecksController> _localizer;

        public DeckBoxesController(MemoryContext context, UserManager<User> userManager, IWebHostEnvironment webHostEnvironment)
        {
            _context = context;
            _userManager = userManager;
            _webHostEnvironment = webHostEnvironment;
        }

        // GET: DeckBoxes
        public async Task<IActionResult> Index()
        {          
            string userId =  _userManager.GetUserId(User);
            List<DeckBox> memoryContext = 
                await _context.DeckBoxes.Include("User")
                .Where(d => d.UserId == userId).Include(p=> p.Image)
                .Include(p=>p.Decks).ThenInclude(p=>p.Cards)
                .ThenInclude(p=>p.Image).OrderBy(p=>p.Rating).ToListAsync();
            return View(memoryContext);
        }

        // GET: DeckBoxes/Create
        public IActionResult Create()
        {             
            return View();
        }
        // POST: DeckBoxes/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Title,Description,Rating,Private,CreatedAt,DeckBoxcColor")] DeckBox deckBox, IFormFile imageDb)
        {
            if (ModelState.IsValid)
            {             
                Image dbImage = new Image();
                if (imageDb != null)
                {
                    string folderPath = Path.Combine(_webHostEnvironment.WebRootPath, "images", "deckBox_images");
                    var filename = ImageAbillities.UploadImage(imageDb, folderPath);

                    dbImage.DeckBoxImageName = filename;
                    dbImage.DeckBoxId = deckBox.Id;
                    deckBox.Image = dbImage;
                    deckBox.DeckBoxcColor = null;
                }
                if(deckBox.DeckBoxcColor == null)
                {
                    deckBox.DeckBoxcColor = "#999999";
                }
                User user = await _userManager.GetUserAsync(User);
                List<DeckBox> deckBoxes = _context.DeckBoxes.Where(d => d.UserId == user.Id).ToList();
                deckBox.Rating = deckBoxes.Count + 1;
                deckBox.UserId = user.Id;
                _context.Add(deckBox);
                await _context.SaveChangesAsync();
                CreateDecksDefault(deckBox);
                return RedirectToAction(nameof(Index));

            }
            return View(deckBox);
        }

        // GET: DeckBoxes/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            string userId = _userManager.GetUserId(User);
            var deckBox = await _context.DeckBoxes.Include(p=>p.Image).FirstOrDefaultAsync(p=>p.Id == id && p.UserId == userId);
            if (deckBox == null)
            {
                return View("AccessError"); 
            }
 
            return View(deckBox);
        }

        // POST: DeckBoxes/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Title,Description,UserId,Rating,Private,UpdatedAt,DeckBoxcColor,CreatedAt,Image")]
        DeckBox deckBox, IFormFile Imagedb)
        {
            if (id != deckBox.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                Image imaged = await _context.Images.FirstOrDefaultAsync(p => p.DeckBoxId == deckBox.Id);                
         
                try
                {
                    if (Imagedb != null && imaged!=null)
                    {
                        List<DeckBox> deckBoxes = _context.DeckBoxes
                        .Where(p => p.Image.DeckBoxImageName == imaged.DeckBoxImageName
                        && p.Id != deckBox.Id).ToList();
                        if (imaged!= null && deckBoxes.Count == 0)
                        {
                            string folderPathLstImg = Path.Combine(_webHostEnvironment.WebRootPath, "images", "deckBox_images");
                            ImageAbillities.DeleteImage(imaged.DeckBoxImageName, folderPathLstImg);
                        }
                        string folderPath = Path.Combine(_webHostEnvironment.WebRootPath, "images", "deckBox_images");
                        var filename = ImageAbillities.UploadImage(Imagedb, folderPath);
                        deckBox.DeckBoxcColor = null;
                        Image dbImage = new Image();
                        dbImage.DeckBoxId = deckBox.Id;
                        dbImage.DeckBoxImageName = filename;
                        deckBox.Image = dbImage;
                        _context.Remove(imaged);
                    }
                    else if(Imagedb!=null && imaged == null)
                    {
                        string folderPath = Path.Combine(_webHostEnvironment.WebRootPath, "images", "deckBox_images");
                        var filename = ImageAbillities.UploadImage(Imagedb, folderPath);
                        Image dbImage = new Image();
                        dbImage.DeckBoxId = deckBox.Id;
                        dbImage.DeckBoxImageName = filename;
                        deckBox.Image = dbImage;
                        deckBox.DeckBoxcColor = null;
                    }
                   else if (Imagedb == null && imaged!=null && deckBox.DeckBoxcColor!=null)
                    {
                     List<DeckBox> deckBoxes = _context.DeckBoxes
                     .Where(p => p.Image.DeckBoxImageName == imaged.DeckBoxImageName
                     && p.Id != deckBox.Id).ToList();
                        if (imaged != null && deckBoxes.Count == 0)
                           {
                               string folderPathLstImg = Path.Combine(_webHostEnvironment.WebRootPath, "images", "deckBox_images");
                               ImageAbillities.DeleteImage(imaged.DeckBoxImageName, folderPathLstImg);
                            }
                        deckBox.Image = null;
                        if (imaged != null)
                            _context.Images.Remove(imaged);                  
                    }                  
                    _context.Update(deckBox);
                    _context.SaveChanges();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!DeckBoxExists(deckBox.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
              
                return RedirectToAction(nameof(Index));
            }

            return View(deckBox);
        }


        public async Task<IActionResult> Delete(int id)
        {
            DeckBox deckbox = _context.DeckBoxes.Include(p => p.Image).FirstOrDefault(p => p.Id == id);
            if (deckbox.Image != null)
            {
                List<DeckBox> deckBoxes = _context.DeckBoxes.Where(p => p.Image.DeckBoxImageName == deckbox.Image.DeckBoxImageName
             && p.Id != deckbox.Id).ToList();
                if (deckbox.Image.DeckBoxImageName != null && deckBoxes.Count == 0)
                {
                    string folderPathLstImg = Path.Combine(_webHostEnvironment.WebRootPath, "images", "deckBox_images");
                    ImageAbillities.DeleteImage(deckbox.Image.DeckBoxImageName, folderPathLstImg);
                    Image image = _context.Images.FirstOrDefault(p => p.DeckBoxId == deckbox.Id);
                    _context.Remove(image);
                    await _context.SaveChangesAsync();
                }
            }

            List<Deck> decks = await _context.Decks.Include(p => p.Cards).ThenInclude(p => p.Image).Where(p => p.DeckBoxId == id).ToListAsync();
            List<Card> cards = new List<Card>();
            foreach (var c in decks)
            {
                if (c.Cards != null)
                {
                    cards.AddRange(c.Cards);
                }
            }
            if (cards != null)
            {
                foreach (var c in cards)
                {
                    List<Card> cardsfordelete = new List<Card>();
                    if (c.Image != null)
                    {
                        cardsfordelete = _context.Cards.Where(p => p.Image.CardImageName == c.Image.CardImageName && p.Id != c.Id).ToList();
                    }
                    if (c.Image != null && cardsfordelete.Count == 0)
                    {
                        string folderPathLstImg = Path.Combine(_webHostEnvironment.WebRootPath, "images", "card_images");
                        ImageAbillities.DeleteImage(c.Image.CardImageName, folderPathLstImg);
                        Image image = _context.Images.FirstOrDefault(p => p.CardId == c.Id);
                        _context.Remove(image);
                        await _context.SaveChangesAsync();
                    }
                }
            }
            _context.DeckBoxes.Remove(deckbox);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }
        public void CreateDecksDefault(DeckBox deckBox)
        {
            int repeatDay = 1;
            
            for (int i = 0; i < 8; i++)
            {
                Deck deck = new Deck 
                { 
                    RepeatDay = repeatDay, 
                    DeckBoxId = deckBox.Id,
                    LastSendMsgDate = DateTime.Now.AddDays(repeatDay)

                };
                
                repeatDay += repeatDay;                
                _context.Add(deck);
                _context.SaveChanges();
            }
        }

        [HttpGet]
        public async Task<IActionResult> GetDeckBox(int? id)
        {

            User user = await _userManager.GetUserAsync(User);

            DeckBox deckBox = 
                await _context.DeckBoxes
                .Include(p=>p.Image)
                .FirstOrDefaultAsync(p=>p.Id==id);

            DeckBox deckBoxNew = new DeckBox
            {
                UserId = user.Id,
                Title = deckBox.Title,
                Description = deckBox.Description,
                Rating = deckBox.Rating,
                DeckBoxcColor = deckBox.DeckBoxcColor,
            };
            
            List<Deck> decks = await _context.Decks
                .Include(d => d.DeckBox)
                    .Where(d => d.DeckBoxId == deckBox.Id)
                .Include(d => d.Cards).ToListAsync();

            if (ModelState.IsValid)
            {
                _context.Add(deckBoxNew);
                await _context.SaveChangesAsync();
                if(deckBox.Image!=null)
                {
                    Image newdbimg = new Image
                    {
                        DeckBoxId = deckBoxNew.Id,
                        DeckBoxImageName = deckBox.Image.DeckBoxImageName

                    };
                    _context.Add(newdbimg);
                    await _context.SaveChangesAsync();
                }

                CreateDecksDefault(deckBoxNew);
                CreateCardsGetDeckBox(deckBoxNew, decks);

                return RedirectToAction(nameof(Index));
            }

            return RedirectToAction(nameof(Index));
        }

        public void CreateCardsGetDeckBox(DeckBox deckBoxNew, List<Deck> decks)
        {
            foreach (Deck deckDeckBox in decks)
            {
                List<Card> cards = _context.Cards.Include(c => c.Deck).Include(p=>p.Image)
                    .Where(c => c.DeckId == deckDeckBox.Id).ToList();
                Deck deck = deckBoxNew.Decks.FirstOrDefault(p => p.RepeatDay == 1);

                foreach (Card card in cards)
                {
                    var image = _context.Images.FirstOrDefault(x => x.CardId == card.Id);
                     
                    Card cardNew = new Card
                    {

                        Word = card.Word,
                        Defination = card.Defination,
                        DeckId = deck.Id,
                        UseExample = card.UseExample,
                        ShowCount = 0, 
                    };
                    _context.Add(cardNew);
                    _context.SaveChanges();
                    if(card.Image != null)
                    {
                        Image newCardImg = new Image
                        {
                            CardId = cardNew.Id,
                            CardImageName = card.Image.CardImageName
                        };
                        _context.Add(newCardImg);
                        _context.SaveChanges();
                    }
                    
                }
            }
        }
        public async Task<IActionResult> Base64Encode(int deckboxid)
        {
            DeckBox deckbox = await _context.DeckBoxes.FindAsync(deckboxid);
            string link = $"{deckbox.Id}|{DateTime.Now.AddHours(2).ToString("dd.MM.yyyy HH:mm:ss")}|{DateTime.Now.AddHours(2).ToString("MM.dd.yyyy HH:mm:ss")}";
            var linkbytes = System.Text.Encoding.UTF8.GetBytes(link);
            string encUrl = System.Convert.ToBase64String(linkbytes);
            string callbackUrl = Url.Action(
                        "DeckboxView",
                        "DeckBoxes",
                        new { deckboxid = encUrl },
                        protocol: HttpContext.Request.Scheme);

            return Ok(Json("{\"URL\": " + $" \"{callbackUrl} \"" + "}"));
        }
        public async Task<IActionResult> DeckboxView(string deckboxid)
        {
            try
            {
                var base64EncodedBytes = System.Convert.FromBase64String(deckboxid);
                deckboxid = System.Text.Encoding.UTF8.GetString(base64EncodedBytes);
                string[] parts = deckboxid.Split('|');

                string culture = Thread.CurrentThread.CurrentCulture.IetfLanguageTag;
                if (culture == "ru")
                {
                    if (Convert.ToDateTime(parts[1]) >= DateTime.Now)
                    {
                        DeckBox deckBox = await
                        _context.DeckBoxes
                        .Include(p => p.User)
                        .Include(p => p.Image)
                        .Include(p => p.Decks)
                        .ThenInclude(p => p.Cards)
                       .FirstOrDefaultAsync(d => d.Id == Convert.ToInt32(parts[0]));
                        return View(deckBox);
                    }
                }
                else
                {
                    if (Convert.ToDateTime(parts[2]) >= DateTime.Now)
                    {
                        DeckBox deckBox = await
                        _context.DeckBoxes
                        .Include(p => p.User)
                        .Include(p => p.Image)
                        .Include(p => p.Decks)
                        .ThenInclude(p => p.Cards)
                       .FirstOrDefaultAsync(d => d.Id == Convert.ToInt32(parts[0]));
                        return View(deckBox);
                    }

                }
                TempData["warning"] = _localizer["warningText"];
                return RedirectToAction(nameof(Index));

            }
            catch
            {
                return View("AccessError");
            }

        }
        private bool DeckBoxExists(int id)
        {
            return _context.DeckBoxes.Any(e => e.Id == id);
        }
        [HttpPost]
        public IActionResult RatingSort([FromBody]List<RatingSortViewModel> ratings)
        {
            foreach(var rating in ratings)
            {
                DeckBox deckBox = _context.DeckBoxes.Find(rating.DeckBoxId);
                deckBox.Rating = rating.NewRating;
                _context.Update(deckBox);
            }
            _context.SaveChanges();

            return Json("Ok");
        }
    }
}
