﻿using MemoryCards.Models;
using MemoryCards.Services;
using MemoryCards.Solid;
using MemoryCards.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Localization;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace MemoryCards.Controllers
{
    public class AccountController : Controller
    {
        private readonly UserManager<User> _userManager;
        private readonly SignInManager<User> _signInManager;
        private readonly MemoryContext _context;
        private readonly IWebHostEnvironment _webHostEnvironment;
        private readonly IServiceScopeFactory _serviceScopeFactory;
        private readonly IStringLocalizer<AccountController> _localizer;
        public AccountController(UserManager<User> userManager, SignInManager<User> signInManager,
            MemoryContext context, IWebHostEnvironment webHostEnvironment, IServiceScopeFactory serviceScopeFactory, IStringLocalizer<AccountController> localizer)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _context = context;
            _webHostEnvironment = webHostEnvironment;
            _serviceScopeFactory = serviceScopeFactory;
            _localizer = localizer;
        }
        [HttpGet]
        public IActionResult Register()
        {
            CountryList();
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Register(RegisterViewModel model)
        {    
            if (ModelState.IsValid)
              {
                  User user = new User { Email = model.Email, UserName = model.Email, Login = model.Login , Country = model.Country};
                  var result = await _userManager.CreateAsync(user, model.Password);
                  if (result.Succeeded)
                  {
                    var code = await _userManager.GenerateEmailConfirmationTokenAsync(user);
                    var callbackUrl = Url.Action(
                        "ConfirmEmail",
                        "Account",
                        new { userId = user.Id, code = code },
                        protocol: HttpContext.Request.Scheme);
                    EmailService emailService = new EmailService(_serviceScopeFactory);
                    if (model.Country == "Russia" || model.Country == "Kyrgyzstan")
                    {
                        await emailService.SendEmailAsync(model.Email, "Confirm your account",
                        $"<div class<h3 style='text-align: center;font-size: 27px;color: black; font-weight: 600;'>Почти готово!</h3> " +
                        $"<p style='font-size: 15px;text-align: center;color: black;padding-top: 1%;'>Здраствуйте, {user.Login}!" +
                        $"<p style='padding-left: 24%;padding-right: 24%;font-size: 14px;text-align: center;color: black;padding-top: 1%;'>Что бы завершить настройку аккаунта " +
                        $"и приступить к работе с приложением MemoryCard, потвердите правильность указанного адреса электронной почты</p>" +
                        $"<a style='font-size: 1rem;border: none;padding: 0.375rem 0.3rem;text-align: center;cursor: pointer;margin-top: 4%;display: inline-block;color: rgb(255, 255, 255);" +
                        $"background-color: rgb(0, 123, 255);border-radius: 0.25rem;width: 45%;text-decoration: none;" +
                        $" margin-bottom: 12%;'<a href='{callbackUrl}'>Подтвердить адрес электронной почты</a>");
                        return new ContentResult
                        {
                            ContentType = "text/html; charset=utf-8",
                            Content = "<html><body><h2 style='text-align: center; padding-top='300px'>Чтобы завершить регистрацию, пожалуйста, подтвердите адрес вашей электронной почты<h2></body></html>"
                        };
                    }
                    else
                    {
                        await emailService.SendEmailAsync(model.Email, "Confirm your account",
                        $"<div class<h3 style='text-align: center;font-size: 27px;color: black; font-weight: 600;'>Almost done!</h3> " +
                        $"<p style='font-size: 15px;text-align: center;color: black;padding-top: 1%;'>Hello, {user.Login}!" +
                        $"<p style='padding-left: 24%;padding-right: 24%;font-size: 14px;text-align: center;color: black;padding-top: 1%;'>To complete the account setup " +
                        $"and start working with the MemoryCard application, confirm that the specified email address is correct</p>" +
                        $"<a style='font-size: 1rem;border: none;padding: 0.375rem 0.3rem;text-align: center;cursor: pointer;margin-top: 4%;display: inline-block;color: rgb(255, 255, 255);" +
                        $"background-color: rgb(0, 123, 255);border-radius: 0.25rem;width: 45%;text-decoration: none;" +
                        $" margin-bottom: 12%;'<a href='{callbackUrl}'>Confirm your email address</a>");
                        return new ContentResult
                        {
                            ContentType = "text/html;",
                            Content = "<html><body><h2 style='text-align: center; margin-top='30px'>" +
                            "Please confirm your email to complete your registration<h2></body></html>"
                        };

                    }
                    
                  }
                  else
                  {
                      foreach (var error in result.Errors)
                      {
                          ModelState.AddModelError(string.Empty, error.Description);
                      }
                  }
            }
            CountryList();
            return View(model);
        }

        [HttpGet]
        [AllowAnonymous]
        public async Task<IActionResult> ConfirmEmail(string userId, string code)
        {
            if (userId == null || code == null)
            {
                return View("AccessError");
            }
            var user = await _userManager.FindByIdAsync(userId);
            if (user == null)
            {
                return View("AccessError");
            }
            var result = await _userManager.ConfirmEmailAsync(user, code);
            if (result.Succeeded)
                return RedirectToAction("Login", "Account");
            else
                return View("AccessError");
        }

        [HttpGet]
        public IActionResult Login(string returnUrl = null)
        {
            return View(new LoginViewModel { ReturnUrl = returnUrl });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Login(LoginViewModel model)
        {
          
            if (ModelState.IsValid)
            {
                User user = await _userManager.FindByEmailAsync(model.Email);
                if (user == null)
                {
                    ModelState.AddModelError("", "Неправильный логин и (или) пароль");
                }
                else
                {
                    if (!await _userManager.IsEmailConfirmedAsync(user))
                    {
                        ModelState.AddModelError(string.Empty, "Вы не подтвердили свой email");
                        return View(model);
                    }
                    Microsoft.AspNetCore.Identity.SignInResult result =
                            await _signInManager.PasswordSignInAsync(user,
                                model.Password, model.RememberMe, false);
                    if (result.Succeeded)
                    {
                        if (!string.IsNullOrEmpty(model.ReturnUrl) && Url.IsLocalUrl(model.ReturnUrl))
                        {
                            return Redirect(model.ReturnUrl);
                        }
                        else
                        {
                            return RedirectToAction("Index", "DeckBoxes");
                        }
                    }

                    else
                    {
                        ModelState.AddModelError("", "Неправильный логин и (или) пароль");
                    }
                }
            }
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> LogOff()
        {
            await _signInManager.SignOutAsync();
            return RedirectToAction("DeckBox", "Home");
        }

        [AcceptVerbs("Get", "Post")]
        public IActionResult CheckEmail(string email)
        {
            var users = _userManager.Users.Where(e => e.Email == email).ToList();
            var userid = _userManager.GetUserId(User);
            User customUser = _userManager.Users.FirstOrDefault(p => p.Id == userid);

            users.Remove(customUser);
            if (users.Count == 0)
                return Json(true);
            return Json(false);
        }

        [Authorize]
        public async Task<IActionResult> UserProfiles(int? pageNumber, string searchString, int pageSize = 20)
        {
            User user = await _userManager.GetUserAsync(User);
            IQueryable<User> users = _context.Users.Where(p=>p.Id != user.Id && p.Email != "seleniumdefaulttestuser@mail.ru");
            if (!String.IsNullOrEmpty(searchString) && searchString.Length > 2)
            {
                users = users.Where(u =>
                  u.Login.Contains(searchString) && u.Id!=user.Id ||
                  u.Country.Contains(searchString) && u.Id != user.Id ||
                  u.Email.Contains(searchString) && u.Id!=user.Id);
            }
            
            UsersListViewModel viewModel = new UsersListViewModel
            {
                Users = users,
                PageViewModel = await PaginatedListViewModel<User>.CreateAsync(users.Include("Image"), pageNumber ?? 1, pageSize)
            };
            if (!String.IsNullOrEmpty(searchString)) return PartialView(viewModel);
            else return View(viewModel);
        }

        [Authorize]
        public async Task<IActionResult> UserProfile(string id)
        {
            var user = await _userManager.Users.Include(p => p.Image).FirstOrDefaultAsync(e => e.Id == id);
            user.DeckBoxes = _context.DeckBoxes.Include(d => d.User).ThenInclude(d => d.Image).Include(d => d.Decks).Include(d => d.Image).Where(p => p.UserId == user.Id).ToList();
            return View(user);
        }

        [Authorize]
        [HttpGet]
        public async Task<IActionResult> Profile()
        {
            User user = await _userManager.GetUserAsync(User);
            Image image = _context.Images.Include("User").FirstOrDefault(i => i.UserId == user.Id);
            if (user == null)
            {
                return NotFound();
            }
            ChangeProfileUserViewModel model = new ChangeProfileUserViewModel()
            {
                Id = user.Id,
                Email = user.Email,
                Login = user.Login,
                Country = user.Country
            };
            if (image != null)
            {
                model.Avatar = image.Avatar;
                model.ImageId = image.Id;
            }
            CountryList();
            return View(model);
        }

        [Authorize]
        [HttpPost, ActionName("Profile")]
        public async Task<IActionResult> ChangeProfile(ChangeProfileUserViewModel model)
        {
            if (ModelState.IsValid)
            {
                User user = await _userManager.FindByIdAsync(model.Id);

                if (user != null)
                {
                    user.Email = model.Email;
                    user.Login = model.Login;
                    user.Country = model.Country;

                    if (model.NewPassword != null)
                    {
                        var _passwordValidator =
                        HttpContext.RequestServices.GetService(typeof(IPasswordValidator<User>)) as IPasswordValidator<User>;
                        var _passwordHasher =
                            HttpContext.RequestServices.GetService(typeof(IPasswordHasher<User>)) as IPasswordHasher<User>;
                        IdentityResult result =
                            await _passwordValidator.ValidateAsync(_userManager, user, model.NewPassword);
                        if (result.Succeeded)
                        {
                            user.PasswordHash = _passwordHasher.HashPassword(user, model.NewPassword);
                            await _userManager.UpdateAsync(user);
                            ChangeAvatar(model, user);
                            return RedirectToAction("Profile");
                        }
                        else
                        {
                            foreach (var error in result.Errors)
                            {
                                ModelState.AddModelError(string.Empty, error.Description);
                            }
                        }
                    }
                    else
                    {                        
                        var result = await _userManager.UpdateAsync(user);
                        ChangeAvatar(model, user);
                        if (result.Succeeded)
                        {                            
                            return RedirectToAction("Profile");
                        }
                        else
                        {
                            foreach (var error in result.Errors)
                            {
                                ModelState.AddModelError(string.Empty, error.Description);
                            }
                        }
                    }
                }
                else
                {
                    ModelState.AddModelError(string.Empty, "The user is not found");
                }
            }
            return View(model);
        }

        public void CountryList()
        {
            List<string> CountryList = new List<string>();
            CultureInfo[] CInfoList = CultureInfo.GetCultures(CultureTypes.SpecificCultures);
            foreach (CultureInfo CInfo in CInfoList)
            {
                RegionInfo R = new RegionInfo(CInfo.LCID);
                if (!(CountryList.Contains(R.EnglishName)))
                {
                    CountryList.Add(R.EnglishName);
                }
            }

            CountryList.Sort();
            ViewBag.CountryList = CountryList;
        }
        [HttpGet]
        [AllowAnonymous]
        public IActionResult ForgotPassword()
        {
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> ForgotPassword(ForgotPasswordViewModel model)
        {
            if (ModelState.IsValid)
            {
                var user = await _userManager.FindByEmailAsync(model.Email);
                if (user == null || !(await _userManager.IsEmailConfirmedAsync(user)))
                {                   
                    return RedirectToAction("DeckBox", "Home");
                }
                if (user.ForgotpasswordDate > DateTime.Now)
                {
                    TempData["forgotpasswordDateError"] =_localizer["forgotpasswordDate"];
                    return View(model);

                }
                else
                {
                    var code = await _userManager.GeneratePasswordResetTokenAsync(user);
                    var callbackUrl = Url.Action("ResetPassword", "Account", new { userId = user.Id, code = code }, protocol: HttpContext.Request.Scheme);
                    EmailService emailService = new EmailService(_serviceScopeFactory);
                    /*  if (user.Country == "Russia" || user.Country == "Kyrgyzstan")
                      {
                          await emailService.SendEmailAsync(model.Email, "Reset Password",
                      $"<div class<h3 style='text-align: center;font-size: 27px;color: black; font-weight: 600;'>Почти готово!</h3> " +
                      $"<p style='font-size: 15px;text-align: center;color: black;padding-top: 1%;'>Здраствуйте, {user.Login}!" +
                      $"<p style='padding-left: 2%;padding-right: 2%;font-size: 14px;text-align: center;color: black;padding-top: 1%;'>Для восстановления пароля пройдите по ссылке ниже: <br /> " +
                      $"<a style='font-size: 1rem;border: none;padding: 1px;text-align: center;cursor: pointer;margin-top: 3%;display: inline-block;color: rgb(255, 255, 255);" +
                      $"background-color: rgb(0, 123, 255);border-radius: 0.25rem;width: 45%;text-decoration: none;" +
                      $" margin-bottom: 12%;'<a href='{callbackUrl}'>Подтвердить адрес электронной почты</a>");
                          user.ForgotpasswordDate = DateTime.Now.AddMinutes(10);
                          _context.Update(user);
                          await _context.SaveChangesAsync();
                      }
                      else
                      {
                          await emailService.SendEmailAsync(model.Email, "Reset Password",
                          $"<div class<h3 style='text-align: center;font-size: 27px;color: black; font-weight: 600;'>Almost done!</h3> " +
                          $"<p style='font-size: 15px;text-align: center;color: black;padding-top: 1%;'>Hello, {user.Login}!" +
                          $"<p style='padding-left: 10%;padding-right: 10%;font-size: 14px;text-align: center;color: black;padding-top: 1%;'>To recover your password, follow the link below: <br /> " +
                          $"<a style='font-size: 1rem;border: none;padding: 0.375rem 0.3rem;text-align: center;cursor: pointer;margin-top: 3%;display: inline-block;color: rgb(255, 255, 255);" +
                          $"background-color: rgb(0, 123, 255);border-radius: 0.25rem;width: 45%;text-decoration: none;" +
                          $" margin-bottom: 12%;'<a href='{callbackUrl}'>Confirm your email address</a>");
                          user.ForgotpasswordDate = DateTime.Now.AddMinutes(10);
                          _context.Update(user);
                          await _context.SaveChangesAsync();
                      }*/
                    if (user.Country == "Russia" || user.Country == "Kyrgyzstan")
                    {
                        await emailService.SendEmailAsync(model.Email, "Reset Password",
                       $"<div><h3 style='text-align: center;font-size: 27px;color: black; font-weight: 600;'>Почти готово!</h3> " +
                       $"<p style='font-size: 17px;text-align: center;color: black;'>Здраствуйте, {user.Login}!" +
                       $"<p style='font-size: 16px; text-align: center; color: black;'>Для восстановления пароля пройдите по ссылке ниже: " +
                       $"<br><a style='font-size: 1.1rem; border: none; text-align: center; cursor: pointer; margin-top: 15px; display: inline-block; color: rgb(255, 255, 255);" +
                       $"background-color: rgb(0, 123, 255); border-radius: 0.25rem; text-decoration: none; padding: 7px calc(50% - 590px); text-align: center;" +
                       $" margin-bottom: 12%; padding-right: 6px; padding-left: 6px;' href='{callbackUrl}'>Подтвердить адрес электронной почты</a></div>");
                        user.ForgotpasswordDate = DateTime.Now.AddMinutes(10);
                        _context.Update(user);
                        await _context.SaveChangesAsync();
                    }
                    else
                    {
                        await emailService.SendEmailAsync(model.Email, "Reset Password",
                        $"<div><h3 style='text-align: center;font-size: 27px;color: black; font-weight: 600;'>Almost done!</h3> " +
                        $"<p style='font-size: 17px;text-align: center;color: black;'>Hello, {user.Login}!" +
                        $"<p style='font-size: 16px; text-align: center; color: black;'>To recover your password, follow the link below: " +
                        $"<br><a style='font-size: 1.1rem; border: none; text-align: center; cursor: pointer; margin-top: 15px; display: inline-block; color: rgb(255, 255, 255); " +
                        $"background-color: rgb(0, 123, 255); border-radius: 0.25rem; text-decoration: none; padding: 7px calc(50% - 590px); text-align: center; " +
                        $"margin-bottom: 12%; padding-right: 6px; padding-left: 6px;' href='{callbackUrl}'>Confirm your email address</a></div>");
                        user.ForgotpasswordDate = DateTime.Now.AddMinutes(10);
                        _context.Update(user);
                        await _context.SaveChangesAsync();
                    }
                }
                return RedirectToAction("DeckBox", "Home");
            }
            return View(model);
        }
        [HttpGet]
        [AllowAnonymous]
        public IActionResult ResetPassword(string code = null)
        {
            return code == null ? View("AccessError") : View();
        }
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> ResetPassword(ResetPasswordViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            var user = await _userManager.FindByEmailAsync(model.Email);
            if (user == null)
            {
                return RedirectToAction("DeckBox", "Home");
            }
            var result = await _userManager.ResetPasswordAsync(user, model.Code, model.Password);
            if (result.Succeeded)
            {
                return RedirectToAction("DeckBox", "Home");
            }
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError(string.Empty, error.Description);
            }
            return View(model);
        }
        [Authorize]
        public void ChangeAvatar(ChangeProfileUserViewModel model, User user)
        {

            if (model.Image != null && model.Image.ImageFile != null)
            {
                if (model.Avatar != null)
                {
                    Image imageEdit = _context.Images.FirstOrDefault(i => i.Avatar == model.Avatar);
                    string folderPathLstImg = Path.Combine(_webHostEnvironment.WebRootPath, "images", "avatars");
                    ImageAbillities.DeleteImage(imageEdit.Avatar, folderPathLstImg);


                    string folderPath = Path.Combine(_webHostEnvironment.WebRootPath, "images", "avatars");
                    var filename = ImageAbillities.UploadImage(model.Image.ImageFile, folderPath);

                    model.Image.Avatar = filename;
                    model.Image.UserId = user.Id;

                    imageEdit.Avatar = model.Image.Avatar;


                    _context.Update(imageEdit);
                    _context.SaveChanges();
                }
                else
                {
                    Image image = new Image();

                    string folderPath = Path.Combine(_webHostEnvironment.WebRootPath, "images", "avatars");
                    var filename = ImageAbillities.UploadImage(model.Image.ImageFile, folderPath);

                    model.Image.Avatar = filename;
                    model.Image.UserId = user.Id;

                    image.Avatar = model.Image.Avatar;
                    image.UserId = model.Image.UserId;
                    _context.Add(image);
                    _context.SaveChanges();
                }
            }
        }
        [Authorize]
        public async Task<IActionResult> DeleteAvatar(int id)
        {
            Image image = await _context.Images.FindAsync(id);

            if (image.Avatar != null)
            {
                string folderPathLstImg = Path.Combine(_webHostEnvironment.WebRootPath, "images", "avatars");
                string filePathLstImg = Path.Combine(folderPathLstImg, image.Avatar);
                System.IO.File.Delete(filePathLstImg);
            }
            _context.Images.Remove(image);
            await _context.SaveChangesAsync();
            return RedirectToAction("Profile");
        }
    }
}