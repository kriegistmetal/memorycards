﻿
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MemoryCards.Models;
using System;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Localization;
namespace MemoryCards.Controllers
{
    [Authorize]
    public class DecksController : Controller
    {
        private readonly MemoryContext _context;
        private readonly UserManager<User> _userManager;
        private readonly IStringLocalizer<DecksController> _localizer;

        public DecksController(MemoryContext context, IStringLocalizer<DecksController> localizer, UserManager<User> userManager)
        {
            _context = context;
            _localizer = localizer;
            _userManager = userManager;

        }

        public IActionResult Index(int? Id)
        {
            string userId =  _userManager.GetUserId(User);
            var deckBox = _context.DeckBoxes.FirstOrDefault(p => p.Id == Id);
            var deck = _context.Decks
                .Include(c => c.Cards)
                .Where(p => p.DeckBoxId == deckBox.Id && deckBox.UserId == userId)
                .OrderBy(c => c.RepeatDay);
            if(deck.Count()==0)
                return View("AccessError");
            return View(deck);
        }
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            string userId = _userManager.GetUserId(User);
            var deck = await _context.Decks.FirstOrDefaultAsync(p=>p.Id==id && p.DeckBox.UserId == userId);
            
            if (deck == null)
            {
                return View("AccessError");
            }
            return View(deck);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,RepeatDay,DeckBoxId,CreatedAt,UpdatedAt, LastSendMsgDate")] Deck deck)
        {
            if (id != deck.Id)
            {
                return NotFound();
            }
            if (ModelState.IsValid)
            {
                try
                {
                    var deckBox = _context.DeckBoxes
                        .AsNoTracking()
                        .Include(d => d.Decks)
                        .FirstOrDefault(p => p.Id == deck.DeckBoxId);
                    var check = deckBox.Decks.FirstOrDefault(c => c.RepeatDay == deck.RepeatDay);

                    if (check == null)
                    {
                        deck.LastSendMsgDate = DateTime.Now.AddDays(deck.RepeatDay);
                        _context.Update(deck);
                        await _context.SaveChangesAsync();
                    }
                    else
                    {
                          TempData["deckerror"]=_localizer["deckError"]; 
                        return View(deck);
                    }
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!DeckExists(deck.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index), new { Id = deck.DeckBoxId });
            }
            return View(deck);
        }

        private bool DeckExists(int id)
        {
            return _context.Decks.Any(e => e.Id == id);
        }
    }
}
