﻿using MemoryCards.Models;
using MemoryCards.ViewModels;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Localization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace MemoryCards.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly MemoryContext _context;
        private readonly UserManager<User> _userManager;        
        private readonly SignInManager<User> _signInManager;

        public HomeController(ILogger<HomeController> logger, MemoryContext context, 
            UserManager<User> userManager, SignInManager<User> signInManager)
        {
            _logger = logger;
            _context = context;
            _userManager = userManager;
            _signInManager = signInManager;
        }

        public async Task<IActionResult> DeckBox()
        {
            if (_signInManager.IsSignedIn(User))
            {
                return RedirectToAction("Index", "DeckBoxes");
            }
            else
            {
                DeckBox deckBox = await _context.DeckBoxes.Include("Image").FirstOrDefaultAsync(d => d.UserId == null);
                return View(deckBox);
            }
        }

        public IActionResult Deck(int? Id)
        {
            var deck = _context.Decks.Include(p => p.Cards).Where(p => p.DeckBoxId == Id).OrderBy(p => p.RepeatDay);
            return View(deck);
        }
        public IActionResult DeckUserProfile(int? Id)
        {
            var deck = _context.Decks.Include(p => p.Cards).Where(p => p.DeckBoxId == Id).OrderBy(p => p.RepeatDay);
            return View(deck);
        }
        public async Task<IActionResult> Card(int? Id)
        {
            List<Card> cards = await _context.Cards.Include("Deck")
                .Include(p => p.Image)
                .Where(c => c.DeckId == Id)
                .OrderBy(c => c.MoveDate).ToListAsync();

            CardDeckViewModel cardDeckViewModel = new CardDeckViewModel
            {
                Cards = cards,
                DeckId = Id,
                Deck = _context.Decks.FirstOrDefault(p => p.Id == Id)
            };
            return View(cardDeckViewModel);

        }
        public async Task<IActionResult> CardUserProfile(int? Id)
        {
            List<Card> cards = await _context.Cards.Include("Deck")
                .Include(p => p.Image)
                .Where(c => c.DeckId == Id)
                .OrderBy(c => c.MoveDate).ToListAsync();

            CardDeckViewModel cardDeckViewModel = new CardDeckViewModel
            {
                Cards = cards,
                DeckId = Id,
                Deck = _context.Decks.FirstOrDefault(p => p.Id == Id)
            };
            return View(cardDeckViewModel);

        }
        [HttpPost]
        public IActionResult SetLanguage(string culture, string returnUrl)
        {
            Response.Cookies.Append(
                CookieRequestCultureProvider.DefaultCookieName,
                CookieRequestCultureProvider.MakeCookieValue(new RequestCulture(culture)),
                new CookieOptions { Expires = DateTimeOffset.UtcNow.AddYears(1) }
            );

            return LocalRedirect(returnUrl);
        }
        [HttpPost]
        public async Task<IActionResult> SetDarkTheme(bool darkTheme, string returnUrl)
        {
            User user = await _userManager.GetUserAsync(User);
            user.DarkTheme = darkTheme;
            await _userManager.UpdateAsync(user);
            return LocalRedirect(returnUrl);
        }
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    } 
}