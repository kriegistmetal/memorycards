﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using MemoryCards;
using MemoryCards.Models;
using MemoryCards.ViewModels;
using System.IO;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using MemoryCards.Solid;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;

namespace MemoryCards.Controllers
{
    [Authorize]
    public class CardsController : Controller
    {
        private readonly MemoryContext _context;
        private readonly IWebHostEnvironment _webHostEnvironment;
        private readonly UserManager<User> _userManager;
        public CardsController(MemoryContext context, IWebHostEnvironment webHostEnvironment, UserManager<User> userManager)
        {
            _context = context;
            _webHostEnvironment = webHostEnvironment;
            _userManager = userManager;
        }

        // GET: Cards
        public async Task<IActionResult> Index(int? Id, int? Nextcardid)
        {
            string userId = _userManager.GetUserId(User);
            List<Card> cards = await _context.Cards.Include("Deck")
                .Include(p=>p.Deck.DeckBox)
                .Include(p=>p.Image)
                .Where(c => c.DeckId == Id && c.Deck.DeckBox.UserId==userId)
                .OrderBy(c => c.MoveDate).ToListAsync();

            CardDeckViewModel cardDeckViewModel = new CardDeckViewModel
            {
                Cards =  cards,
                DeckId = Id,
                Deck = _context.Decks.Include(d=>d.DeckBox).FirstOrDefault(p => p.Id == Id && p.DeckBox.UserId == userId)
            };
            if (cardDeckViewModel.Deck == null)
                return View("AccessError");
            if (Nextcardid != null)
                ViewBag.Nextid = Nextcardid;
            return View(cardDeckViewModel);

        }

        // GET: Cards/Create
        public IActionResult Create(int? id)
        {
            string userId = _userManager.GetUserId(User);
            CardDeckViewModel cardDeckViewModel = new CardDeckViewModel
            {
                Deck = _context.Decks.Include(d => d.DeckBox).FirstOrDefault(p => p.Id == id && p.DeckBox.UserId == userId),
                card = new Card(),
            };
            if (cardDeckViewModel.Deck == null)
                return View("AccessError"); 
            return View(cardDeckViewModel);
        }

        // POST: Cards/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Word,Defination,DeckId,UseExample,CreatedAt,UpdatedAt,Image")] Card card, int DeckId)
        {
            if (ModelState.IsValid)
            {
                if (card.Image != null)
                {
                    string folderPath = Path.Combine(_webHostEnvironment.WebRootPath, "images", "card_images");
                    var filename = ImageAbillities.UploadImage(card.Image.ImageFile, folderPath);
                    card.Image.CardImageName = filename;
                }
                card.DeckId = DeckId;
                card.ShowCount = 0;
                card.MoveDate = DateTime.Now;
                _context.Add(card);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index), new { Id = DeckId });
            }
            return View(card);
        }

        public async Task<IActionResult> Know(int Id)
        {
            Card card = _context.Cards.FirstOrDefault(c => c.Id == Id); //карточка опреде
            Deck deck = _context.Decks.FirstOrDefault(d => d.Cards.Contains(card));// из опреде коробки
            List<Card> cards = await _context.Cards.Where(c => c.DeckId == deck.Id)
                .OrderBy(c => c.MoveDate).ToListAsync();

            var next = (from x in cards where x.MoveDate > card.MoveDate orderby x.MoveDate ascending select x).FirstOrDefault();
            if (next == null)
            {
                next = (from x in cards where x.MoveDate < card.MoveDate orderby x.MoveDate ascending select x).FirstOrDefault();
            }

            if (deck == null)
            {
                return View();
            }
            else
            {
                DeckBox deckBox = _context.DeckBoxes.Include(p => p.Decks).FirstOrDefault(db => db.Decks.Contains(deck));//из опред словаря
                deckBox.Decks = deckBox.Decks.OrderBy(p => p.RepeatDay).ToList();//сортиров репеат дай
                Deck nextDeck = deckBox.Decks.FirstOrDefault(p => p.RepeatDay > deck.RepeatDay); //отсортров репеат дау по большен у определн словаря больше определен коробки
                if (nextDeck != null)
                {
                    card.DeckId = nextDeck.Id;
                    card.Deck = nextDeck;
                    card.ShowCount += 1;
                    card.MoveDate = DateTime.Now;
                    if (card.ShowCount == 3)
                    {
                        deck.CountLearnCards += 1;
                    }
                    _context.Update(card);
                    await _context.SaveChangesAsync();
                }
            }
            if (next?.Id == null)
            {
                return RedirectToAction(nameof(Index), new { Id = deck.Id });
            }
            else
            {
                return RedirectToAction(nameof(Index), new { Id = deck.Id, Nextcardid = next.Id });
            }
        }

        public async Task<IActionResult> DontKnow(int Id)
        {
            Card card = _context.Cards.FirstOrDefault(c => c.Id == Id);
            Deck deck = _context.Decks.FirstOrDefault(d => d.Cards.Contains(card));
            List<Card> cards = await _context.Cards.Where(c => c.DeckId == deck.Id)
                .OrderBy(c => c.MoveDate).ToListAsync();

            var next = (from x in cards where x.MoveDate > card.MoveDate orderby x.MoveDate ascending select x).FirstOrDefault();
            if (next == null)
            {
                next = (from x in cards where x.MoveDate < card.MoveDate orderby x.MoveDate ascending select x).FirstOrDefault();
            }

            DeckBox deckBox = _context.DeckBoxes.Include(p => p.Decks).FirstOrDefault(db => db.Decks.Contains(deck));
            deckBox.Decks = deckBox.Decks.OrderByDescending(p => p.RepeatDay).ToList();
     
             
           Deck prevDeck = deckBox.Decks.FirstOrDefault(p=>p.RepeatDay<deck.RepeatDay);
           
            if (prevDeck != null)
            {
                card.DeckId = prevDeck.Id;
                card.Deck = prevDeck;
                card.MoveDate = DateTime.Now;
                _context.Update(card);
                await _context.SaveChangesAsync();
            }

            if (next?.Id == null)
            {
                return RedirectToAction(nameof(Index), new { Id = deck.Id });
            }
            else
            {
                return RedirectToAction(nameof(Index), new { Id = deck.Id, Nextcardid = next.Id });
            }
        }
        // GET: Cards/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            string userId = _userManager.GetUserId(User);
            var card = await _context.Cards.Include(p=>p.Deck).Include(p=>p.Deck.DeckBox).Include(p => p.Image).FirstOrDefaultAsync(p => p.Id == id && p.Deck.DeckBox.UserId==userId);
            if (card == null)
            {
                return View("AccessError"); 
            }
            return View(card);
        }

        // POST: Cards/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Word,Defination,DeckId,UseExample,ShowCount,MoveDate,CreatedAt,UpdatedAt,Image")] Card card, int DeckId
            , IFormFile imageDb)
        {
            if (id != card.Id)
            {
                return NotFound();
            }
            if (ModelState.IsValid)
            {
                Image image = await _context.Images.FirstOrDefaultAsync(p => p.CardImageName == card.Image.CardImageName && p.CardId == card.Id);
                try
                {
                    if (imageDb != null)
                    {
                        List<Card> cards = _context.Cards.Where(p => p.Image.CardImageName == card.Image.CardImageName && p.Id != card.Id).ToList();
                        if (card.Image.CardImageName != null && cards.Count == 0)
                        {
                            string folderPathLstImg = Path.Combine(_webHostEnvironment.WebRootPath, "images", "card_images");
                            ImageAbillities.DeleteImage(card.Image.CardImageName, folderPathLstImg);
                            Image imageS = _context.Images.FirstOrDefault(p => p.CardId == card.Id && p.CardImageName == card.Image.CardImageName);
                            _context.Remove(imageS);
                            await _context.SaveChangesAsync();
                        }
                        string folderPath = Path.Combine(_webHostEnvironment.WebRootPath, "images", "card_images");
                        var filename = ImageAbillities.UploadImage(imageDb, folderPath);
                        card.Image.CardImageName = filename;

                        card.DeckId = DeckId;
                        _context.Update(card);
                        await _context.SaveChangesAsync();

                    }
                    else if (imageDb == null)
                    {
                        if (card.Image.CardImageName == null)
                        {
                            _context.Update(card);
                            _context.SaveChanges();
                            Image noimage = await _context.Images.FirstOrDefaultAsync(p => p.CardId == card.Id && p.Id == card.Image.Id);
                            _context.Images.Remove(noimage);
                            _context.SaveChanges();
                            return RedirectToAction(nameof(Index), new { Id = DeckId });
                        }
                        else
                        {
                            _context.Images.Remove(image);
                            await _context.SaveChangesAsync();
                        }
                    }
                    _context.Update(card);
                    _context.SaveChanges();
                }


                catch (DbUpdateConcurrencyException)
                {

                    if (!CardExists(card.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }

                return RedirectToAction(nameof(Index), new { Id = DeckId });
            }
            return View(card);
        }
        public async Task<IActionResult> Delete(int id, int DeckId)
        {
            var card = _context.Cards.Include(p => p.Image).FirstOrDefault(p => p.Id == id);
            List<Card> cards = _context.Cards.Where(p => p.Image == card.Image
           && p.Id != card.Id).ToList();

            if (card.Image != null && cards.Count == 0)
            {

                string folderPathLstImg = Path.Combine(_webHostEnvironment.WebRootPath, "images", "card_images");
                ImageAbillities.DeleteImage(card.Image.CardImageName, folderPathLstImg);
                Image image = _context.Images.FirstOrDefault(p => p.CardId == card.Id);
                _context.Remove(image);
                await _context.SaveChangesAsync();
            }
            _context.Cards.Remove(card);
            await _context.SaveChangesAsync();
            DeckId = card.DeckId;
            return RedirectToAction(nameof(Index), new { Id = DeckId });
        }

        private bool CardExists(int id)
        {
            return _context.Cards.Any(e => e.Id == id);
        }
    }
}
