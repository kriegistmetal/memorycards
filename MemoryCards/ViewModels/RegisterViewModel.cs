﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MemoryCards.ViewModels
{
    public class RegisterViewModel
    {
        [Required(ErrorMessage = "Введите почту")]
        [Remote(action: "CheckEmail", controller: "Account", ErrorMessage = "Email уже используется")]
        [StringLength(31, MinimumLength = 6, ErrorMessage = "Длина Email должна быть от 6 до 31 символов")]
        [EmailAddress(ErrorMessage = "Некорректный адрес электронной почты")]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [Required(ErrorMessage = "Укажите логин")]
        [StringLength(15, MinimumLength = 3, ErrorMessage = "Длина логина должна быть от 3 до 15 символов")]
        [Display(Name = "Login")]
        public string Login { get; set; }

        [Required(ErrorMessage = "Укажите страну проживания")]
        [Display(Name = "Country")]
        public string Country { get; set; }

        [StringLength(10, MinimumLength = 6, ErrorMessage = "Длина пароля должна быть от 6 до 10 символов")]
        [Required(ErrorMessage = "Введите пароль")]
        [DataType(DataType.Password)]
        [Display(Name = "Пароль")]
        public string Password { get; set; }

        [Required(ErrorMessage = "Подтвердите пароль")]
        [Compare("Password", ErrorMessage = "Пароли не совпадают")]
        [DataType(DataType.Password)]
        [Display(Name = "Подтвердить пароль")]
        public string PasswordConfirm { get; set; }
    }
}
