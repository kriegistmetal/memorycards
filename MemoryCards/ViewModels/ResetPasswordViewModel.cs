﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MemoryCards.ViewModels
{
    public class ResetPasswordViewModel
    {
        [Required(ErrorMessage = "Введите почту")]
        [StringLength(31, MinimumLength = 6, ErrorMessage = "Длина Email должна быть от 6 до 31 символов")]
        [EmailAddress(ErrorMessage = "Некорректный адрес электронной почты")]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [Required(ErrorMessage = "Введите пароль")]
        [DataType(DataType.Password)]

        [StringLength(10, MinimumLength = 6, ErrorMessage = "Длина пароля должна быть от 6 до 10 символов")]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [Required(ErrorMessage = "Подтвердите пароль")]
        [DataType(DataType.Password)]        
        [Display(Name = "Подтвердить пароль")]
        [Compare("Password", ErrorMessage = "Пароли не совпадают")]
        public string ConfirmPassword { get; set; }
        public string Code { get; set; }
    }
}
