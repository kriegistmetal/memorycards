﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MemoryCards.ViewModels
{
    public class RatingSortViewModel
    {
        public int DeckBoxId { get; set; }
        public int NewRating { get; set; }
    }
}
