﻿using MemoryCards.Models;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MemoryCards.ViewModels
{
    public class CardDeckViewModel
    {
        public List<Card> Cards { get; set; }
        public int? DeckId{ get; set; }
        public Card card { get; set; }
        public Deck Deck { get; set; }
    }
}
