﻿using MemoryCards.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace MemoryCards.ViewModels
{
    public class ChangeProfileUserViewModel
    {
        public string Id { get; set; }

        [Remote(action: "CheckEmail", controller: "Account", ErrorMessage = "Email уже используется")]
        [EmailAddress(ErrorMessage = "Некорректный адрес электронной почты")]
        [StringLength(31, MinimumLength = 6, ErrorMessage = "Длина Email должна быть от 6 до 31 символов")]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [StringLength(15, MinimumLength = 3, ErrorMessage = "Длина логина должна быть от 3 до 15 символов")]
        [Display(Name = "Логин")]
        public string Login { get; set; }

        [Display(Name = "Страна проживания")]
        public string Country { get; set; }

        [StringLength(10, MinimumLength = 6, ErrorMessage = "Длина пароля должна быть от 6 до 10 символов")]
        [DataType(DataType.Password)]
        [Display(Name = "Введите новый пароль")]
        public string NewPassword { get; set; }

        [Compare("NewPassword", ErrorMessage = "Пароли не совпадают")]
        [DataType(DataType.Password)]
        [Display(Name = "Подтвердить пароль")]
        public string PasswordConfirm { get; set; }

        public string Avatar { get; set; }
        public Image Image { get; set; }
        public int ImageId { get; set; }
    }
}
