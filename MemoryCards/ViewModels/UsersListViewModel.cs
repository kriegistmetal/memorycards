﻿using MemoryCards.Models;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MemoryCards.ViewModels
{
    public class UsersListViewModel
    {
        public IEnumerable<User> Users { get; set; }        
        public PaginatedListViewModel<User> PageViewModel { get; set; }
    }
}
