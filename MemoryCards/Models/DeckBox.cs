﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace MemoryCards.Models
{
    public class DeckBox : TimeStamp
    {
        public int Id { get; set; }
        [Required(ErrorMessage = "Отсутствует название")]
        [StringLength(50, MinimumLength = 3, ErrorMessage = "Длина названия должна быть от 3 до 50 символов")]
        public string Title { get; set; }
        [StringLength(50, MinimumLength = 3, ErrorMessage = "Длина описания должна быть от 3 до 50 символов")]
        public string Description { get; set; }
                
        public string UserId { get; set; }
        public User User { get; set; }

        public string DeckBoxcColor { get; set; }
        public int Rating { get; set; }
        public bool Private { get; set; }
        public Image Image { get; set; }
        public List<Deck> Decks { get; set;}
        public DeckBox()
        {
            this.CreatedAt = DateTime.Now;
            this.UpdatedAt = DateTime.Now;
        }
        public int LearnedWords()
        {
            int countCards = 0;
            foreach (var item in Decks)
            {
                if (item.CountLearnCards != 0)
                {
                    countCards += 1;
                }
            }
            return countCards;
        }
           public int CountWords()
           {
            int countWords = 0;
            foreach (var item in Decks)
            {
                countWords += item.Cards.Count;
            }
                return countWords;
            }   
    }
}
