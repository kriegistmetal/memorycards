﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MemoryCards.Models
{
    public class Deck : TimeStamp
    {
        public int Id { get; set; }
        [Range(1, 360, ErrorMessage = "Недопустимое значение")]
        public int RepeatDay { get; set; }
        public int CountLearnCards { get; set; }
        public int DeckBoxId { get; set; }      
        public DeckBox DeckBox { get; set; }
        public List<Card> Cards { get; set; }
        public DateTime LastSendMsgDate { get; set; }
        public Deck()
        {
            this.CreatedAt = DateTime.Now;
            this.UpdatedAt = DateTime.Now;
        }
    }
}
