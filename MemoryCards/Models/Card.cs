﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace MemoryCards.Models
{
    public class Card : TimeStamp
    {
        public int Id { get; set; }
        [Required(ErrorMessage = "Обязательное поле")]
        [StringLength(50, MinimumLength = 3, ErrorMessage = "Длина названия должна быть от 3 до 50 символов")]
        public string Word { get; set; }

        [Required(ErrorMessage = "Обязательное поле")]
        [StringLength(150, MinimumLength = 3, ErrorMessage = "Длина определения должна быть от 3 до 150 символов")]
        public string Defination { get; set; }

        public int DeckId { get; set; }
        public Deck Deck { get; set; }
        [Required(ErrorMessage = "Обязательное поле")]
        [StringLength(150, MinimumLength = 1, ErrorMessage = "Длина названия должна быть от 1 до 150 символов")]
        public string UseExample { get; set; }    
        public int? ShowCount { get; set; }
        public DateTime? MoveDate { get; set; }
        public Image Image { get; set; }
        public Card()
        {
            this.CreatedAt = DateTime.Now;
            this.UpdatedAt = DateTime.Now;
        }
    }
}
