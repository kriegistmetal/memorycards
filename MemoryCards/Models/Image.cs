﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace MemoryCards.Models
{
    public class Image
    {
      public int Id { get; set; }
      public string DeckBoxImageName { get; set; }
      public string CardImageName { get; set; }
      public string Avatar { get; set; }
      [NotMapped]
      public IFormFile ImageFile { get; set; }
      public int? DeckBoxId { get; set; }
      public DeckBox DeckBox { get; set; }
      public int? CardId { get; set; }
      public Card Card { get; set; }
      public string UserId { get; set; }
      public User User { get; set; }
    }
}
