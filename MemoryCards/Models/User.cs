﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MemoryCards.Models
{
    public class User : IdentityUser
    { 
        public DateTime? Registered_At { get; set; }
        public List<DeckBox> DeckBoxes { get; set; }
        [StringLength(15, MinimumLength = 3, ErrorMessage = "Длина логина должна быть от 3 до 15 символов")]
        public string Login { get; set; }
        public string Country { get; set; }
        public bool DarkTheme { get; set; }
        public Image Image { get; set; }
        public DateTime? ForgotpasswordDate { get; set; }
        public User()
        {
            this.Registered_At = DateTime.Now;
        }
    }
}
