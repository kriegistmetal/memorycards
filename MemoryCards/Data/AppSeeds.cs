﻿using MemoryCards.Models;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MemoryCards.Data
{
    public class AppSeeds
    {       
        public static async Task CreateDataDefaultDeckboxAsync(IServiceProvider service)
        {
            var context = service.GetRequiredService<MemoryContext>();
           
            var env = service.GetRequiredService<IWebHostEnvironment>();
           
            string adminEmail = "seleniumdefaulttestuser@mail.ru";
            string userForSearch = "thisuserforsearch@mail.ru";
    
            if (env.EnvironmentName=="Staging")
            {
                try
                {
                    context.Database.EnsureDeleted();
                    context.Database.Migrate();
                    var userManager = service.GetRequiredService<UserManager<User>>();
                    User existedSearcher = await userManager.FindByEmailAsync(userForSearch);
                    User existingUser = await userManager.FindByEmailAsync(adminEmail);

                    User user = new User
                    {
                        Email = adminEmail,
                        UserName = adminEmail,
                        Login = "TestUser",
                        Country = "Kyrgyzstan",
                        EmailConfirmed = true
                    };

                    User searcher = new User
                    {
                        Id = "RealMadrid-Atletico-4-1",
                        Email = userForSearch,
                        UserName = userForSearch,
                        Login = "SomeUser",
                        Country = "Kyrgyzstan",
                        EmailConfirmed = true
                    };

                    var result = await userManager.CreateAsync(user, "Integration_1_one");
                    var result2 = await userManager.CreateAsync(searcher, "AntonUser21+");

                    DeckBox deckBox = new DeckBox
                    {
                        Title = "TestTitle",
                        Description = "TestDescription",
                        DeckBoxcColor = "#0066FF",
                        UserId = user.Id
                    };
                    DeckBox deckBoxForSearcher = new DeckBox
                    {
                        Title = "TitleForSearcher",
                        Description = "sadjsadkasjdaksl",
                        DeckBoxcColor = "#000",
                        UserId = searcher.Id
                    };
                    context.Add(deckBoxForSearcher);
                    context.Add(deckBox);
                    await context.SaveChangesAsync();

                    int repeatDay = 1;
                    for (int i = 0; i < 8; i++)
                    {
                        Deck deck = new Deck
                        {
                            RepeatDay = repeatDay,
                            DeckBoxId = deckBox.Id,
                            LastSendMsgDate = DateTime.Now.AddDays(repeatDay)
                        };
                        repeatDay += repeatDay;
                        context.Add(deck);
                        context.SaveChanges();
                    }

                    Deck Deck = await context.Decks.FirstOrDefaultAsync(d => d.RepeatDay == 1 && d.DeckBoxId == deckBox.Id);
                    Card card = new Card
                    {
                        DeckId = Deck.Id,
                        Word = "TestWord",
                        Defination = "TestDefination",
                        UseExample = "TestUseExample"
                    };
                    context.Add(card);
                    context.SaveChanges();
                }
                catch (Exception exc)
                {
                    Console.WriteLine("You've been fucked: " + exc.Message);
                }       
            }      
            
            var existContext = await context.DeckBoxes.FirstOrDefaultAsync(m => m.Title == "Food Products");

            if (existContext == null)
            {
                DeckBox deckBox = new DeckBox
                {
                    Title = "Food Products",
                    Description = "Learning English words, on the topic Food"
                };
                context.Add(deckBox);
                await context.SaveChangesAsync();

                int repeatDay = 1;
                for (int i = 0; i < 8; i++)
                {
                    Deck deck = new Deck
                    {
                        RepeatDay = repeatDay,
                        DeckBoxId = deckBox.Id,
                        LastSendMsgDate = DateTime.Now.AddDays(repeatDay)
                    };
                    repeatDay += repeatDay;
                    context.Add(deck);
                    context.SaveChanges();
                }

                Deck deck_1 = await context.Decks.FirstOrDefaultAsync(d => d.RepeatDay == 1 && d.DeckBoxId == deckBox.Id);
                Card card_1 = new Card { DeckId = deck_1.Id, Word = "Onion",
                    Defination = "A vegetable with a strong smell and flavour",
                    UseExample = "I always cry when I'm chopping onions"
                };
                context.Add(card_1);
                Card card_2 = new Card { DeckId = deck_1.Id, Word = "Pearl barley",
                    Defination = "Grains of barley with the outer covering removed",
                    UseExample = "Of these nine applications one, the pearl barley application resulted in an order imposing a dumping duty"
                };
                context.Add(card_2);
                Card card_3 = new Card { DeckId = deck_1.Id, Word = "Horseradish",
                    Defination = "A plant with a long, white root that has a strong taste",
                    UseExample = "Subcortical afferents to the frontal lobe in the rhesus monkey studied by means of retrograde horseradish peroxidase transport"
                };
                context.Add(card_3);
                Card card_4 = new Card { DeckId = deck_1.Id, Word = "Hazel grouse",
                    Defination = "A small fat bird, shot for sport and food",
                    UseExample = "Is one of the smaller members of the grouse family of birds"
                };
                context.Add(card_4);
                Card card_5 = new Card { DeckId = deck_1.Id, Word = "Mushroom",
                    Defination = "A fungus with a round top and short stem",
                    UseExample = "I had a mushroom omelette"
                };
                context.Add(card_5);

                Deck deck_2 = await context.Decks.FirstOrDefaultAsync(d => d.RepeatDay == 2 && d.DeckBoxId == deckBox.Id);
                Card card_6 = new Card { DeckId = deck_2.Id, Word = "Lamb",
                    Defination = "A young sheep, or the flesh of a young sheep eaten as meat",
                    UseExample = "She bought a side of lamb from the butcher's shop"
                };
                context.Add(card_6);
                Card card_7 = new Card { DeckId = deck_2.Id, Word = "Plaice",
                    Defination = "A sea fish with a flat, circular body, or its flesh eaten as food",
                    UseExample = "Flounders and plaice were used as experimental final hosts"
                };
                context.Add(card_7);
                Card card_8 = new Card { DeckId = deck_2.Id, Word = "Champagne",
                    Defination = "An expensive white or pink fizzy with bubbles wine made in the Champagne area of Eastern France",
                    UseExample = "We always celebrate our wedding anniversary with a bottle of champagne"
                };
                context.Add(card_8);
                Card card_9 = new Card { DeckId = deck_2.Id, Word = "Milkshake",
                    Defination = "A drink made of milk and usually ice cream and a flavour such as fruit or chocolate",
                    UseExample = "It reminded me of the froth at the end of a strawberry milkshake"
                };
                context.Add(card_9);
                Card card_10 = new Card { DeckId = deck_2.Id, Word = "Goose",
                    Defination = "A large water bird similar to a duck but larger, or the meat from this bird",
                    UseExample = "The fuel and energy sector is the goose that lays the golden eggs"
                };
                context.Add(card_10);

                Deck deck_3 = await context.Decks.FirstOrDefaultAsync(d => d.RepeatDay == 4 && d.DeckBoxId == deckBox.Id);
                Card card_11 = new Card { DeckId = deck_3.Id, Word = "Bream",
                    Defination = "A type of fish that can be eaten, found especially in lakes and rivers",
                    UseExample = "Developmental changes in the visual pigments of black bream"
                };
                context.Add(card_11);
                Card card_12 = new Card { DeckId = deck_3.Id, Word = "Veal",
                    Defination = "Mmeat from a very young cow",
                    UseExample = "Down with white things, be they wines or veal, and up with beef"
                };
                context.Add(card_12);
                Card card_13 = new Card { DeckId = deck_3.Id, Word = "Cabbage",
                    Defination = "A large, round vegetable with large green, white, or purple leaves that can be eaten cooked or uncooked",
                    UseExample = "If you overcook the cabbage it'll turn to mush"
                };
                context.Add(card_13);
                Card card_14 = new Card { DeckId = deck_3.Id, Word = "Watermelon",
                    Defination = "A large, round or oval-shaped fruit with dark green skin, sweet pink flesh, and a lot of black seeds",
                    UseExample = "Same size bands were obtained from test thrips specimens collected from onion and watermelon"
                };
                context.Add(card_14);
                Card card_15 = new Card { DeckId = deck_3.Id, Word = "Sturgeon",
                    Defination = "A type of fish that lives in northern parts of the world and is usually caught for its eggs",
                    UseExample = "Their structure has the same characteristic architectural features as sturgeon spermatozoa"
                };
                context.Add(card_15);

                Deck deck_4 = await context.Decks.FirstOrDefaultAsync(d => d.RepeatDay == 8 && d.DeckBoxId == deckBox.Id);
                Card card_16 = new Card { DeckId = deck_4.Id, Word = "Cognac",
                    Defination = "High quality brandy strong alcoholic drink made in western France",
                    UseExample = "Would you like another cognac"
                };
                context.Add(card_16);
                Card card_17 = new Card { DeckId = deck_4.Id, Word = "Yogurt",
                    Defination = "A slightly sour, thick liquid made from milk with bacteria added to it",
                    UseExample = "All I had for lunch was a yogurt"
                };
                context.Add(card_17);
                Card card_18 = new Card { DeckId = deck_4.Id, Word = "Cinnamon",
                    Defination = "The bark of a tropical tree, or a brown powder made from this",
                    UseExample = "The smell of cinnamon makes me hungry for cookies"
                };
                context.Add(card_18);
                Card card_19 = new Card { DeckId = deck_4.Id, Word = "Rice",
                    Defination = "The small seeds of a particular type of grass, cooked, and eaten as food",
                    UseExample = "The prisoners existed on a meagre diet of rice and fish"
                };
                context.Add(card_19);

                Deck deck_5 = await context.Decks.FirstOrDefaultAsync(d => d.RepeatDay == 16 && d.DeckBoxId == deckBox.Id);
                Card card_20 = new Card { DeckId = deck_5.Id, Word = "Melon",
                    Defination = "A large, round fruit with hard yellow or green skin, sweet flesh, and a lot of seeds",
                    UseExample = "I put orange, banana, and melon in the fruit salad"
                };
                context.Add(card_20);
                Card card_22 = new Card { DeckId = deck_5.Id, Word = "Rabbit",
                    Defination = "A small animal with long ears and large front teeth, or the meat of this animal eaten as food",
                    UseExample = "He waved his magic wand and a rabbit appeared"
                };
                context.Add(card_22);
                Card card_23 = new Card { DeckId = deck_5.Id, Word = "Turkey",
                    Defination = "A large bird grown for its meat on farms, the flesh of this bird used as food",
                    UseExample = "What did you do that for, you turkey?"
                };
                context.Add(card_23);

                Deck deck_6 = await context.Decks.FirstOrDefaultAsync(d => d.RepeatDay == 32 && d.DeckBoxId == deckBox.Id);
                Card card_24 = new Card { DeckId = deck_6.Id, Word = "Beer",
                    Defination = "An alcoholic drink made from grain and hops",
                    UseExample = "There's plenty more beer in the fridge"
                };
                context.Add(card_24);
                Card card_25 = new Card { DeckId = deck_6.Id, Word = "Coffee",
                    Defination = "A dark brown powder with a strong flavour and smell that is made by crushing dark beans from a tropical bush and used to make a drink",
                    UseExample = "A cup of coffee"
                };
                context.Add(card_25);
                Card card_26 = new Card { DeckId = deck_6.Id, Word = "Bread",
                    Defination = "A food made from flour, water, and usually yeast, mixed together and baked",
                    UseExample = "We had a quick meal of bread and soup"
                };
                context.Add(card_26);
                Card card_27 = new Card { DeckId = deck_6.Id, Word = "Sugar",
                    Defination = "A sweet substance especially from the plants sugar cane and sugar beet, used to make food and drinks sweet",
                    UseExample = "Add one dessertspoon of sugar"
                };
                context.Add(card_27);
                Card card_28 = new Card { DeckId = deck_6.Id, Word = "Chicken",
                    Defination = "A type of bird kept on a farm for its eggs or its meat, or the meat of this bird that is cooked and eaten",
                    UseExample = "Fry four chicken joints in a pan with some mushrooms and garlic"
                };
                context.Add(card_28);

                Deck deck_7 = await context.Decks.FirstOrDefaultAsync(d => d.RepeatDay == 64 && d.DeckBoxId == deckBox.Id);
                Card card_29 = new Card { DeckId = deck_7.Id, Word = "Tomato",
                    Defination = "A round, red fruit with a lot of seeds, eaten cooked or uncooked as a vegetable, for example in salads or sauces",
                    UseExample = "Layer the pasta with slices of tomato"
                };
                context.Add(card_29);
                Card card_30 = new Card { DeckId = deck_7.Id, Word = "Apple",
                    Defination = "A round fruit with firm, white flesh and a green, red, or yellow skin",
                    UseExample = "He was munching on an apple"
                };
                context.Add(card_30);
                Card card_31 = new Card { DeckId = deck_7.Id, Word = "Cherry",
                    Defination = "A small, round, soft red or black fruit with a single hard seed in the middle, or the tree on which the fruit grows",
                    UseExample = "She placed a bowl of cherries on the table"
                };
                context.Add(card_31);
                context.SaveChanges();

                Image image = new Image { DeckBoxId = deckBox.Id, DeckBoxImageName = "DefaultDeckBox.jpg" };
                context.Add(image);
                Image image_1 = new Image { CardId = card_1.Id, CardImageName = "Onion.jpg" };
                context.Add(image_1);
                Image image_2 = new Image { CardId = card_3.Id, CardImageName = "Horseradish.jpg" };
                context.Add(image_2);
                Image image_3 = new Image { CardId = card_5.Id, CardImageName = "Mushroom.jpg" };
                context.Add(image_3);
                Image image_4 = new Image { CardId = card_6.Id, CardImageName = "Lamb.jpg" };
                context.Add(image_4);
                Image image_5 = new Image { CardId = card_8.Id, CardImageName = "Champagne.jpg" };
                context.Add(image_5);
                Image image_6 = new Image { CardId = card_11.Id, CardImageName = "Bream.jpg" };
                context.Add(image_6);
                Image image_7 = new Image { CardId = card_13.Id, CardImageName = "Cabbage.jpg" };
                context.Add(image_7);
                Image image_8 = new Image { CardId = card_15.Id, CardImageName = "Sturgeon.jpg" };
                context.Add(image_8);
                Image image_9 = new Image { CardId = card_16.Id, CardImageName = "Cognac.jpg" };
                context.Add(image_9);
                Image image_10 = new Image { CardId = card_20.Id, CardImageName = "Melon.jpg" };
                context.Add(image_10);
                Image image_11 = new Image { CardId = card_23.Id, CardImageName = "Turkey.jpg" };
                context.Add(image_11);
                Image image_12 = new Image { CardId = card_24.Id, CardImageName = "Beer.jpg" };
                context.Add(image_12);
                Image image_13 = new Image { CardId = card_26.Id, CardImageName = "Bread.jpg" };
                context.Add(image_13);
                Image image_14 = new Image { CardId = card_29.Id, CardImageName = "Tomato.jpg" };
                context.Add(image_14);
                context.SaveChanges();
            }
        }
    }
}
