﻿using MemoryCards.Models;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MemoryCards
{
    public class MemoryContext : IdentityDbContext<User>
    {
        public DbSet<DeckBox> DeckBoxes { get; set; }
        public DbSet<Deck> Decks { get; set; }
        public DbSet<Card> Cards { get; set; }
        public DbSet<Image> Images { get; set; }
        public MemoryContext(DbContextOptions<MemoryContext> options)
           : base(options)
        {
        }
    }
}
