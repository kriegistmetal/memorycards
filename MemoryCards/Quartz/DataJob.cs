﻿using MemoryCards.Models;
using MemoryCards.Workers;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Quartz;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MemoryCards.Quartz
{
    public class DataJob : IJob
    {
        private readonly MemoryContext _context;
        private readonly IServiceScopeFactory _serviceScopeFactory;
        private readonly ILogger<Startup> _logger;
       
        public DataJob(IServiceScopeFactory serviceScopeFactory, MemoryContext context, ILogger<Startup> logger)
        {
            _serviceScopeFactory = serviceScopeFactory;
            _context = context;
            _logger = logger;
        }

        public async Task  Execute(IJobExecutionContext context)
        {
            List<DeckBox> deckBoxes = new List<DeckBox>();

            List<Deck> decks = new List<Deck>();
               
            using (var scope = _serviceScopeFactory.CreateScope())
            {
                try
                {
                    var emailsender = scope.ServiceProvider.GetService<IEmailSender>();
                    var memoryContext = scope.ServiceProvider.GetService<MemoryContext>();
                    deckBoxes = memoryContext.DeckBoxes.Include("User").Include(p => p.Decks).ThenInclude(p => p.Cards).ToList();
                    foreach (var d in deckBoxes)
                    {
                        decks.AddRange(d.Decks);
                    }
                    List<Deck> sendDeck = new List<Deck>();
                    foreach (var c in decks)
                    {
                       if (c.LastSendMsgDate.DayOfYear == DateTime.Now.DayOfYear)
                        {
                            sendDeck.Add(c);
                        }
                    }
                    foreach (var c in sendDeck)
                    {
                        if (c.Cards.Count != 0)
                        {
                            c.LastSendMsgDate = DateTime.Now.AddDays(c.RepeatDay);
                            memoryContext.Update(c);
                            memoryContext.SaveChanges();
                            if (c.DeckBox.User.Country == "Russia" || c.DeckBox.User.Country == "Kyrgyzstan")
                            {
                                await emailsender.SendEmailAsync($"{c.DeckBox.User.Email}", $"Пришло время повторить словарь {c.DeckBox.Title}",
                                $"<div style='padding: 0 calc(50% - 270px); margin-top: 10px;'><h3 style='text-align: center;font-size: 26px;color: black; font-weight: 600;'>Здравствуйте, {c.DeckBox.User.Login}!</h3> " +
                                $"<p style='font-size: 18px; font-weight: 600; text-align: center; color: black;'>Пришло время повторить коробку в словаре <span style='color: green; text-decoration: underline;'>{c.DeckBox.Title}</span> с частотой повторения <span style='color: green; text-decoration: underline;'>{c.RepeatDay}</span></p></div> "
                              );
                            }
                            else
                            {
                                await emailsender.SendEmailAsync($"{c.DeckBox.User.Email}", $"It's time to repeat the dictionary {c.DeckBox.Title}",
                             $"<div style='padding: 0 calc(50% - 270px); margin-top: 10px;'><h3 style='text-align: center;font-size: 26px;color: black; font-weight: 600;'>Hello, {c.DeckBox.User.Login}!</h3> " +
                             $"<p style='font-size: 18px; font-weight: 600; text-align: center; color: black;'>It's time to redo the box in opportunity <span style='color: green; text-decoration: underline;'>{c.DeckBox.Title}</span> with a repetition rate <span style='color: green; text-decoration: underline;'>{c.RepeatDay}</span></p></div> "
                             );
                            }
                            sendDeck.Remove(c);
                        }
                    }
                }
                catch(Exception ex)
                {
                    _logger.LogInformation(ex.ToString());
                }                   
            }
        }
    }
}
