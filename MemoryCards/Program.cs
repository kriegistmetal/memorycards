using MemoryCards.Data;
using MemoryCards.Quartz;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MemoryCards
{
    public class Program
    {
        public static async Task Main(string[] args)
        {    
            var host = BuildWebHost(args);
             using (var scope = host.Services.CreateScope())
             {
                 var serviceProvider = scope.ServiceProvider;
                 try
                 {
                      DataScheduler.Start(serviceProvider);
                      await AppSeeds.CreateDataDefaultDeckboxAsync(serviceProvider);
                 }
                 catch (Exception)
                 {
                     throw;
                 }
             }
             host.Run();

        }
        public static IWebHost BuildWebHost(string[] args) =>
           WebHost.CreateDefaultBuilder(args)
               .UseStartup<Startup>()
               .Build();
    }
}