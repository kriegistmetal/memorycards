﻿$('body').keydown(function (e) {
    console.log(e.keyCode);
    if (e.keyCode == 39) {
        $('.next').on('click', plusSlides(1));
    }
    else if (e.keyCode == 37) {
        $('.prev').on('click', plusSlides(-1));
    }
});
$(document).ready(function () {
    let nextid = $('.slideshow-container').data('nextid');
    if (nextid) {
        $('.mySlides').css('display', 'none');
        $('#' + nextid + '').parent().parent().parent().parent().css('display', 'block');
    }
});