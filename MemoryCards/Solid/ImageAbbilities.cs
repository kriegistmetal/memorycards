﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace MemoryCards.Solid
{

    public class ImageAbillities
    {
        private readonly IWebHostEnvironment _webHostEnvironment;
        public ImageAbillities(IWebHostEnvironment webHostEnvironment)
        {
            _webHostEnvironment = webHostEnvironment;
        }
        public static string UploadImage(IFormFile image, string folderpath)
        {
            string fileName = $"{Guid.NewGuid().ToString()}_{image.FileName}";
            string filePath = Path.Combine(folderpath, fileName);

            using (var stream = new FileStream(filePath, FileMode.Create))
            {
                image.CopyTo(stream);
            }
            return fileName;
        }


        public static void DeleteImage(string imageName, string folderPath)
        {
            string filePath = Path.Combine(folderPath, imageName);
            System.IO.File.Delete(filePath);
        }
    }
}
