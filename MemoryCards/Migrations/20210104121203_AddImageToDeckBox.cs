﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace MemoryCards.Migrations
{
    public partial class AddImageToDeckBox : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_Images_DeckBoxId",
                table: "Images");

            migrationBuilder.DropColumn(
                name: "ImageName",
                table: "DeckBoxes");

            migrationBuilder.CreateIndex(
                name: "IX_Images_DeckBoxId",
                table: "Images",
                column: "DeckBoxId",
                unique: true,
                filter: "[DeckBoxId] IS NOT NULL");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_Images_DeckBoxId",
                table: "Images");

            migrationBuilder.AddColumn<string>(
                name: "ImageName",
                table: "DeckBoxes",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Images_DeckBoxId",
                table: "Images",
                column: "DeckBoxId");
        }
    }
}
