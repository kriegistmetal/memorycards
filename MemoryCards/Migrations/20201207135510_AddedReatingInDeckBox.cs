﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace MemoryCards.Migrations
{
    public partial class AddedReatingInDeckBox : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Cards_DeckBoxes_DeckBoxId",
                table: "Cards");

            migrationBuilder.DropIndex(
                name: "IX_Cards_DeckBoxId",
                table: "Cards");

            migrationBuilder.DropColumn(
                name: "DeckBoxId",
                table: "Cards");

            migrationBuilder.AddColumn<int>(
                name: "Rating",
                table: "DeckBoxes",
                type: "int",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Rating",
                table: "DeckBoxes");

            migrationBuilder.AddColumn<int>(
                name: "DeckBoxId",
                table: "Cards",
                type: "int",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Cards_DeckBoxId",
                table: "Cards",
                column: "DeckBoxId");

            migrationBuilder.AddForeignKey(
                name: "FK_Cards_DeckBoxes_DeckBoxId",
                table: "Cards",
                column: "DeckBoxId",
                principalTable: "DeckBoxes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
