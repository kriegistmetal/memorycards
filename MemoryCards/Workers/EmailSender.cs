﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Threading.Tasks;

namespace MemoryCards.Workers
{
	public class EmailSender : IEmailSender
	{
		private readonly IServiceScopeFactory serviceScopeFactory;

		public EmailSender(IServiceScopeFactory serviceScopeFactory)
		{
			this.serviceScopeFactory = serviceScopeFactory;
		}
		public Task SendEmailAsync(string email, string subject, string message)
		{
			var scope = serviceScopeFactory.CreateScope();
			var _configuration = scope.ServiceProvider.GetService<IConfiguration>();
			var from = _configuration["Email"];
			var pass = _configuration["Password"];
			var gateway = _configuration["Gateway"];
			var port = _configuration["Port"];
			SmtpClient client = new SmtpClient(gateway, Convert.ToInt32(port));
			client.DeliveryMethod = SmtpDeliveryMethod.Network;
			client.UseDefaultCredentials = false;
			client.Credentials = new System.Net.NetworkCredential(from, pass);
			client.EnableSsl = true;
			var mail = new MailMessage(from, email);
			mail.Subject = subject;
			mail.Body = message;
			mail.IsBodyHtml = true;
			return client.SendMailAsync(mail);
		}
	}
}
