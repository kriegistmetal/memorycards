﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using Xunit;

namespace MemoryCards.SeleniumUITests.AccountControllerAI
{
   public class LogOffTests : IDisposable
    {
        private readonly IWebDriver _driver;
        public LogOffTests()
        {
            ChromeOptions option = new ChromeOptions();
            option.AddArguments("--window-size=500,500");
            _driver = new ChromeDriver(option);
        }
        public void Dispose()
        {
            _driver.Quit();
            _driver.Dispose();
        }
        [Fact]
        public void LogOutTest()
        {
            _driver.Navigate().GoToUrl("https://localhost:44394/DeckBoxes/Create");
            IWebElement elementEmail = _driver.FindElement(By.Id("Email"));
            IWebElement elementPassword = _driver.FindElement(By.Id("Password"));
            IWebElement submitButton = _driver.FindElement(By.XPath("/html/body/div/main/div/div/section/form/div[4]/button"));
            elementEmail.SendKeys("seleniumdefaulttestuser@mail.ru");
            elementPassword.SendKeys("Integration_1_one");
            Thread.Sleep(1000);
            submitButton.Click();
            Thread.Sleep(2000);

            Assert.Equal("Создание нового словаря - MemoryCards", _driver.Title);
            Assert.Contains("Название", _driver.PageSource);
            Assert.Contains("Описание", _driver.PageSource);
            Assert.Contains("Фоновая картинка", _driver.PageSource);
            Assert.Contains("Цвет фона", _driver.PageSource);
            Assert.Contains("Приватный", _driver.PageSource);
            Assert.Contains("Создать словарь", _driver.PageSource);
            Assert.Contains("Назад", _driver.PageSource);
            IWebElement FirstDropdownforsmalldisplays = _driver.FindElement(By.XPath("/html/body/header/nav/div/button/span"));
            FirstDropdownforsmalldisplays.Click();
            Thread.Sleep(500);
            IWebElement OpenDropdownListAccount = _driver.FindElement(By.XPath("/html/body/header/nav/div/div/div/ul/li[1]/a"));
            OpenDropdownListAccount.Click();
            Thread.Sleep(500);
            IWebElement exitFromApp = _driver.FindElement(By.XPath("/html/body/header/nav/div/div/div/ul/li[1]/div/form/input[1]"));
            exitFromApp.Click();

        }
    }
}
