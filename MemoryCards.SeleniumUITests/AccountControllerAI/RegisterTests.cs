﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using Xunit;

namespace MemoryCards.SeleniumUITests.AccountControllerAI
{
    public class RegisterTests : IDisposable
    {
        private readonly IWebDriver _driver;
        public RegisterTests()
        {
            ChromeOptions option = new ChromeOptions();
            option.AddArguments("--window-size=500,500");
            _driver = new ChromeDriver(option);
        }
        public void Dispose()
        {
            Thread.Sleep(2000);
            _driver.Quit();
            _driver.Dispose();
        }

        [Fact]
        public void ReturnsRegisterViewTest()
        {
            _driver.Navigate().GoToUrl("https://localhost:44394/Account/Register");
            Assert.Equal("Регистрация нового пользователя - MemoryCards", _driver.Title);
            Assert.Contains("Регистрация нового пользователя", _driver.PageSource);
            Assert.Contains("Страна проживания", _driver.PageSource);
            Assert.Contains("Логин", _driver.PageSource);
            Assert.Contains("Пароль", _driver.PageSource);
            Assert.Contains("Подтвердить пароль", _driver.PageSource);
            Assert.Contains("Зарегистрироваться", _driver.PageSource);
        }

        [Fact]
        public void RegisterEnterModelDataTest()
        {
            _driver.Navigate().GoToUrl("https://localhost:44394/Account/Register");

            IWebElement email = _driver.FindElement(By.Id("Email"));
            email.SendKeys("seleniumdefaulttestuser@mail.ru");
            IWebElement country = _driver.FindElement(By.Id("Country"));            
            country.SendKeys("Canada");
            IWebElement login = _driver.FindElement(By.Id("Login"));            
            login.SendKeys("TestLogin");
            IWebElement password = _driver.FindElement(By.Id("Password"));           
            password.SendKeys("Integration_1_one");
            IWebElement passwordConfirm = _driver.FindElement(By.Id("PasswordConfirm"));            
            passwordConfirm.SendKeys("Integration_1_one");

            Assert.NotNull(email);
            Assert.NotNull(country);
            Assert.NotNull(login);
            Assert.NotNull(password);
            Assert.NotNull(passwordConfirm);
            _driver.FindElement(By.ClassName("btn")).Click();
        }
        [Fact]
        public void RegisterModelRequiredTest()
        {
            _driver.Navigate().GoToUrl("https://localhost:44394/Account/Register");
            _driver.FindElement(By.ClassName("btn")).Click();

            string errorMessageEmail = _driver.FindElement(By.Id("validemail")).Text;
            string errorMessageCountry = _driver.FindElement(By.Id("validcountry")).Text;
            string errorMessageLogin = _driver.FindElement(By.Id("validlogin")).Text;
            string errorMessagePassword = _driver.FindElement(By.Id("validpassword")).Text;
            string errorMessagePasswordConfirm = _driver.FindElement(By.Id("validpasswordconfirm")).Text;

            Assert.Equal("Пожалуйста, введите свой адрес электронной почты", errorMessageEmail);
            Assert.Equal("Пожалуйста, укажите страну где вы проживаете", errorMessageCountry);
            Assert.Equal("Пожалуйста, укажите свой логин", errorMessageLogin);
            Assert.Equal("Пожалуйста, введите свой пароль", errorMessagePassword);
            Assert.Equal("Пожалуйста, подтвердите свой пароль", errorMessagePasswordConfirm);
        }
        [Fact]
        public void RegisterEmailRegularExpressionTest()
        {
            _driver.Navigate().GoToUrl("https://localhost:44394/Account/Register");
            _driver.FindElement(By.Id("Email")).SendKeys("Email");
            _driver.FindElement(By.ClassName("btn")).Click();

            string errorMessageEmail = _driver.FindElement(By.Id("validemail")).Text;
            Assert.Equal("Некорректный адрес электронной почты", errorMessageEmail);
        }
        [Fact]
        public void RegisterLoginStringLengthTest()
        {
            _driver.Navigate().GoToUrl("https://localhost:44394/Account/Register");
            _driver.FindElement(By.Id("Login")).SendKeys("L");
            _driver.FindElement(By.ClassName("btn")).Click();

            string errorMessageLogin = _driver.FindElement(By.Id("validlogin")).Text;
            Assert.Equal("Длина логина должна быть от 3 до 15 символов", errorMessageLogin);
        }
        [Fact]
        public void RegisterPasswordConfirmCompareTest()
        {
            _driver.Navigate().GoToUrl("https://localhost:44394/Account/Register");
            _driver.FindElement(By.Id("Password")).SendKeys("123");
            _driver.FindElement(By.Id("PasswordConfirm")).SendKeys("321");
            _driver.FindElement(By.ClassName("btn")).Click();

            string errorMessagePasswordConfirm = _driver.FindElement(By.Id("validpasswordconfirm")).Text;
            Assert.Equal("Ваши пароли не совпадают", errorMessagePasswordConfirm);
        }
    }
}
