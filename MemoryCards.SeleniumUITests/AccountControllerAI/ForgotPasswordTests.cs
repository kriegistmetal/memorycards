﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace MemoryCards.SeleniumUITests.AccountControllerAI
{
    public class ForgotPasswordTests : IDisposable
    {
        private readonly IWebDriver _driver;
        public ForgotPasswordTests()
        {
            ChromeOptions option = new ChromeOptions();
            option.AddArguments("--window-size=500,500");
            _driver = new ChromeDriver(option);
        }
        public void Dispose()
        {
            _driver.Quit();
            _driver.Dispose();
        }
        [Fact]
        public void ReturnForgotPasswordViewTest()
        {
            _driver.Navigate().GoToUrl("https://localhost:44394/Account/ForgotPassword");
            Assert.Equal("Забыли пароль? - MemoryCards", _driver.Title);
            Assert.Contains("Адрес электронной почты", _driver.PageSource);
            Assert.Contains("Отправить", _driver.PageSource);
        }

        [Fact]
        public void ForgotPasswordEmailRegularExpressionTest()
        {
            _driver.Navigate().GoToUrl("https://localhost:44394/Account/ForgotPassword");
            _driver.FindElement(By.Id("Email")).SendKeys("Email");
            _driver.FindElement(By.ClassName("btn")).Click();

            var errorMessageEmail = _driver.FindElement(By.Id("validemail")).Text;
            Assert.Equal("Некорректный адрес электронной почты", errorMessageEmail);
        }

        [Fact]
        public void ForgotPasswordRequired()
        {
            _driver.Navigate().GoToUrl("https://localhost:44394/Account/ForgotPassword");
            _driver.FindElement(By.ClassName("btn")).Click();

            IAlert alert = _driver.SwitchTo().Alert();
            alert.Accept();
            var errorMessageEmail = _driver.FindElement(By.Id("validemail")).Text;
            Assert.Equal("Пожалуйста, введите свой адрес электронной почты", errorMessageEmail);

        }
        [Fact]
        public void ForgotPasswordEmailStringLengthTest()
        {
            _driver.Navigate().GoToUrl("https://localhost:44394/Account/ForgotPassword");
            _driver.FindElement(By.Id("Email")).SendKeys("s@r.a");
            _driver.FindElement(By.ClassName("btn")).Click();

            var errorMessageLogin = _driver.FindElement(By.Id("validemail")).Text;
            Assert.Equal("Длина адреса электронной почты должна быть от 6 до 31 символов", errorMessageLogin);
        }
    }
}
