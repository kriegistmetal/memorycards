﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using Xunit;

namespace MemoryCards.SeleniumUITests.AccountControllerAI
{
    public class LoginTests : IDisposable
    {
        private readonly IWebDriver _driver;
        public LoginTests()
        {
            ChromeOptions option = new ChromeOptions();
            option.AddArguments("--window-size=500,500");
            _driver = new ChromeDriver(option);
        }
        public void Dispose()
        {
            Thread.Sleep(2000);
            _driver.Quit();
            _driver.Dispose();
        }

        [Fact]
        public void ReturnsLoginViewTest()
        {
            _driver.Navigate().GoToUrl("https://localhost:44394/Account/Login?ReturnUrl=%2F");
            Assert.Equal("Вход в приложение - MemoryCards", _driver.Title);
            Assert.Contains("Вход в приложение", _driver.PageSource);
            Assert.Contains("Адрес электронной почты", _driver.PageSource);
            Assert.Contains("Пароль", _driver.PageSource);
            Assert.Contains("Запомнить?", _driver.PageSource);
            Assert.Contains("Войти", _driver.PageSource);
            Assert.Contains("Забыли свой пароль?", _driver.PageSource);
        }
        [Fact]
        public void LoginEnterModelDataTest()
        {
            _driver.Navigate().GoToUrl("https://localhost:44394/Account/Login?ReturnUrl=%2F");
            IWebElement email = _driver.FindElement(By.Id("Email"));
            email.SendKeys("seleniumdefaulttestuser@mail.ru");
            IWebElement password = _driver.FindElement(By.Id("Password"));
            password.SendKeys("Integration_1_one");            

            Assert.NotNull(email);            
            Assert.NotNull(password);            
            _driver.FindElement(By.ClassName("btn")).Click();
        }
        [Fact]
        public void LoginModelRequiredTest()
        {
            _driver.Navigate().GoToUrl("https://localhost:44394/Account/Login?ReturnUrl=%2F");
            _driver.FindElement(By.ClassName("btn")).Click();

            string errorMessageEmail = _driver.FindElement(By.Id("validemail")).Text;
            string errorMessagePassword = _driver.FindElement(By.Id("validpassword")).Text;            

            Assert.Equal("Пожалуйста, введите адрес вашей электронной почты", errorMessageEmail);            
            Assert.Equal("Пожалуйста, введите свой пароль", errorMessagePassword);            
        }
        [Fact]
        public void LoginEmailRegularExpressionTest()
        {
            _driver.Navigate().GoToUrl("https://localhost:44394/Account/Login?ReturnUrl=%2F");
            _driver.FindElement(By.Id("Email")).SendKeys("Email");
            _driver.FindElement(By.ClassName("btn")).Click();

            string errorMessageEmail = _driver.FindElement(By.Id("validemail")).Text;
            Assert.Equal("Некорректный адрес электронной почты", errorMessageEmail);
        }
    }
}
