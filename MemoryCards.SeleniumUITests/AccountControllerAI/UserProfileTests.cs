﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace MemoryCards.SeleniumUITests.AccountControllerAI
{
   public class UserProfileTests : IDisposable
    {
        private readonly IWebDriver _driver;
        public UserProfileTests()
        {
            ChromeOptions option = new ChromeOptions();
            option.AddArguments("--window-size=500,500");
            _driver = new ChromeDriver(option);
        }
        public void Dispose()
        {
            _driver.Quit();
            _driver.Dispose();
        }
        private void Login()
        {
            _driver.Navigate().GoToUrl("https://localhost:44394/Account/UserProfile/RealMadrid-Atletico-4-1");
            IWebElement email = _driver.FindElement(By.Id("Email"));
            email.SendKeys("seleniumdefaulttestuser@mail.ru");
            IWebElement password = _driver.FindElement(By.Id("Password"));
            password.SendKeys("Integration_1_one");
            _driver.FindElement(By.ClassName("btn")).Click();
        }
        [Fact]
        public void ReturnUserProfileViewTest()
        {
            Login();
            Assert.Equal("Профиль пользователя - MemoryCards", _driver.Title);
            Assert.Contains("SomeUser", _driver.PageSource);
            Assert.Contains("Kyrgyzstan", _driver.PageSource);
            Assert.Contains("1", _driver.PageSource);
            Assert.Contains("Доступные словари", _driver.PageSource);
            Assert.Contains("1", _driver.PageSource);
            Assert.Contains("Всего словарей", _driver.PageSource);
            Assert.Contains("Словари", _driver.PageSource);
            Assert.Contains("TitleForSearcher", _driver.PageSource);
            Assert.Contains("sadjsadkasjdaksl", _driver.PageSource);
            Assert.Contains("Добавить к себе", _driver.PageSource);
        }
        [Fact]
        public void AddDictionaryIfExistedTest()
        {
            Login();
            IWebElement addToMyDicts = _driver.FindElement(By.XPath("/html/body/div/main/div/div/div/div[3]/div[2]/div/div/div/a[2]"));
            Assert.NotNull(addToMyDicts);
            addToMyDicts.Click();
        }
    }
}
