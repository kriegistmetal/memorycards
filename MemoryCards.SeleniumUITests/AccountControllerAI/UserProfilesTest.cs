﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace MemoryCards.SeleniumUITests.AccountControllerAI
{
    public class UserProfilesTest : IDisposable
    {
        private readonly IWebDriver _driver;
        private bool recordFound;
        public UserProfilesTest()
        {
            ChromeOptions option = new ChromeOptions();
            option.AddArguments("--window-size=500,500");
            _driver = new ChromeDriver(option);
        }
        public void Dispose()
        {
            _driver.Quit();
            _driver.Dispose();
        }
        private void Login()
        {
            _driver.Navigate().GoToUrl("https://localhost:44394/Account/UserProfiles");
            IWebElement email = _driver.FindElement(By.Id("Email"));
            email.SendKeys("seleniumdefaulttestuser@mail.ru");
            IWebElement password = _driver.FindElement(By.Id("Password"));
            password.SendKeys("Integration_1_one");
            _driver.FindElement(By.ClassName("btn")).Click();
        }
        [Fact]
        public void ReturnUserProfilesViewTest()
        {
            Login();
            _driver.Navigate().GoToUrl("https://localhost:44394/Account/UserProfiles");
            Assert.Equal("Пользователи сайта - MemoryCards", _driver.Title);
            Assert.Contains("Поиск пользователей", _driver.PageSource);
            Assert.Contains("Логин", _driver.PageSource);
            Assert.Contains("Страна проживания", _driver.PageSource);
            Assert.Contains("Адрес электронной почты", _driver.PageSource);
            Assert.Contains("SomeUser", _driver.PageSource);
            Assert.Contains("Kyrgyzstan", _driver.PageSource);
            Assert.Contains("thisuserforsearch@mail.ru", _driver.PageSource);
            Assert.Contains("Предыдущая", _driver.PageSource);
            Assert.Contains("Следующая", _driver.PageSource);
            Assert.Contains("1", _driver.PageSource);
        }
        [Fact]
        public void SearchUserInUserProfilesTest()
        {
            Login();
            var search = _driver.FindElement(By.Id("#search"));
            search.SendKeys("asds");
            Assert.NotNull(search);
            search.Submit();

        }
        [Fact]
        public void GetIntoTestUserProfileTest()
        {
            Login();
            IWebElement getInto = _driver.FindElement(By.XPath("/html/body/div/main/div/table/tbody/tr"));
            Assert.NotNull(getInto);
            getInto.Click();
        }
    }
 }

