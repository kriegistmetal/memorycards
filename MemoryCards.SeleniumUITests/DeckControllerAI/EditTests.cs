﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using Xunit;

namespace MemoryCards.SeleniumUITests.DeckControllerAI
{
   public class EditTests : IDisposable
    {
        private readonly IWebDriver _driver;

        public EditTests()
        {
            ChromeOptions option = new ChromeOptions();
            option.AddArguments("--window-size=1000,1000");
            _driver = new ChromeDriver(option);
        }
        public void Dispose()
        {
            Thread.Sleep(2000);
            _driver.Quit();
            _driver.Dispose();
        }
        private void Login()
        {
            _driver.Navigate().GoToUrl("https://localhost:44394/Account/Login?ReturnUrl=%2F");
            IWebElement email = _driver.FindElement(By.Id("Email"));
            email.SendKeys("seleniumdefaulttestuser@mail.ru");
            IWebElement password = _driver.FindElement(By.Id("Password"));
            password.SendKeys("Integration_1_one");
            _driver.FindElement(By.ClassName("btn")).Click();
        }
        [Fact]
        public void EditViewTest()
        {
            Login();
            Assert.Equal("Мои словари - MemoryCards", _driver.Title);
            Assert.Contains("Пользователи", _driver.PageSource);
            Assert.Contains("Мои словари", _driver.PageSource);
            IWebElement ReturnDeckSIndex = _driver.FindElement(By.XPath("/html/body/div/main/div/div/div/div/div[2]/a/div"));
            ReturnDeckSIndex.Click();

            Assert.Equal("Коробки - MemoryCards", _driver.Title);
            Assert.Contains("Пользователи", _driver.PageSource);
            Assert.Contains("Количество слов", _driver.PageSource);
            _driver.FindElement(By.Id("edit")).Click();

            Assert.Equal("Редактирование коробки - MemoryCards", _driver.Title);
            Assert.Contains("Сохранить", _driver.PageSource);
            Assert.Contains("Редактирование коробки", _driver.PageSource);
            Assert.Contains("Частота повторений в днях", _driver.PageSource);

            IWebElement repeatday = _driver.FindElement(By.Id("repeatday"));
            repeatday.Clear();
            repeatday.SendKeys("0");

            _driver.FindElement(By.Id("DeckEdit")).Click();

            string errorMessageRDay = _driver.FindElement(By.Id("validRepeatDay")).Text;
           
            Assert.Equal("Введеное значение недопустимо, введите цифры от 1 до 360", errorMessageRDay);
             
        }
        [Fact]
        public void EditNotValidRepeatDay()
        {
            Login();
            Assert.Equal("Мои словари - MemoryCards", _driver.Title);
            Assert.Contains("Пользователи", _driver.PageSource);
            Assert.Contains("Мои словари", _driver.PageSource);
            IWebElement ReturnDeckSIndex = _driver.FindElement(By.ClassName("work"));
            string url = ReturnDeckSIndex.GetAttribute("href");
            _driver.Navigate().GoToUrl(url);

            Assert.Equal("Коробки - MemoryCards", _driver.Title);
            Assert.Contains("Пользователи", _driver.PageSource);
            Assert.Contains("Количество слов", _driver.PageSource);
            _driver.FindElement(By.Id("edit")).Click();

            Assert.Equal("Редактирование коробки - MemoryCards", _driver.Title);
            Assert.Contains("Сохранить", _driver.PageSource);
            Assert.Contains("Редактирование коробки", _driver.PageSource);
            Assert.Contains("Частота повторений в днях", _driver.PageSource);

            IWebElement repeatday = _driver.FindElement(By.Id("repeatday"));
            repeatday.Clear();
            repeatday.SendKeys("1");

            _driver.FindElement(By.Id("DeckEdit")).Click();

            string errorMessageRDay = _driver.FindElement(By.Id("deckerror")).Text;

            Assert.Contains("Коробка с указанным значением уже существует! Повторите попытку", errorMessageRDay);

        }
    }
}
