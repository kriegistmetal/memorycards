﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using Xunit;

namespace MemoryCards.SeleniumUITests.DeckBoxesControllerAI
{
    public class CreateTests : IDisposable
    {
        private readonly IWebDriver _driver;
        public CreateTests()
        {
            ChromeOptions option = new ChromeOptions();
            option.AddArguments("--window-size=1000,1000");
            _driver = new ChromeDriver(option);
        }
        public void Dispose()
        {
            Thread.Sleep(2000);
            _driver.Quit();
            _driver.Dispose();
        }
        private void Login()
        {
            _driver.Navigate().GoToUrl("https://localhost:44394/Account/Login?ReturnUrl=%2F");
            IWebElement email = _driver.FindElement(By.Id("Email"));
            email.SendKeys("seleniumdefaulttestuser@mail.ru");
            IWebElement password = _driver.FindElement(By.Id("Password"));
            password.SendKeys("Integration_1_one");
            _driver.FindElement(By.ClassName("btn")).Click();
        }
        [Fact]
        public void ReturnsCreateViewTest()
        {
            Login();

            _driver.Navigate().GoToUrl("https://localhost:44394/DeckBoxes/Create");
            Assert.Equal("Создание нового словаря - MemoryCards", _driver.Title);
            Assert.Contains("Пользователи", _driver.PageSource);
            Assert.Contains("Создание нового словаря", _driver.PageSource);
            Assert.Contains("Название", _driver.PageSource);
            Assert.Contains("Описание", _driver.PageSource);
            Assert.Contains("Фоновая картинка", _driver.PageSource);
            Assert.Contains("Цвет фона", _driver.PageSource);
            Assert.Contains("Приватный", _driver.PageSource);
            Assert.Contains("Создать словарь", _driver.PageSource);
            Assert.Contains("Назад", _driver.PageSource);
        }
        [Fact]
        public void CreateNewDeckBoxTest_ReturnsIndexView()
        {
            Login();

            _driver.Navigate().GoToUrl("https://localhost:44394/DeckBoxes/Create");
            IWebElement title = _driver.FindElement(By.Id("Title"));
            title.SendKeys("TestTitle");
            IWebElement description = _driver.FindElement(By.Id("Description"));
            description.SendKeys("TestDescription");
            IWebElement deckBoxcColor = _driver.FindElement(By.Id("pickcolor"));
            deckBoxcColor.SendKeys("#000066");
            _driver.FindElement(By.XPath("/html/body/div/main/div[1]/div/form/div[6]/input")).Click();
            
            Assert.Equal("Мои словари - MemoryCards", _driver.Title);
            Assert.Contains("Пользователи", _driver.PageSource);
            Assert.Contains("Мои словари", _driver.PageSource);
            Assert.Contains("Создать новый словарь", _driver.PageSource);
            Assert.Contains("TestTitle", _driver.PageSource);
            Assert.Contains("TestDescription", _driver.PageSource);
            Assert.Contains("Изучено слов", _driver.PageSource);
            Assert.Contains("Количество слов в словаре", _driver.PageSource);
            Assert.Contains("Публичный", _driver.PageSource);
            Assert.Contains("Поделиться", _driver.PageSource);
            Assert.Contains("Редактировать", _driver.PageSource);
            Assert.Contains("Удалить", _driver.PageSource);
        }
        [Fact]
        public void DeckBoxModelRequiredTest()
        {
            Login();

            _driver.Navigate().GoToUrl("https://localhost:44394/DeckBoxes/Create");            
            _driver.FindElement(By.XPath("/html/body/div/main/div[1]/div/form/div[6]/input")).Click();

            string errorMessageTitle = _driver.FindElement(By.Id("validTitle")).Text;           
            Assert.Equal("Пожалуйста, введите название словаря", errorMessageTitle);
        }
        [Fact]
        public void DeckBoxCreateModelStringLengthTest()
        {
            Login();

            _driver.Navigate().GoToUrl("https://localhost:44394/DeckBoxes/Create");
            IWebElement title = _driver.FindElement(By.Id("Title"));
            title.SendKeys("T");
            IWebElement description = _driver.FindElement(By.Id("Description"));
            description.SendKeys("T");
            _driver.FindElement(By.XPath("/html/body/div/main/div[1]/div/form/div[6]/input")).Click();

            string errorMessageTitle = _driver.FindElement(By.Id("validTitle")).Text;
            string errorMessageDescription = _driver.FindElement(By.Id("validDescription")).Text;
            Assert.Equal("Длина названия должна быть от 3 до 50 символов", errorMessageTitle);
            Assert.Equal("Длина описания должна быть от 3 до 50 символов", errorMessageDescription);
        }
        [Fact]
        public void ClickBack_ReturnsDeckBoxIndex()
        {
            Login();
            
            Assert.Equal("Мои словари - MemoryCards", _driver.Title);
            Assert.Contains("Пользователи", _driver.PageSource);
            Assert.Contains("Мои словари", _driver.PageSource);

            IWebElement editDeckbox = _driver.FindElement(By.Id("createDeckbox"));
            string url = editDeckbox.GetAttribute("href");
            _driver.Navigate().GoToUrl(url);

            _driver.FindElement(By.XPath("/html/body/div/main/div[2]/a")).Click();
            Assert.Equal("Мои словари - MemoryCards", _driver.Title);
            Assert.Contains("Пользователи", _driver.PageSource);
            Assert.Contains("Мои словари", _driver.PageSource);
        }
    }
}