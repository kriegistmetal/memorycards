﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using Xunit;

namespace MemoryCards.SeleniumUITests.DeckBoxesControllerAI
{
    public class IndexTests : IDisposable
    {
        private readonly IWebDriver _driver;
        public IndexTests()
        {
            ChromeOptions option = new ChromeOptions();
            option.AddArguments("--window-size=1000,1000");
            _driver = new ChromeDriver(option);
        }
        public void Dispose()
        {
            Thread.Sleep(2000);
            _driver.Quit();
            _driver.Dispose();
        }
        private void Login()
        {
            _driver.Navigate().GoToUrl("https://localhost:44394/Account/Login?ReturnUrl=%2F");
            IWebElement email = _driver.FindElement(By.Id("Email"));
            email.SendKeys("seleniumdefaulttestuser@mail.ru");
            IWebElement password = _driver.FindElement(By.Id("Password"));
            password.SendKeys("Integration_1_one");
            _driver.FindElement(By.ClassName("btn")).Click();
        }
        [Fact]
        public void ReturnsIndexViewTest()
        {
            Login();
            
            Assert.Equal("Мои словари - MemoryCards", _driver.Title);
            Assert.Contains("Пользователи", _driver.PageSource);
            Assert.Contains("Мои словари", _driver.PageSource);
            Assert.Contains("Создать новый словарь", _driver.PageSource);

            Assert.Contains("TestTitle", _driver.PageSource);
            Assert.Contains("TestDescription", _driver.PageSource);
            Assert.Contains("Изучено слов", _driver.PageSource);
            Assert.Contains("Количество слов в словаре", _driver.PageSource);
            Assert.Contains("Публичный", _driver.PageSource);
            Assert.Contains("Поделиться", _driver.PageSource);
            Assert.Contains("Редактировать", _driver.PageSource);
            Assert.Contains("Удалить", _driver.PageSource);
        }
        [Fact]
        public void ClickEditTest()
        {
            Login();
           
            Assert.Equal("Мои словари - MemoryCards", _driver.Title);
            Assert.Contains("Пользователи", _driver.PageSource);
            Assert.Contains("Мои словари", _driver.PageSource);

            IWebElement editDeckbox = _driver.FindElement(By.ClassName("editDeckbox"));
            string url = editDeckbox.GetAttribute("href");
            _driver.Navigate().GoToUrl(url);

            Assert.Equal("Редактирование словаря - MemoryCards", _driver.Title);
            Assert.Contains("Пользователи", _driver.PageSource);
            Assert.Contains("Редактирование словаря", _driver.PageSource);
            Assert.Contains("Название", _driver.PageSource);            
            Assert.Contains("TestTitle", _driver.PageSource);
            Assert.Contains("Описание", _driver.PageSource);
            Assert.Contains("TestDescription", _driver.PageSource);
            Assert.Contains("Фоновая картинка", _driver.PageSource);
            Assert.Contains("Цвет фона", _driver.PageSource);
            Assert.Contains("Приватный", _driver.PageSource);            
            Assert.Contains("Отредактировать", _driver.PageSource);
            Assert.Contains("Назад", _driver.PageSource);
        }
        [Fact]
        public void ClickDeleteTest()
        {
            Login();
                        
            Assert.Equal("Мои словари - MemoryCards", _driver.Title);
            Assert.Contains("Пользователи", _driver.PageSource);
            Assert.Contains("Мои словари", _driver.PageSource);

            IWebElement editDeckbox = _driver.FindElement(By.ClassName("deleteDeckbox"));
            string url = editDeckbox.GetAttribute("href");
            _driver.Navigate().GoToUrl(url);

            Assert.Equal("Мои словари - MemoryCards", _driver.Title);
            Assert.Contains("Пользователи", _driver.PageSource);
            Assert.Contains("Мои словари", _driver.PageSource);
            Assert.Contains("Создать новый словарь", _driver.PageSource);
        }
        [Fact]
        public void ClickCreateNewDeckboxTest()
        {
            Login();
            
            Assert.Equal("Мои словари - MemoryCards", _driver.Title);
            Assert.Contains("Пользователи", _driver.PageSource);
            Assert.Contains("Мои словари", _driver.PageSource);

            IWebElement editDeckbox = _driver.FindElement(By.Id("createDeckbox"));
            string url = editDeckbox.GetAttribute("href");
            _driver.Navigate().GoToUrl(url);

            Assert.Equal("Создание нового словаря - MemoryCards", _driver.Title);
            Assert.Contains("Пользователи", _driver.PageSource);
            Assert.Contains("Создание нового словаря", _driver.PageSource);
            Assert.Contains("Название", _driver.PageSource);
            Assert.Contains("Описание", _driver.PageSource);
            Assert.Contains("Фоновая картинка", _driver.PageSource);
            Assert.Contains("Цвет фона", _driver.PageSource);
            Assert.Contains("Приватный", _driver.PageSource);
            Assert.Contains("Создать словарь", _driver.PageSource);
            Assert.Contains("Назад", _driver.PageSource);
        }
    }
}