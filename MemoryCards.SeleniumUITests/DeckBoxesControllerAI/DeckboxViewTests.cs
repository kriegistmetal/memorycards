﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using Xunit;

namespace MemoryCards.SeleniumUITests.DeckBoxesControllerAI
{
    public class DeckboxViewTests : IDisposable
    {
        private readonly IWebDriver _driver;
        public DeckboxViewTests()
        {
            ChromeOptions option = new ChromeOptions();
            option.AddArguments("--window-size=1000,1000");
            _driver = new ChromeDriver(option);
        }
        public void Dispose()
        {
            Thread.Sleep(2000);
            _driver.Quit();
            _driver.Dispose();
        }
        private void Login()
        {
            _driver.Navigate().GoToUrl("https://localhost:44394/Account/Login?ReturnUrl=%2F");
            IWebElement email = _driver.FindElement(By.Id("Email"));
            email.SendKeys("seleniumdefaulttestuser@mail.ru");
            IWebElement password = _driver.FindElement(By.Id("Password"));
            password.SendKeys("Integration_1_one");
            _driver.FindElement(By.ClassName("btn")).Click();
        }
        [Fact]
        public void ReturnsDeckboxViewTest()
        {
            Login();
            var share = _driver.FindElement(By.Id("share"));
            string id = share.GetAttribute("name");           

            string link = $"{id}|{DateTime.Now.AddHours(2)}";
            var linkbytes = System.Text.Encoding.UTF8.GetBytes(link);
            string encUrl = System.Convert.ToBase64String(linkbytes);

            string getUrl = "https://localhost:44394/DeckBoxes/DeckboxView?deckboxid=" + encUrl;
            _driver.Navigate().GoToUrl(getUrl);

            Assert.Equal("Подарок - MemoryCards", _driver.Title);
            Assert.Contains("Пользователи", _driver.PageSource);
            Assert.Contains("поделился с вами словарем!", _driver.PageSource);
            Assert.Contains("Количество слов в словаре", _driver.PageSource);
            Assert.Contains("Взять себе", _driver.PageSource);
        }
        [Fact]
        public void ClickshareTest_ReturnsDeckboxViewIndex()
        {
            Login();

            var share = _driver.FindElement(By.Id("share"));
            string id = share.GetAttribute("name");

            string link = $"{id}|{DateTime.Now.AddHours(2)}";
            var linkbytes = System.Text.Encoding.UTF8.GetBytes(link);
            string encUrl = System.Convert.ToBase64String(linkbytes);

            string getUrl = "https://localhost:44394/DeckBoxes/DeckboxView?deckboxid=" + encUrl;
            _driver.Navigate().GoToUrl(getUrl);
            Assert.Equal("Подарок - MemoryCards", _driver.Title);
            Assert.Contains("Пользователи", _driver.PageSource);
            Assert.Contains("поделился с вами словарем!", _driver.PageSource);

            IWebElement editDeckbox = _driver.FindElement(By.Id("share"));
            string url = editDeckbox.GetAttribute("href");
            _driver.Navigate().GoToUrl(url);

            Assert.Equal("Мои словари - MemoryCards", _driver.Title);
            Assert.Contains("Пользователи", _driver.PageSource);
            Assert.Contains("Мои словари", _driver.PageSource);
            Assert.Contains("Создать новый словарь", _driver.PageSource);
            Assert.Contains("TestTitle", _driver.PageSource);
            Assert.Contains("TestDescription", _driver.PageSource);
            Assert.Contains("Изучено слов", _driver.PageSource);
            Assert.Contains("Количество слов в словаре", _driver.PageSource);
            Assert.Contains("Публичный", _driver.PageSource);
            Assert.Contains("Поделиться", _driver.PageSource);
            Assert.Contains("Редактировать", _driver.PageSource);
            Assert.Contains("Удалить", _driver.PageSource);
        }
    }
}
