﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using Xunit;

namespace MemoryCards.SeleniumUITests.CardsControllerAI
{
    public class EditTests : IDisposable
    {
        private readonly IWebDriver _driver;
        public EditTests()
        {
            ChromeOptions option = new ChromeOptions();
            option.AddArguments("--window-size=1000,1000");
            _driver = new ChromeDriver(option);
        }
        public void Dispose()
        {
            Thread.Sleep(2000);
            _driver.Quit();
            _driver.Dispose();
        }
        private void Login()
        {
            _driver.Navigate().GoToUrl("https://localhost:44394/Account/Login?ReturnUrl=%2F");
            IWebElement email = _driver.FindElement(By.Id("Email"));
            email.SendKeys("seleniumdefaulttestuser@mail.ru");
            IWebElement password = _driver.FindElement(By.Id("Password"));
            password.SendKeys("Integration_1_one");
            _driver.FindElement(By.ClassName("btn")).Click();
        }
        private void GoIntoCards()
        {
            Assert.Equal("Мои словари - MemoryCards", _driver.Title);
            Assert.Contains("Пользователи", _driver.PageSource);
            Assert.Contains("Мои словари", _driver.PageSource);
            IWebElement ReturnDeckSIndex = _driver.FindElement(By.ClassName("work"));
            string url = ReturnDeckSIndex.GetAttribute("href");
            _driver.Navigate().GoToUrl(url);

            Assert.Equal("Коробки - MemoryCards", _driver.Title);
            Assert.Contains("Пользователи", _driver.PageSource);
            Assert.Contains("Количество слов", _driver.PageSource);

            _driver.FindElement(By.XPath("/html/body/div/main/div[1]/div/div[1]/div/a[2]")).Click();

            Assert.Equal("Карточки - MemoryCards", _driver.Title);
            Assert.Contains("Пользователи", _driver.PageSource);
            Assert.Contains("Знаю", _driver.PageSource);
            Assert.Contains("Не знаю", _driver.PageSource);
            Assert.Contains("+", _driver.PageSource);
        }

        [Fact]
        public void ReturnsEditViewTest()
        {
            Login();
            GoIntoCards();
            _driver.FindElement(By.XPath("/html/body/div/main/div[1]/div/div/div[1]/a[2]/i")).Click();

            Assert.Equal("Редактирование карточки - MemoryCards", _driver.Title);
            Assert.Contains("Пользователи", _driver.PageSource);
            Assert.Contains("Редактирование карточки", _driver.PageSource);
            Assert.Contains("Слово", _driver.PageSource);
            Assert.Contains("Значение слова", _driver.PageSource);
            Assert.Contains("Пример слова в предложении", _driver.PageSource);
            Assert.Contains("Фоновая картинка", _driver.PageSource);
            Assert.Contains("Отредактировать", _driver.PageSource);
            Assert.Contains("Назад", _driver.PageSource);
        }
        [Fact]
        public void EditCardsTest_ReturnsIndexView()
        {
            Login();
            GoIntoCards();

            _driver.FindElement(By.XPath("/html/body/div/main/div[1]/div/div/div[1]/a[2]/i")).Click();
            IWebElement word = _driver.FindElement(By.Id("Word"));
            word.Clear();
            word.SendKeys("ExampleWord");

            IWebElement defination = _driver.FindElement(By.Id("Defination"));
            defination.Clear();
            defination.SendKeys("ExampleDefination");

            IWebElement useExample = _driver.FindElement(By.Id("exampleFormControlTextarea1"));
            useExample.Clear();
            useExample.SendKeys("ExampleUseExample");
            Thread.Sleep(10000);
            _driver.FindElement(By.XPath("/html/body/div/main/div[1]/div/form/div[5]/input")).Click();


            Assert.Equal("Карточки - MemoryCards", _driver.Title);
            Assert.Contains("Пользователи", _driver.PageSource);
            Assert.Contains("Знаю", _driver.PageSource);
            Assert.Contains("Не знаю", _driver.PageSource);
            Assert.Contains("+", _driver.PageSource);
        }
        [Fact]
        public void CardsEditModelStringLengthTest()
        {
            Login();
            GoIntoCards();

            _driver.FindElement(By.XPath("/html/body/div/main/div[1]/div/div/div[1]/a[2]/i")).Click();

            IWebElement word = _driver.FindElement(By.Id("Word"));
            word.Clear();
            word.SendKeys("E");

            IWebElement defination = _driver.FindElement(By.Id("Defination"));
            defination.Clear();
            defination.SendKeys("E");
            _driver.FindElement(By.XPath("/html/body/div/main/div[1]/div/form/div[5]/input")).Click();

            string errorMessageWord = _driver.FindElement(By.Id("validWord")).Text;
            string errorMessageDefination = _driver.FindElement(By.Id("validDefination")).Text;
            Assert.Equal("Длина названия должна быть от 3 до 50 символов", errorMessageWord);
            Assert.Equal("Длина определения должна быть от 3 до 150 символов", errorMessageDefination);
        }


        [Fact]
        public void ClickBack_ReturnsCardsIndex()
        {
            Login();
            GoIntoCards();
            _driver.FindElement(By.XPath("/html/body/div/main/div[1]/div/div/div[1]/a[2]/i")).Click();
            _driver.FindElement(By.XPath("/html/body/div/main/div[2]/a")).Click();
            Assert.Equal("Карточки - MemoryCards", _driver.Title);
            Assert.Contains("Пользователи", _driver.PageSource);
            Assert.Contains("Знаю", _driver.PageSource);
            Assert.Contains("Не знаю", _driver.PageSource);
            Assert.Contains("+", _driver.PageSource);
        }

    }
}
