﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using Xunit;

namespace MemoryCards.SeleniumUITests.CardsControllerAI
{
    public class IndexTests : IDisposable
    {
        private readonly IWebDriver _driver;
        public IndexTests()
        {
            ChromeOptions option = new ChromeOptions();
            option.AddArguments("--window-size=1000,1000");
            _driver = new ChromeDriver(option);
        }
        public void Dispose()
        {
            Thread.Sleep(2000);
            _driver.Quit();
            _driver.Dispose();
        }
        private void Login()
        {
            _driver.Navigate().GoToUrl("https://localhost:44394/Account/Login?ReturnUrl=%2F");
            IWebElement email = _driver.FindElement(By.Id("Email"));
            email.SendKeys("seleniumdefaulttestuser@mail.ru");
            IWebElement password = _driver.FindElement(By.Id("Password"));
            password.SendKeys("Integration_1_one");
            _driver.FindElement(By.ClassName("btn")).Click();
        }
        private void GoIntoCards()
        {
            Assert.Equal("Мои словари - MemoryCards", _driver.Title);
            Assert.Contains("Пользователи", _driver.PageSource);
            Assert.Contains("Мои словари", _driver.PageSource);
            IWebElement ReturnDeckSIndex = _driver.FindElement(By.ClassName("work"));
            string url = ReturnDeckSIndex.GetAttribute("href");
            _driver.Navigate().GoToUrl(url);

            Assert.Equal("Коробки - MemoryCards", _driver.Title);
            Assert.Contains("Пользователи", _driver.PageSource);
            Assert.Contains("Количество слов", _driver.PageSource);

            _driver.FindElement(By.XPath("/html/body/div/main/div[1]/div/div[1]/div/a[2]")).Click();

            Assert.Equal("Карточки - MemoryCards", _driver.Title);
            Assert.Contains("Пользователи", _driver.PageSource);
            Assert.Contains("Знаю", _driver.PageSource);
            Assert.Contains("Не знаю", _driver.PageSource);
            Assert.Contains("+", _driver.PageSource);
        }
        [Fact]
        public void ReturnsIndexViewTest()
        {
            Login();

            Assert.Equal("Мои словари - MemoryCards", _driver.Title);
            Assert.Contains("Пользователи", _driver.PageSource);
            Assert.Contains("Мои словари", _driver.PageSource);
            IWebElement ReturnDeckSIndex = _driver.FindElement(By.ClassName("work"));
            string url = ReturnDeckSIndex.GetAttribute("href");
            _driver.Navigate().GoToUrl(url);

            Assert.Equal("Коробки - MemoryCards", _driver.Title);
            Assert.Contains("Пользователи", _driver.PageSource);
            Assert.Contains("Количество слов", _driver.PageSource);

            _driver.FindElement(By.XPath("/html/body/div/main/div[1]/div/div[1]/div/a[2]")).Click();

            Assert.Equal("Карточки - MemoryCards", _driver.Title);
            Assert.Contains("Пользователи", _driver.PageSource);
            Assert.Contains("Знаю", _driver.PageSource);
            Assert.Contains("Не знаю", _driver.PageSource);
            Assert.Contains("+", _driver.PageSource);
        }
        [Fact]
        public void ClickEditTest()
        {
            Login();
            GoIntoCards();
            _driver.FindElement(By.XPath("/html/body/div/main/div[1]/div/div/div[1]/a[2]/i")).Click();

            Assert.Equal("Редактирование карточки - MemoryCards", _driver.Title);
            Assert.Contains("Пользователи", _driver.PageSource);
            Assert.Contains("Редактирование карточки", _driver.PageSource);
            Assert.Contains("Слово", _driver.PageSource);
            Assert.Contains("Значение слова", _driver.PageSource);
            Assert.Contains("Пример слова в предложении", _driver.PageSource);
            Assert.Contains("Фоновая картинка", _driver.PageSource);
            Assert.Contains("Отредактировать", _driver.PageSource);
            Assert.Contains("Назад", _driver.PageSource);
        }
        [Fact]
        public void ClickDeleteTest()
        {
            Login();
            GoIntoCards();
            
            IWebElement delete = _driver.FindElement(By.Id("#delete"));
            string url = delete.GetAttribute("href");
            _driver.Navigate().GoToUrl(url);

            Assert.Equal("Карточки - MemoryCards", _driver.Title);
            Assert.Contains("Пользователи", _driver.PageSource);
            Assert.Contains("+", _driver.PageSource);
        }
        [Fact]
        public void ClickCreateNewDeckboxTest()
        {
            Login();
            GoIntoCards();
            _driver.FindElement(By.XPath("/html/body/div/main/div[2]/div[1]/div/div/a/p")).Click();

            Assert.Equal("Создание новой карточки - MemoryCards", _driver.Title);
            Assert.Contains("Пользователи", _driver.PageSource);
            Assert.Contains("Создание новой карточки", _driver.PageSource);
            Assert.Contains("Слово", _driver.PageSource);
            Assert.Contains("Значение слова", _driver.PageSource);
            Assert.Contains("Пример слова в предложении", _driver.PageSource);
            Assert.Contains("Фоновая картинка", _driver.PageSource);
            Assert.Contains("Создать карточку", _driver.PageSource);
            Assert.Contains("Назад", _driver.PageSource);
        }
    }
}
