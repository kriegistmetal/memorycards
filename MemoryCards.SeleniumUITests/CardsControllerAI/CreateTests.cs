﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using Xunit;

namespace MemoryCards.SeleniumUITests.CardsControllerAI
{
    public class CreateTests : IDisposable
    {
        private readonly IWebDriver _driver;
        public CreateTests()
        {
            ChromeOptions option = new ChromeOptions();
            option.AddArguments("--window-size=1000,1000");
            _driver = new ChromeDriver(option);
        }
        public void Dispose()
        {
            Thread.Sleep(2000);
            _driver.Quit();
            _driver.Dispose();
        }
        private void Login()
        {
            _driver.Navigate().GoToUrl("https://localhost:44394/Account/Login?ReturnUrl=%2F");
            IWebElement email = _driver.FindElement(By.Id("Email"));
            email.SendKeys("seleniumdefaulttestuser@mail.ru");
            IWebElement password = _driver.FindElement(By.Id("Password"));
            password.SendKeys("Integration_1_one");
            _driver.FindElement(By.ClassName("btn")).Click();
        }
        private void GoIntoCards()
        {
            Assert.Equal("Мои словари - MemoryCards", _driver.Title);
            Assert.Contains("Пользователи", _driver.PageSource);
            Assert.Contains("Мои словари", _driver.PageSource);
            IWebElement ReturnDeckSIndex = _driver.FindElement(By.ClassName("work"));
            string url = ReturnDeckSIndex.GetAttribute("href");
            _driver.Navigate().GoToUrl(url);

            Assert.Equal("Коробки - MemoryCards", _driver.Title);
            Assert.Contains("Пользователи", _driver.PageSource);
            Assert.Contains("Количество слов", _driver.PageSource);

            _driver.FindElement(By.XPath("/html/body/div/main/div[1]/div/div[1]/div/a[2]")).Click();

            Assert.Equal("Карточки - MemoryCards", _driver.Title);
            Assert.Contains("Пользователи", _driver.PageSource);
            Assert.Contains("Знаю", _driver.PageSource);
            Assert.Contains("Не знаю", _driver.PageSource);
            Assert.Contains("+", _driver.PageSource);
        }
        [Fact]
        public void ReturnsCardsCreateViewTest()
        {
            Login();
            GoIntoCards();
            _driver.FindElement(By.XPath("/html/body/div/main/div[2]/div[1]/div/div/a/p")).Click();

            Assert.Equal("Создание новой карточки - MemoryCards", _driver.Title);
            Assert.Contains("Пользователи", _driver.PageSource);
            Assert.Contains("Создание новой карточки", _driver.PageSource);
            Assert.Contains("Слово", _driver.PageSource);
            Assert.Contains("Значение слова", _driver.PageSource);
            Assert.Contains("Пример слова в предложении", _driver.PageSource);
            Assert.Contains("Фоновая картинка", _driver.PageSource);
            Assert.Contains("Создать карточку", _driver.PageSource);
            Assert.Contains("Назад", _driver.PageSource);
        }
        [Fact]
        public void CreateNewCardsTest_ReturnsIndexView()
        {
            Login();
            GoIntoCards();

            _driver.FindElement(By.XPath("/html/body/div/main/div[2]/div[1]/div/div/a/p")).Click();

            IWebElement word = _driver.FindElement(By.Id("card_Word"));
            word.SendKeys("ExampleWordCreate");

            IWebElement defination = _driver.FindElement(By.Id("card_Defination"));
            defination.SendKeys("ExampleDefinationCreate");

            IWebElement useExample = _driver.FindElement(By.Id("exampleFormControlTextarea1"));
            useExample.SendKeys("ExampleUseExampleCreate");

            _driver.FindElement(By.XPath("/html/body/div/main/div[1]/div/form/div[5]/input")).Click();

            Assert.Equal("Карточки - MemoryCards", _driver.Title);
            Assert.Contains("Пользователи", _driver.PageSource);
            Assert.Contains("Знаю", _driver.PageSource);
            Assert.Contains("Не знаю", _driver.PageSource);
            Assert.Contains("+", _driver.PageSource);
        }
        [Fact]
        public void CardsModelRequiredTest()
        {
            Login();
            GoIntoCards();

            _driver.FindElement(By.XPath("/html/body/div/main/div[2]/div[1]/div/div/a/p")).Click();
            _driver.FindElement(By.XPath("/html/body/div/main/div[1]/div/form/div[5]/input")).Click();
            string errorMessageWord = _driver.FindElement(By.Id("validWord")).Text;
            Assert.Equal("Обязательное поле", errorMessageWord);
            string errorMessageDefination = _driver.FindElement(By.Id("validDefination")).Text;
            Assert.Equal("Обязательное поле", errorMessageDefination);
        }
    }
}
