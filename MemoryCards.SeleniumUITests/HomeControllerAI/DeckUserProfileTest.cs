﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using Xunit;

namespace MemoryCards.SeleniumUITests.HomeControllerAI
{
   public class DeckUserProfileTest : IDisposable
    {
        private readonly IWebDriver _driver;

        public DeckUserProfileTest()
        {
            ChromeOptions option = new ChromeOptions();
            option.AddArguments("--window-size=1000,1000");
            _driver = new ChromeDriver(option);
        }
        public void Dispose()
        {
            Thread.Sleep(2000);
            _driver.Quit();
            _driver.Dispose();
        }
        [Fact]
        public void DeckViewTest()
        {
            _driver.Navigate().GoToUrl("https://localhost:44394/Home/DeckUserProfile/2");

            Assert.Equal("Коробки - MemoryCards", _driver.Title);
            Assert.Contains("Количество слов", _driver.PageSource);
            Assert.Contains("Назад", _driver.PageSource);
            Assert.Contains("1", _driver.PageSource);
        }
        [Fact]
        public void NotClickEdit()
        {
            _driver.Navigate().GoToUrl("https://localhost:44394/Home/DeckUserProfile/2");
            _driver.FindElement(By.Id("edit")).Click();

            Assert.Equal("Коробки - MemoryCards", _driver.Title);
            Assert.Contains("Количество слов", _driver.PageSource);
            Assert.Contains("Назад", _driver.PageSource);
            Assert.Contains("1", _driver.PageSource);
        }
    }
}
