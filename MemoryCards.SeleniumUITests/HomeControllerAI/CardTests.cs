﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using Xunit;

namespace MemoryCards.SeleniumUITests.HomeControllerAI
{
   public class CardTests : IDisposable
    {
        private readonly IWebDriver _driver;
        public CardTests()
        {
            ChromeOptions option = new ChromeOptions();
            option.AddArguments("--window-size=1000,1000");
            _driver = new ChromeDriver(option);
        }
        public void Dispose()
        {
            Thread.Sleep(2000);
            _driver.Quit();
            _driver.Dispose();
        }
        [Fact]
        public void CardViewTest()
        {
            _driver.Navigate().GoToUrl("https://localhost:44394/Home/Card/14");

            Assert.Equal("Карточки - MemoryCards", _driver.Title);
            Assert.Contains("Назад", _driver.PageSource);
            Assert.Contains("Значение слова :", _driver.PageSource);
            Assert.Contains("Не знаю", _driver.PageSource);
            Assert.Contains("Знаю", _driver.PageSource);
            Assert.Contains("Карточки", _driver.PageSource);
            Assert.Contains("Пример слова в предложении :", _driver.PageSource);
        }
        [Fact]
        public void NotClickCreateBtn()
        {
            _driver.Navigate().GoToUrl("https://localhost:44394/Home/Card/14");

            IWebElement shareDeckBox = _driver.FindElement(By.Id("create"));
            string url = shareDeckBox.GetAttribute("href");
            _driver.Navigate().GoToUrl(url);

            Assert.Equal("Карточки - MemoryCards", _driver.Title);
            Assert.Contains("Назад", _driver.PageSource);
            Assert.Contains("Значение слова :", _driver.PageSource);
            Assert.Contains("Не знаю", _driver.PageSource);
            Assert.Contains("Знаю", _driver.PageSource);
            Assert.Contains("Карточки", _driver.PageSource);
            Assert.Contains("Пример слова в предложении :", _driver.PageSource);
        }
     
        [Fact]
        public void NotClickEditBtn()
        {
            _driver.Navigate().GoToUrl("https://localhost:44394/Home/Card/14");

            IWebElement shareDeckBox = _driver.FindElement(By.Id("edit"));
            string url = shareDeckBox.GetAttribute("href");
            _driver.Navigate().GoToUrl(url);

            Assert.Equal("Карточки - MemoryCards", _driver.Title);
            Assert.Contains("Назад", _driver.PageSource);
            Assert.Contains("Значение слова :", _driver.PageSource);
            Assert.Contains("Не знаю", _driver.PageSource);
            Assert.Contains("Знаю", _driver.PageSource);
            Assert.Contains("Карточки", _driver.PageSource);
            Assert.Contains("Пример слова в предложении :", _driver.PageSource);
        }
        [Fact]
        public void NotClickDeleteBtn()
        {
            _driver.Navigate().GoToUrl("https://localhost:44394/Home/Card/14");

            IWebElement shareDeckBox = _driver.FindElement(By.Id("Delete"));
            string url = shareDeckBox.GetAttribute("href");
            _driver.Navigate().GoToUrl(url);

            Assert.Equal("Карточки - MemoryCards", _driver.Title);
            Assert.Contains("Назад", _driver.PageSource);
            Assert.Contains("Значение слова :", _driver.PageSource);
            Assert.Contains("Не знаю", _driver.PageSource);
            Assert.Contains("Знаю", _driver.PageSource);
            Assert.Contains("Карточки", _driver.PageSource);
            Assert.Contains("Пример слова в предложении :", _driver.PageSource);
        }
    }
}
