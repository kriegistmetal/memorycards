﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using Xunit;

namespace MemoryCards.SeleniumUITests.HomeControllerAI
{
  public  class DeckBoxTests : IDisposable
    {
        private readonly IWebDriver _driver;
        public DeckBoxTests()
        {
            ChromeOptions option = new ChromeOptions();
            option.AddArguments("--window-size=1000,1000");
            _driver = new ChromeDriver(option);
        }
        public void Dispose()
        {
            Thread.Sleep(2000);
            _driver.Quit();
            _driver.Dispose();
        }
        [Fact]
        public void DeckBoxViewTest()
        {
            _driver.Navigate().GoToUrl("https://localhost:44394/Home/DeckBox");

            Assert.Equal("Домашняя страница - MemoryCards", _driver.Title);
            Assert.Contains("Добро пожаловать!", _driver.PageSource);
            Assert.Contains("это приложение разработано для изучения английского языка", _driver.PageSource);
            Assert.Contains("Поделиться", _driver.PageSource);
            Assert.Contains("Редактировать", _driver.PageSource);
            Assert.Contains("Удалить", _driver.PageSource);
        }
        [Fact]
        public void NotClickShareBtn()
        {
            _driver.Navigate().GoToUrl("https://localhost:44394/Home/DeckBox");

            IWebElement shareDeckBox = _driver.FindElement(By.Id("share"));
            string url = shareDeckBox.GetAttribute("href");
            _driver.Navigate().GoToUrl(url);

            Assert.Equal("Домашняя страница - MemoryCards", _driver.Title);
            Assert.Contains("Добро пожаловать!", _driver.PageSource);
            Assert.Contains("это приложение разработано для изучения английского языка", _driver.PageSource);
            Assert.Contains("Поделиться", _driver.PageSource);
            Assert.Contains("Редактировать", _driver.PageSource);
            Assert.Contains("Удалить", _driver.PageSource);
        }
        [Fact]
        public void NotClickEditBtn()
        {
            _driver.Navigate().GoToUrl("https://localhost:44394/Home/DeckBox");

            IWebElement shareDeckBox = _driver.FindElement(By.Id("edit"));
            string url = shareDeckBox.GetAttribute("href");
            _driver.Navigate().GoToUrl(url);

            Assert.Equal("Домашняя страница - MemoryCards", _driver.Title);
            Assert.Contains("Добро пожаловать!", _driver.PageSource);
            Assert.Contains("это приложение разработано для изучения английского языка", _driver.PageSource);
            Assert.Contains("Поделиться", _driver.PageSource);
            Assert.Contains("Редактировать", _driver.PageSource);
            Assert.Contains("Удалить", _driver.PageSource);
        }
        [Fact]
        public void NotClickDeleteBtn()
        {
            _driver.Navigate().GoToUrl("https://localhost:44394/Home/DeckBox");

            IWebElement shareDeckBox = _driver.FindElement(By.Id("delete"));
            string url = shareDeckBox.GetAttribute("href");
            _driver.Navigate().GoToUrl(url);

            Assert.Equal("Домашняя страница - MemoryCards", _driver.Title);
            Assert.Contains("Добро пожаловать!", _driver.PageSource);
            Assert.Contains("это приложение разработано для изучения английского языка", _driver.PageSource);
            Assert.Contains("Поделиться", _driver.PageSource);
            Assert.Contains("Редактировать", _driver.PageSource);
            Assert.Contains("Удалить", _driver.PageSource);
        }
    }
}
